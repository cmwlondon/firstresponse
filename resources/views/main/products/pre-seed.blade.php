@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
	@include('main.layouts.partials._masthead', ['masthead_image'=>'pre-seed', 'alt'=>'Pre&#8209;Seed&#8482; Fertility-friendly personal lubricant'])
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>PRE&#8209;SEED&#8482; FERTILITY&#8209;FRIENDLY PERSONAL LUBRICANT</h1>
			<p class="mt2">Pre&#8209;Seed&#8482; Fertility&#8209;Friendly Lubricant is specially designed for use when trying to conceive. During this time, most women will experience occasional vaginal dryness at some point, which can cause discomfort during intimacy. This dryness can increase as a couple transitions from lovemaking to “baby-making”, with scheduled intercourse around their fertile time.</p>

			<p class="mt2">Pre&#8209;Seed&#8482; Fertility&#8209;Friendly Lubricant mimics natural fertile fluids and helps to relieve vaginal dryness. Its moisture is delivered in the same pH, osmolality and viscosity (consistency) as fertile cervical fluids (mucus). This provides an optimal environment for sperm on their journey through the vagina, into the cervix and up to the fallopian tubes where they meet the egg.</p>
			
			

		</div>
	</div>

	

	<div class="cream except-white pb0">
		<div class="maxInner">
			<div class="columns span-5 sm-12 md-6 md-before-3 mt4">
				<img src="{{{$image_path}}}/packs/pre-seed-large.png?{{{$version}}}"
						srcset="
						{{{$image_path}}}/packs/pre-seed-small.png?{{{$version}}} 240w,
						{{{$image_path}}}/packs/pre-seed-medium.png?{{{$version}}} 540w,
						{{{$image_path}}}/packs/pre-seed-large.png?{{{$version}}} 960w"
	 					alt="PRE&#8209;SEED&#8482; FERTILITY-FRIENDLY PERSONAL LUBRICANT" class="pack"/>
			</div>
			<div class="columns span-7 sm-12 md-10 md-before-1 pack-text">
				<h2>WHY CHOOSE PRE&#8209;SEED&#8482; FERTILITY&#8209;FRIENDLY PERSONAL LUBRICANT?</h2>
				<p>Pre&#8209;Seed&#8482; Fertility&#8209;Friendly Lubricant is a high-quality lubricant that mimics fertile cervical fluids to support sperm on their journey. Unlike most other lubricants, which can be toxic to sperm and should not be used when you are ready to conceive, Pre&#8209;Seed&#8482; Fertility&#8209;Friendly Lubricant is pH-balanced and isotonic, so sperm can swim freely. Pre&#8209;Seed&#8482; Fertility&#8209;Friendly Lubricant has been clinically shown to be fertility friendly, is safe for use and doesn’t harm sperm.</p>
				<p class="mt1 smaller">To read full details and instructions, <a href="/pdf/Pre-Seed-More-Info-Leaflet.pdf" target="_blank" title="Pre&#8209;Seed More Info Leaflet">click here</a>.</p>
				<div class="buttons tac mt2">
					<a href="{!! __('links.buy-pre-seed-boots') !!}" target="_blank" alt="Pre&#8209;Seed : Boots" class="button left" @click="track">Buy Now</a>
					{{-- <a href="{!! route('faqs') !!}" class="button right">FAQs</a> --}}
				</div>
				<p class="mt2 smaller">Available at:</p>
				<div class="buttons tac" style="margin-top:0.5rem;">
					<a href="{!! __('links.buy-pre-seed-boots') !!}" target="_blank" rel="nofollow"><img src="{{{$image_path}}}/stores/boots.svg?{{{$version}}}" alt="Boots" style="height:5rem" @click="track"/></a>
				</div>
			</div>

		</div>
	</div>

	<div class="cream">
		<div class="columns span-12 mt4">
			<h5>You might also be interested in:</h5>
			<div class="button-group"><a href="{!! route('our-pregnancy-tests') !!}" class="button twoline left"><span>Pregnancy Tests</span>
			</a> <a href="{!! route('your-test-results') !!}" class="button twoline right"><span>Pregnancy Test<br/>Results</span></a></div>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
