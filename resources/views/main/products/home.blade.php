@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
	@include('main.layouts.partials._masthead', ['masthead_image'=>'pregnancy-test', 'alt'=>' Pregnancy Test'])
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>Our Pregnancy Tests</h1>
			<p class="mt2">When you want to know if you’re pregnant, you want to find out as soon as you can. The FIRST RESPONSE&#8482; Early Result Pregnancy Test, with first-to-detect technology, is sensitive enough to capture scant amounts of the pregnancy hormone. No test gives you an answer sooner than FIRST RESPONSE&#8482;.</p>
		</div>
	</div>

	<div class="notCream buy-now">
		<div class="maxInner">
			
			<div class="columns span-12 mt4">
				<div class="row full">
					<div class="columns span-5 sm-12 md-10 md-before-1 packs">
						<a name="early-results-pregnancy-test"></a>
						<h2>FIRST RESPONSE&#8482; Early <br class="hidden vis-sm vis-md"/>Result <br class="hidden vis-xxlg"/>Pregnancy Test</h2>
						<img src="{{{$image_path}}}/shims/shim-3-1.gif" class="shim"/>
						<img src="{{{$image_path}}}/packs/early-results-pregnancy-test-large.png?{{{$version}}}"
						srcset="
						{{{$image_path}}}/packs/early-results-pregnancy-test-small.png?{{{$version}}} 240w,
						{{{$image_path}}}/packs/early-results-pregnancy-test-medium.png?{{{$version}}} 540w,
						{{{$image_path}}}/packs/early-results-pregnancy-test-large.png?{{{$version}}} 960w"
	 					alt="FIRST RESPONSE&#8482; Early Result Pregnancy Test" class="pack"/>
						<div class="buttons">
							<a href="{!! route('buy-now') !!}" class="button left">Buy Now</a>
							<a href="{!! route('faqs') !!}" class="button right">FAQs</a>
						</div>
					</div>
					<div class="columns span-7 sm-12 md-10 md-before-1 details">
						<ul>
							<li>
								<h4 data-ref="simple">Simple</h4>
								<p>Just hold the test in your urine stream for 5 seconds.</p>
							</li>
							<li>
								<h4 data-ref="fast">Fast</h4>
								<p>Read the result in 3 minutes.</p>
							</li>
							<li>
								<h4 data-ref="clear">Clear</h4>
								<p>Two pink lines pregnant, one pink line not pregnant.</p>
							</li>
							<li>
								<h4 data-ref="accurate">Sensitive</h4>
								<p>FIRST RESPONSE&#8482; has ultra-sensitive technology that can detect really small amounts of the pregnancy hormone.</p>
							</li>
						</ul>
						<p class="disclaimer">* Always read the leaflet, in laboratory testing, FIRST RESPONSE&#8482; Early Result Pregnancy Test  detected hormone levels consistent with pregnancy in 76% of women 6 days before the day of missed period, in 96% of women 5 days before the day of missed period, and in >99% of women 4 or fewer days before the day of missed period.</p>
					</div>
				</div>
			</div>

			<div class="columns span-12 mt4">
				<div class="row full">
					<div class="columns span-5 sm-12 md-10 md-before-1 packs">
						<a name="rapid-results-pregnancy-test"></a>
						<h2>FIRST RESPONSE&#8482; Rapid <br class="hidden vis-sm vis-md"/>Result Pregnancy Test</h2>
						<img src="{{{$image_path}}}/shims/shim-3-1.gif" class="shim"/>
						<img src="{{{$image_path}}}/packs/early-results-pregnancy-test-large.png?{{{$version}}}"
						srcset="
						{{{$image_path}}}/packs/rapid-results-pregnancy-test-small.png?{{{$version}}} 240w,
						{{{$image_path}}}/packs/rapid-results-pregnancy-test-medium.png?{{{$version}}} 540w,
						{{{$image_path}}}/packs/rapid-results-pregnancy-test-large.png?{{{$version}}} 960w"
	 					alt="FIRST RESPONSE&#8482; Rapid Result Pregnancy Test" class="pack"/>
						<div class="buttons">
							<a href="{!! __('links.buy-asda') !!}" target="_blank" alt="ASDA" class="button left" @click="track">Buy Now</a>
							<a href="{!! route('faqs') !!}" class="button right">FAQs</a>
						</div>
					</div>
					<div class="columns span-7 sm-12 md-10 md-before-1 details">
						<p>Easy to read results in 45 seconds. FIRST RESPONSE&#8482; Rapid Result Pregnancy Test can be used from the first day of your missed period.</p>
						<ul>
							<li>
								<h4 data-ref="simple">Simple</h4>
								<p>Just hold the test in your urine stream for 5 seconds.</p>
							</li>
							<li>
								<h4 data-ref="fast">Fast</h4>
								<p>Read results in 45 seconds.</p>
							</li>
							<li>
								<h4 data-ref="clear">Clear</h4>
								<p>Two pink lines pregnant, one pink line not pregnant.</p>
							</li>
							<li>
								<h4 data-ref="accurate">Accurate</h4>
								<p>Over 99% accurate in laboratory testing.</p>
							</li>
						</ul>
						<p class="mt2">Available in Asda stores only.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="cream">
		<div class="columns span-12 mt4">
			<h5>You might also be interested in:</h5>
			<div class="button-group"><a href="{!! route('how-to-use') !!}" class="button twoline left"><span>How to Test</span>
			</a> <a href="{!! route('your-test-results') !!}" class="button twoline right"><span>Pregnancy Test<br/>Results</span></a></div>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
