@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>HOW TO USE PRE&#8209;SEED&#8482; FERTILITY-FRIENDLY PERSONAL LUBRICANT</h1>
		</div>

		<div class="columns span-12 outline-box">
			<h3 class="mb2">Directions for using Pre&#8209;Seed&#8482; Fertility&#8209;Friendly Lubricant with Disposable Applicator</h3>
			<div class="row">
				<div class="columns span-6 sm-12 md-12">
					<div class="instruction left">
						<img src="{{{$image_path}}}/pre-seed/instruction-1.png" class="w100"/>
						<p>Many women choose to apply the product about 15 minutes prior to intercourse to allow moisture to disperse in the vagina.</p>
						<p><strong>1.</strong> Push the applicator plunger all the way in. Twist the applicator onto the threads of the tube.</p>
						<p><strong>2.</strong> Squeeze from the bottom of the tube to fill the applicator with the amount of lubricant you’d like to use. Most women will choose the 2-3g fill line. Keeping the applicator upright, unscrew the applicator from the tube and place the cap back on.</p>
						<p class="uppercase"><strong>Do not leave the tube uncapped between uses.</strong></p>
					</div>
				</div>
				<div class="columns span-6 sm-12 md-12">
					<div class="instruction right">
						<img src="{{{$image_path}}}/pre-seed/instruction-2.png" class="w100"/>
						<p><strong>3.</strong> Gently insert the applicator into the vagina - about half its length. Once in place, slowly push the plunger to dispense the lubricant.</p>
						<p><strong>4.</strong> Remove and discard the applicator.</p>
						<p>Each applicator is intended for a single use. Discard the applicator following use. Do not store Pre&#8209;Seed&#8482; Fertility&#8209;Friendly Lubricant in applicator for more than 30 minutes prior to use.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="columns span-12 outline-box">
			<h3 class="mb2">DIRECTIONS FOR USING PRE&#8209;SEED&#8482; FERTILITY&#8209;FRIENDLY LUBRICANT EXTERNALLY</h3>
			<p>Remove seal from tube before initial use. Squeeze a small amount of product (teaspoon) onto your finger and apply to genital area. Vary amount to achieve desired lubrication. To read full details and instructions, <a href="/pdf/Pre-Seed-More-Info-Leaflet.pdf" target="_blank" title="Pre&#8209;Seed More Info Leaflet">click here</a>.</p>
		</div>
	</div>

	<div class="cream except-white">
		<div class="columns span-12 mt4">
			<h5>You might also be interested in:</h5>
			<div class="button-group"><a href="{!! route('pre-seed') !!}" class="button twoline left"><span>Pre&#8209;Seed&#8482;<br/>Lubricant</span></a> <a href="{!! route('our-pregnancy-tests') !!}" class="button twoline right"><span>Pregnancy Tests</span>
			</a></div>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
