@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
	@include('main.layouts.partials._masthead', ['masthead_image'=>'first-response-accuracy', 'alt'=>' First Response Accuracy'])
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>FIRST RESPONSE&#8482; Technology</h1>
			<p class="mt2">When the moment comes, you depend on pregnancy test accuracy. The most sensitive at-home pregnancy tests should be able to detect pregnancy before the first day of your expected period and should be able to identify more than one form of the pregnancy hormone hCG. For example, they should be able to detect not only hCG but also a variant of the pregnancy hormone, hyperglycosylated hCG or hCG-H.</p>
			<p class="mt2">The cell, that originates when a fertilised egg is implanted in the uterine wall, initially secretes the pregnancy hormone variant hCG-H. This hormone variant is actually produced in the earliest stages of pregnancy before the primary pregnancy hormone, hCG, which is produced immediately following implantation by a different set of cells.</p>
			<p class="mt2">It is important to note that some women may not have detectable amounts of pregnancy hormone in their urine until after the expected period. This is because hCG levels vary between women, and because some women sometimes have difficulty predicting the exact date of their next period.</p>
			<p class="mt2">The FIRST RESPONSE&#8482; Early Result Pregnancy Test is designed to detect hCG as early as 6 days before your missed period. The amount of pregnancy hormone increases as pregnancy progresses. In laboratory testing, FIRST RESPONSE&#8482; detected hormone levels consistent with pregnancy in 76% of women 6 days before the day of missed period, in 96% of women 5 days before the day of missed period, and in >99% of women 4 or fewer days before the day of missed period.</p>
			<p class="mt2">FIRST RESPONSE&#8482; Early Result Pregnancy Test is over 99% accurate from the day of the expected period in laboratory studies.</p>
			<p class="mt2"><sup>1</sup> Women&rsquo;s Health Journal November '09 Publication: Embryonic Development and Pregnancy Test Sensitivity</p>
			<h3 class="mt2">WHEN CAN I TEST WITH FIRST RESPONSE&#8482; EARLY RESULT PREGNANCY TESTS?</h3>
			<p class="mt2">You can test as early as six days before the day of your missed period. You can use the FIRST RESPONSE&#8482; Early Result Pregnancy Test any time of the day. You don't have to use first morning urine. Generally, however, your first morning urine contains the highest level of the pregnancy hormone.</p>
		</div>
	</div>

	

	<div class="cream">
		<div class="columns span-12 mt4">
			<h5>You might also be interested in:</h5>
			<div class="button-group"><a href="{!! route('how-to-use') !!}" class="button twoline left"><span>How to Test</span>
			</a> <a href="{!! route('your-test-results') !!}" class="button twoline right"><span>Pregnancy Test<br/>Results</span></a></div>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
