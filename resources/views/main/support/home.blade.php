@extends('main.layouts.main')


@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>Get in Touch</h1>
			<p class="mt2">When should I take a pregnancy test? What do my pregnancy test results mean? How do I read my pregnancy test?</p>
			<p class="mt2">Our specialist team are on hand to answer your queries. To get answers quickly, call the FIRST RESPONSE&#8482; Advice Line at Church &amp; Dwight UK Ltd between 8.30am and 5.00pm Monday - Friday on 0800 121 6080. Alternatively please complete our enquiry form below. In all product related correspondence, please quote the lot number printed on the carton.</p>
		</div>

		<div class="columns span-12 outline-box bottom-space">
			
			<div class="form">
				@if (session('response'))
					<h3 class="message">{!! session('response.message') !!}</h3>
				@else


				{!! Form::open(array('url' => ['contact'], 'method' => 'POST', 'id' => 'supportForm') )!!}

				<div class="formRow">
					{!! Form::label('title', 'Title: *') !!}
					{!! Form::select('title', $titlesArr, null, ['id' => 'title', 'class' => 'input short'] ) !!}
					{!! $errors->first('title', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('firstname', 'First Name: *') !!}
					{!! Form::text('firstname',null,['placeholder' => '', 'id' => 'firstname', 'class' => 'input']) !!}
					{!! $errors->first('firstname', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('lastname', 'Last Name: *') !!}
					{!! Form::text('lastname',null,['placeholder' => '', 'id' => 'lastname', 'class' => 'input']) !!}
					{!! $errors->first('lastname', '<small class="error">:message</small>') !!}
				</div>

				{{-- <div class="formRow">
					{!! Form::label('company', 'Company:') !!}
					{!! Form::text('company',null,['placeholder' => '', 'id' => 'company', 'class' => 'input']) !!}
					{!! $errors->first('company', '<small class="error">:message</small>') !!}
				</div> --}}

				{{-- <div class="formRow">
					{!! Form::label('address', 'Address:') !!}
					{!! Form::text('address',null,['placeholder' => '', 'id' => 'address', 'class' => 'input']) !!}
					{!! $errors->first('address', '<small class="error">:message</small>') !!}
				</div> --}}

				{{-- <div class="formRow">
					{!! Form::label('city', 'City:') !!}
					{!! Form::text('city',null,['placeholder' => '', 'id' => 'city', 'class' => 'input']) !!}
					{!! $errors->first('city', '<small class="error">:message</small>') !!}
				</div> --}}

				{{--<div class="formRow">
					{!! Form::label('county', 'County:') !!}
					{!! Form::text('county',null,['placeholder' => '', 'id' => 'county', 'class' => 'input']) !!}
					{!! $errors->first('county', '<small class="error">:message</small>') !!}
				</div>--}}

				{{-- <div class="formRow">
					{!! Form::label('postcode', 'Postcode:') !!}
					{!! Form::text('postcode',null,['placeholder' => '', 'id' => 'postcode', 'class' => 'input']) !!}
					{!! $errors->first('postcode', '<small class="error">:message</small>') !!}
				</div> --}}

				{{-- <div class="formRow">
					{!! Form::label('country', 'Country:') !!}
					{!! Form::text('country',null,['placeholder' => '', 'id' => 'country', 'class' => 'input']) !!}
					{!! $errors->first('country', '<small class="error">:message</small>') !!}
				</div> --}}

				<div class="formRow">
					{!! Form::label('phone', 'Phone Number:') !!}
					{!! Form::text('phone',null,['placeholder' => '', 'id' => 'phone', 'class' => 'input']) !!}
					{!! $errors->first('phone', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('email', 'Email Address: *') !!}
					{!! Form::text('email',null,['placeholder' => '', 'id' => 'email', 'class' => 'input']) !!}
					{!! $errors->first('email', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('product', 'Product:') !!}
					{!! Form::text('product',null,['placeholder' => '', 'id' => 'product', 'class' => 'input']) !!}
					{!! $errors->first('product', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('comments', 'Comments:') !!}
					{!! Form::textarea('comments',null,['placeholder' => '', 'id' => 'comments', 'rows' => '4', 'class' => 'input']) !!}
					{!! $errors->first('comments', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow tar">
					{!! Form::submit('Submit', array('class' => 'button', 'id' => 'bSubmit', 'onClick' => "ga('send', 'event', { eventCategory: 'Contact', eventAction: 'Submit', eventLabel: 'Contact Form'});")) !!}
				</div>

				{!! Form::close() !!}

				@endif
			</div>

		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
