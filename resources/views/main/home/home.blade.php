@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
	@include('main.layouts.partials._masthead', ['masthead_image'=>'first-response', 'alt' => 'Woman looking at pregnancy test'])
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-10 before-1 after-1 intro">
			<h1>No test tells you sooner</h1>
			<p class="mt2">The FIRST RESPONSE&#8482; Early Result Pregnancy Test, with first-to-detect technology, is sensitive enough to capture scant amounts of the pregnancy hormone. No other test gives you an early answer sooner than FIRST&nbsp;RESPONSE&#8482;.</p>
		</div>
		<div class="columns span-4 md-8 md-before-2 sm-10 sm-before-1 mt3">
			<div class="cta-block">
				<a href="{!! route('how-to-use') !!}">
				<img src="data:image/false;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="shim square" alt=" "/>
				<img src="data:image/false;base64,R0lGODlhtwEVAYAAAP///wAAACH5BAEAAAAALAAAAAC3ARUBAAL/hI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubq7vL2+v7CxwsPExcbHyMnKy8zNzs/AwdLT1NXW19jZ2tvc3d7f0NHi4+Tl5ufo6err7O3u7+Dh8vP09fb3+Pn6+/z9/v/w8woMCBBAsaPIgwocKFDBs6fAgxosSJFCtavIgxo8aN8hw7evwIMqTIkSRLmjyJMqXKlSxbunwJM6bMmTRr2ryJM6fOnTx7+vwJNKjQoUSLGj2KNKnSpUybOn0KNarUqVSrWr2KNavWrVy7ev0KNqzYsWTLmj2LNq3atWzbun0LN67cuXTr2r2LN6/evXz7+v0LOLDgwYQLGz6MOLHixYwbO34MObLkyZQrW76MObPmzZw7e/4MOrTo0aRLmz6NOrXq1axbu34NO7bs2bRr276NO7fu3bx7+/4NPLjw4cSLGz+OPLny5cybO38OPbr06dSrW7+OPbv27dy7e/8OPrz48eTLmz+PPr369ezbu38PP2kBADs=" class="shim wide" alt=" "/>
				<img src="{{{$image_path}}}/home/taking-a-test.jpg?{{{$version}}}" class="photo" alt="Taking a Test"/>
				<div class="block-text">
					<div class="oval"><span>Taking a test</span></div>
					<p>Follow step-by-step instructions for how to take your FIRST RESPONSE&#8482; Pregnancy Test</p>
				</div>
				</a>
			</div>
		</div>
		<div class="columns span-4 md-8 md-before-2 sm-10 sm-before-1 mt3">
			<div class="cta-block">
				<a href="{!! route('your-test-results') !!}">
				<img src="data:image/false;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="shim square" alt=" "/>
				<img src="data:image/false;base64,R0lGODlhtwEVAYAAAP///wAAACH5BAEAAAAALAAAAAC3ARUBAAL/hI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubq7vL2+v7CxwsPExcbHyMnKy8zNzs/AwdLT1NXW19jZ2tvc3d7f0NHi4+Tl5ufo6err7O3u7+Dh8vP09fb3+Pn6+/z9/v/w8woMCBBAsaPIgwocKFDBs6fAgxosSJFCtavIgxo8aN8hw7evwIMqTIkSRLmjyJMqXKlSxbunwJM6bMmTRr2ryJM6fOnTx7+vwJNKjQoUSLGj2KNKnSpUybOn0KNarUqVSrWr2KNavWrVy7ev0KNqzYsWTLmj2LNq3atWzbun0LN67cuXTr2r2LN6/evXz7+v0LOLDgwYQLGz6MOLHixYwbO34MObLkyZQrW76MObPmzZw7e/4MOrTo0aRLmz6NOrXq1axbu34NO7bs2bRr276NO7fu3bx7+/4NPLjw4cSLGz+OPLny5cybO38OPbr06dSrW7+OPbv27dy7e/8OPrz48eTLmz+PPr369ezbu38PP2kBADs=" class="shim wide" alt=" "/>
				<img src="{{{$image_path}}}/home/your-test-results.jpg?{{{$version}}}" class="photo" alt="Your test results"/>
				<div class="block-text">
					<div class="oval"><span>Your test results</span></div>
					<p>Quickly understand what your FIRST RESPONSE&#8482; Pregnancy Test results mean</p>
				</div>
				</a>
			</div>
		</div>
		<div class="columns span-4 md-8 md-before-2 sm-10 sm-before-1 mt3">
			<div class="cta-block">
				<a href="{!! route('buy-now') !!}">
				<img src="data:image/false;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="shim square" alt=" "/>
				<img src="data:image/false;base64,R0lGODlhtwEVAYAAAP///wAAACH5BAEAAAAALAAAAAC3ARUBAAL/hI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubq7vL2+v7CxwsPExcbHyMnKy8zNzs/AwdLT1NXW19jZ2tvc3d7f0NHi4+Tl5ufo6err7O3u7+Dh8vP09fb3+Pn6+/z9/v/w8woMCBBAsaPIgwocKFDBs6fAgxosSJFCtavIgxo8aN8hw7evwIMqTIkSRLmjyJMqXKlSxbunwJM6bMmTRr2ryJM6fOnTx7+vwJNKjQoUSLGj2KNKnSpUybOn0KNarUqVSrWr2KNavWrVy7ev0KNqzYsWTLmj2LNq3atWzbun0LN67cuXTr2r2LN6/evXz7+v0LOLDgwYQLGz6MOLHixYwbO34MObLkyZQrW76MObPmzZw7e/4MOrTo0aRLmz6NOrXq1axbu34NO7bs2bRr276NO7fu3bx7+/4NPLjw4cSLGz+OPLny5cybO38OPbr06dSrW7+OPbv27dy7e/8OPrz48eTLmz+PPr369ezbu38PP2kBADs=" class="shim wide" alt=" "/>
				<img src="{{{$image_path}}}/home/buy-now.jpg?{{{$version}}}" class="photo" alt="Buy now"/>
				<div class="block-text">
					<div class="oval"><span>Buy Now</span></div>
					<p>Order your FIRST RESPONSE&#8482; Pregnancy Test online</p>
				</div>
				</a>
			</div>
		</div>
	</div>

	<div class="cream packs">
		<div class="maxInner">
			<div class="columns span-12 mt4">
				<h2>Our Products</h2>
			</div>
			<div class="columns span-6 md-8 md-before-2 sm-10 sm-before-1 product" data-id="1">
				<div class="group">
					<img src="{{{$image_path}}}/packs/early-results-pregnancy-test-large.png?{{{$version}}}"
						srcset="
						{{{$image_path}}}/packs/early-results-pregnancy-test-small.png?{{{$version}}} 240w,
						{{{$image_path}}}/packs/early-results-pregnancy-test-medium.png?{{{$version}}} 540w,
						{{{$image_path}}}/packs/early-results-pregnancy-test-large.png?{{{$version}}} 960w"
						sizes="(min-width:320px) 100vw"
	 					alt="FIRST RESPONSE&#8482; Early Result Pregnancy Test"/>
					<div class="text">
						<div class="text-inner">
							<h5>FIRST RESPONSE&#8482; Early Result Pregnancy Test</h5>
							<p>No other pregnancy test tells you sooner<br/><a href="{!! route('our-pregnancy-tests') !!}#early-results-pregnancy-test">Find out more</a></p>
						</div>
						<div class="buy"><a href="{!! route('buy-now') !!}" class="button buy">Buy Now</a></div>
					</div>
				
					
				</div>
			</div>
			<div class="columns span-6 md-8 md-before-2 sm-10 sm-before-1 product" data-id="2">
				<div class="group">
					<img src="{{{$image_path}}}/packs/rapid-results-pregnancy-test-large.png?{{{$version}}}"
						srcset="
						{{{$image_path}}}/packs/rapid-results-pregnancy-test-small.png?{{{$version}}} 240w,
						{{{$image_path}}}/packs/rapid-results-pregnancy-test-medium.png?{{{$version}}} 540w,
						{{{$image_path}}}/packs/rapid-results-pregnancy-test-large.png?{{{$version}}} 960w"
	 					sizes="(min-width:320px) 100vw"
	 					alt="FIRST RESPONSE&#8482; Rapid Result Pregnancy Test"/>
					<div class="text">
						<div class="text-inner">
							<h5>FIRST RESPONSE&#8482; Rapid Result Pregnancy Test</h5>
							<p>When you&rsquo;re short on time, take our 45 second test<br/><a href="{!! route('our-pregnancy-tests') !!}#rapid-results-pregnancy-test">Find out more</a></p>
							<p class="mt1"><span style="font-size:75%;">Available in Asda stores only.</span></p>
						</div>
						<div class="buy"><a href="{!! __('links.buy-asda') !!}" target="_blank" alt="ASDA" class="button buy" @click="track">Buy Now</a></div>

					</div>
					
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
