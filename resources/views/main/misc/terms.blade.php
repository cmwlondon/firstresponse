@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div id="miscWrapper" class="maxInner bottom-space">
		<div class="columns span-12 intro">
			<h1>Terms and Conditions</h1>
		</div>

		<div class="columns span-12 outline-box forced-left">
			
			<p><a href="#general">General Terms and Conditions</a></p>
			<p><a href="#ecommerce">e-Commerce Terms and Conditions</a></p>
			<p><a href="#ratings">Customer Ratings and Reviews Terms and Conditions</a></p>
			
			<hr class="mt3"/>

			<h2><a name="general" class="nolink">General Terms and Conditions</a></h2>
			<ol>
				<li>All information (&quot;Information&quot;) which you read or see at this Site is protected by copyright and/or other intellectual property laws. The contents (&quot;Contents&quot;) are owned by Church &amp; Dwight UK Ltd (&quot;Church &amp; Dwight&quot;), its affiliates, or other third parties from whom Church &amp; Dwight has received certain legal rights. You may not report, modify, publish, sell, reproduce, distribute, post, display, transmit, or in any way exploit any of this Site&#39;s Contents for commercial purposes. You may, if you wish, download and retain on a disk or in hard drive form a single copy of the Contents of this Site for personal, non-commercial purposes as long as you do not remove any proprietary notices.</li>
				<li>While Church &amp; Dwight has made reasonable efforts to include Information at this site which is accurate and timely, Church &amp; Dwight makes no representations or warranties as to the accuracy of such Information and, specifically, Church &amp; Dwight assumes no liability or responsibility for any errors or omissions in the Information or the Contents of this Site. Moreover, Church &amp; Dwight neither warrants nor represents that your use of the Information will not infringe the rights of third parties who are not affiliated with Church &amp; Dwight. Your access to and use of this Site are at your own risk, and neither Church &amp; Dwight nor any party involved in the creation, transmittal, or maintenance of this Site shall be liable to you for any direct, indirect, consequential, incidental or punitive damages of any kind allegedly arising out of your access or use of this Site, or your inability to access or use this Site. Notwithstanding anything to the contrary contained herein, the Contents of this Site are provided to you on an &quot;AS IS&quot; basis and specifically WITHOUT WARRANTY OF ANY KIND, WHETHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES, SATISFACTORY QUALITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. Please note that some jurisdictions may not permit the exclusion of implied warranties and, as a result, some of the exclusions referenced above may not apply to you. You should check your local laws for any limitations or restrictions which might impact you.</li>
				<li>Church &amp; Dwight assumes no responsibility and shall not be liable for any damages to, or any viruses that may infect, your computer equipment resulting from your access to or use of this Site, or the downloading of any Information from this Site.</li>
				<li>The trademarks, logos and service marks (&quot;Trademarks&quot;) displayed throughout the Site are registered and unregistered Trademarks of Church &amp; Dwight and/or third party licensors. No license, right or permission is granted to you for any use of the Trademarks by you or anyone authorized by you. Misuse of the Trademarks is strictly prohibited and Church &amp; Dwight will aggressively enforce its intellectual property rights to the fullest extent of the law, including the pursuit of criminal prosecution whenever and wherever necessary.</li>
				<li>The pictures and images of people, products, places or things displayed on this Site are either the property of Church &amp; Dwight or are used with the permission of third parties. Any use of such pictures or images by you or anyone authorized or affiliated with you is strictly prohibited. Unauthorized uses of pictures and images may violate copyright or trademark laws, privacy laws, or communication laws or regulations.</li>
				<li>Descriptions of Church &amp; Dwight&#39;s products contained within the Site shall not constitute product labelling. You should use Church &amp; Dwight&#39;s products in accordance with the instructions contained on the cartons and labels found on those products in the country of purchase.</li>
				<li>Internet users located in countries which are subject to U.S.A. trade embargo laws and regulations are prohibited from accessing this Site and are asked to promptly exit at this time.</li>
				<li>Any communication or material transmitted to this Site by electronic mail or other means, shall be treated as non-confidential and non-proprietary. This includes ideas, suggestions, comments, questions and any other information or data. Anything submitted to Church &amp; Dwight can be used, reproduced, transmitted, disclosed or published by Church &amp; Dwight or its affiliates without restriction or compensation, except we would like you to be aware that Church &amp; Dwight has established an Internet Privacy Policy regarding personal data and other information you transmit to this Site which can be accessed via <a href="http://www.firstresponsefertility.com/policies">http://www.firstresponsefertility.com/policies</a></li>
				<li>Church &amp; Dwight has not reviewed all of the sites which are linked to this Site. As a result, Church &amp; Dwight is not responsible for the content of such linked sites and your linking to such sites is at your own risk.</li>
				<li>Church &amp; Dwight reserves the right to alter or delete any material from the Content of this Site at any time. Church &amp; Dwight further reserves the right to discontinue this Site at any time and without notice.</li>
				<li>This Agreement shall be governed by and construed in accordance with the laws of England &amp; Wales, without regard to any conflicts of law provisions. Any cause of action with respect to this Site or this Agreement must be filed in courts of competent jurisdiction in England &amp; Wales within six years after the cause of action has accrued (three years in the case of a cause of action for personal injury); unless such a filing is made in accordance with such rules, the cause shall be permanently barred.</li>
			</ol>
			<p>&copy; 2012 Church &amp; Dwight UK Ltd. For UK residents only. All rights reserved</p>
			<p>FIRST RESPONSE&#8482; is a trademark of Church &amp; Dwight Co, Inc., Princeton, New Jersey, USA</p>

			<h2><a name="ecommerce" class="nolink">e-Commerce Terms and Conditions</a></h2>
			<p><strong>Sale of Products</strong></p>
			<p>This is an important section of the site explaining the terms and conditions you are agreeing to when you use and/or shop from the FIRST RESPONSE&#8482; website. Products are sold from this site by Church &amp; Dwight UK Ltd.</p>
			<p><strong>Acceptance of orders</strong></p>
			<p>We have joined together with Paypal in order to make your shopping as secure as possible. To review their terms and conditions, please click here. Working with industry-leading secure payment processors, we validate your order and credit or debit card details you submit to us before your order can be accepted. Once that has been done, we will acknowledge that your order has been received by sending an e-mail to the e-mail address you provide in your registration form.</p>
			<p>Church &amp; Dwight UK Ltd reserves the right to restrict the number of items available for sale to any individual customer. All orders are subject to availability. If your order cannot be fulfilled you will be offered an alternative or given a full refund.</p>
			<p>We accept payment by most major credit or debit cards. Payment will be debited from your account before the dispatch of your purchase. All credit and charge card holders are subject to validation checks and authorisation by the card issuer.</p>
			<p><strong>Price information</strong></p>
			<p>Church &amp; Dwight UK Ltd has taken great care to ensure prices are accurate. All prices include VAT where appropriate. It is the company&rsquo;s discretion to change pricing at any time, and these may change from time to time to incorporate special on-line offers. All offers are subject to availability.</p>
			<p><strong><a name="ecommerce-delivery" class="nolink">Delivery Costs and Timings</a></strong></p>
			<p>All items will be dispatched by second class post and can take up to seven working days to deliver from the first working day on or after the order date.</p>
			<p>If we are experiencing any difficulties in fulfilling your order, we will contact you to advise you of an expected delivery date.</p>
			<p>Items can be delivered to addresses within the UK only.</p>
			<p><strong>Packaging</strong></p>
			<p>All items will be discreetly packaged.</p>
			<p><strong>Cancellation </strong></p>
			<p>You may cancel your order (or any part of it) at any stage before the Products are dispatched to you. Where you decide to cancel an order after we have dispatched the Products, you will be under a duty to return them to us, at your own risk. All such Products should be returned within 30 days after the Products have been delivered to you. Until such time as they are returned, you must retain possession of the Products and take reasonable care of them. You should return the Products to us unused and in the same condition in which you received them together with the original Product packaging, by one of the means set out in our Returns Procedure.</p>
			<p>After you cancel your order and return the Products, any sum debited by us to your credit/debit card will be refunded in full. We will notify you of your refund via e-mail within a reasonable period of time. We will usually refund any money received from you using the same method originally used by you to pay for your purchase. Refunds for Products purchased as gifts can only be given to the credit/debit card of the person who placed the order. We will usually process the refund due to you as soon as possible and, in any case, within 30 days of you cancelling your order.</p>
			<p><strong>Returning Goods</strong></p>
			<p>If you are unhappy with the products you have received, a full refund will be given if the products are returned to us within 14 days of receiving them. All goods must be returned to us for inspection before a refund can be given. All returns are the responsibility of the customer and we recommend you send all returned goods via a traceable means, such as recorded delivery.</p>
			<p>If any Product you purchase is damaged, faulty or incorrect, when delivered to you we may offer an exchange or refund as appropriate, in accordance with your legal rights. If you believe a Product is faulty, you should return the Product to us in accordance with the Returns Procedure. If you have any questions regarding returns, please Contact Us.</p>
			<p>Our policy on cancellations, returns and refunds does not affect your statutory legal rights.</p>
			<p><strong>General</strong></p>
			<p>Unless otherwise specified, the materials on this website are directed solely at consumers who access this website from the United Kingdom. Church &amp; Dwight UK Ltd does not represent that any product referred to in the materials on this website is appropriate for use, or available, in other locations. Those who choose to access this site from other locations are responsible for compliance with local laws if and to the extent local laws are applicable.</p>
			<p>Any contract between us, whether for use of the site or in relation to the purchase of products or services through the site will be governed by the laws of England and Wales and all parties submit to the non-exclusive jurisdiction of the English Courts.</p>
			<p>Any contract will be communicated in English.</p>
			<p>We have taken every care in the preparation of the content of this website, however we cannot guarantee uninterrupted and totally reliable access to this website, and so therefore cannot guarantee that the information will always be completely up to date and free of mistakes. To the extent permitted by applicable law, Church &amp; Dwight UK Ltd disclaim all warranties; express or implied, as to the accuracy of the information contained in any of the materials on this website and will accept no liability for any loss or damage arising as a result of problems with access.</p>
			<p>Church &amp; Dwight UK Ltd shall not be liable to any person for any loss or damage, which may arise from the use of any of the information contained in any of the materials on this website.</p>
			<p>We may make software owned or operated by third-party companies available to you. You must only use this software in accordance with the terms and conditions imposed by the third-party provider.</p>
			<p>These terms and conditions shall be governed by and construed in accordance with English Law. If any provision of these terms and conditions shall be unlawful, void or for any reason unenforceable then that provision shall be deemed severable and shall not affect the validity and enforceability of the remaining provisions.</p>
			<p>Church &amp; Dwight UK Ltd reserves the right to alter these terms and conditions from time to time upon reasonable notice by posting new terms and conditions on this website. Your continued use of and access to the website confirms your acceptance of the revised terms and conditions.</p>
			<p>&copy; 2012 Church &amp; Dwight UK Ltd. For UK residents only. All rights reserved</p>
			<p>FIRST RESPONSE&#8482; is a trademark of Church &amp; Dwight Co, Inc., Princeton, New Jersey, USA</p>



			<h2><a name="ratings" class="nolink">Customer Ratings and Reviews Terms and Conditions</a></h2>
			<p>These Terms of Use govern your conduct associated with the Customer Ratings and Review service (the &quot;CRR Service&quot;) and the Product Question and Answers service (the &quot;PQA Service&quot;) offered by Church &amp; Dwight UK Ltd. To the extent of any conflict between Church &amp; Dwight UK Ltd&rsquo;s Privacy Policy and these Terms of Use, these Terms of Use shall take precedence with respect to the CRR Service and PQA Service.</p>
			<p>By submitting any content to Church &amp; Dwight UK Ltd, you represent and warrant that:</p>
			<ul>
				<li>you are the sole author and owner of the intellectual property rights thereto;</li>
				<li>all &quot;moral rights&quot; that you may have in such content have been voluntarily waived by you;</li>
				<li>all content that you post is accurate;</li>
				<li>you are at least 16 years old; and that use of the content you supply does not violate these Terms of Use and will not cause injury to any person or entity.</li>
			</ul>
			
			<p>You further agree and warrant that you shall not submit any content:</p>
			<ul>
				<li>that is known by you to be false, inaccurate or misleading;</li>
				<li>that incites violence;</li>
				<li>that contains nudity or graphic or gratuitous violence;</li>
				<li>that infringes any third party&#39;s copyright, patent, trademark, trade secret or other proprietary rights or rights of publicity or privacy;</li>
				<li>that violates any law, statute, ordinance or regulation (including, but not limited to, those governing export control, consumer protection, unfair competition, anti-discrimination or false advertising);</li>
				<li>that is, or may reasonably be considered to be, defamatory, libellous, hateful, racially or religiously biased or offensive, unlawfully threatening or unlawfully harassing to any individual, partnership or corporation;</li>
				<li>for which you were compensated or granted any consideration by any third party;</li>
				<li>that includes any information that references other websites, addresses, email addresses, contact information or phone numbers; or</li>
				<li>that contains any computer viruses, worms or other potentially damaging computer programs or files.</li>
			</ul>
			
			<p>You agree to indemnify and hold Church &amp; Dwight UK Ltd (and its officers, directors, agents, subsidiaries, joint ventures, employees and third-party service providers), harmless from all claims, demands, and damages (actual and consequential) of every kind and nature, known and unknown including reasonable legal fees, arising out of a breach of your representations and warranties set forth above, or your violation of any law or the rights of a third party.</p>
			<p>For any content that you submit, you grant Church &amp; Dwight UK Ltd a, perpetual, irrevocable, royalty-free, transferable right and license to use, copy, modify, delete in its entirety, adapt, publish, translate, create derivative works from and/or sell and/or distribute such content and/or incorporate such content into any form, medium or technology (whether currently existing or new) for the full period of which copyright protection is awarded, throughout the world without compensation to you.</p>
			<p>All content that you submit may be used at Church &amp; Dwight UK Ltd sole discretion. Church &amp; Dwight UK Ltd reserves the right to moderate all submissions prior to posting and to change, condense or delete any content on its website that Church &amp; Dwight UK Ltd deems, in its sole discretion, to violate the content guidelines or any other provision of these Terms of Use. Church &amp; Dwight UK Ltd does not guarantee that you will have any recourse through firstresponsefertility.com to edit or delete any content you have submitted. Ratings and written comments are generally posted within two to four business days. However, Church &amp; Dwight UK Ltd reserves the right to remove or to refuse to post any submission for any reason. You acknowledge that you, not Church &amp; Dwight UK Ltd, are responsible for the contents of your submission. None of the content that you submit shall be subject to any obligation of confidence on the part of Church &amp; Dwight UK Ltd, its agents, subsidiaries, affiliates, partners or third party service providers and their respective directors, officers and employees.</p>
			<p>By submitting your email address in connection with your rating and review, you agree that Church &amp; Dwight UK Ltd and its third party service providers may use your email address to contact you about the status of your review and other administrative purposes.</p>
			<p>Church &amp; Dwight UK Ltd does not allow the contents of ratings and reviews and product question and answer comments to be used by third-party suppliers without our prior consent.</p>
			<p>Ratings and reviews and product question and answer comments left on any medical products are subject to all of the above terms and conditions. They do not constitute the advice of a medical professional nor the opinions of Church &amp; Dwight UK Ltd. They in no way replace a medical consultation.</p>
			<p>The following is not acceptable on this site (these are not exclusive):</p>
			<ul>
				<li>Inappropriate or unsafe medical advice, including reference to inappropriate use of a medicine or medical device</li>
				<li>Disparaging reference to a healthcare product, company, institution or medical profession</li>
				<li>Reference to inappropriate use of a vitamin, mineral, supplement or other complementary healthcare product</li>
				<li>Reference to a named healthcare professional</li>
				<li>Medicinal claims made about a product which is not a medicine</li>
				<li>Excessive praise for a medicine likely to encourage others to over-use</li>
				<li>Messages which, in the moderators view contains vulgar, offensive, sexually orientated, racist or abusive language</li>
				<li>Messages which appear to be blatant advertisements</li>
				<li>Messages which cause offence or distress to anyone reading them (including where users have reported abuse on a post and our moderation team uphold the abuse report)</li>
				<li>Messages which contain personal details such as e-mail address, postal address or telephone number</li>
				<li>Messages which contain price information which is misleading</li>
				<li>Messages which are posted with a username or profile image that could be deemed offensive</li>
				<li>We will not be able to post messages in any other language other than English</li>
			</ul>
			<p>If users do not adhere to these guidelines we may delete their posts. Church &amp; Dwight UK Ltd reserves the right to delete any message or rating and review displayed on firstresponsefertility.com at any time, for any reason without notice.</p>
			<p>&copy; 2012 Church &amp; Dwight UK Ltd. For UK residents only. All rights reserved.</p>
			<p>FIRST RESPONSE&#8482; is a trademark of Church &amp; Dwight Co, Inc., Princeton, New Jersey, USA</p>
		</div>

	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
