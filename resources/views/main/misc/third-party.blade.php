@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div id="miscWrapper" class="maxInner bottom-space">
		<div class="columns span-12 intro">
			<h1>THIRD PARTY INFORMATION COLLECTION</h1>
		</div>

		<div class="columns span-12 outline-box bottom-space all-left">
			
			<p class="mt0">NONE</p>
		</div>

	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
