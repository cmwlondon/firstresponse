@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div id="miscWrapper" class="maxInner bottom-space buy-now coupon">
		<div class="columns span-12 intro">
			<h1>Looking for offers?</h1>
			<p class="mt2">We don’t have any coupons running right now, but we will be running offers throughout the year at the retailers below:*</p>
		</div>
		<div class="columns span-4 before-2 md-6 md-before-0 sm-12 sm-before-0 mt2">
			<img src="data:image/false;base64,R0lGODlhCgAEAIAAAP///wAAACH5BAAAAAAALAAAAAAKAAQAAAIFhI+py1gAOw==" class="shim"/>
			<a href="{!! __('links.buy-boots') !!}" target="_blank" rel="nofollow"><img src="{{{$image_path}}}/stores/boots.svg?{{{$version}}}" alt="Boots" class="logo" @click="track"/></a>
		</div>
		<div class="columns span-4 after-2 md-6 md-after-0 sm-12 sm-after-0 mt2">
			<img src="data:image/false;base64,R0lGODlhCgAEAIAAAP///wAAACH5BAAAAAAALAAAAAAKAAQAAAIFhI+py1gAOw==" class="shim"/>
			<a href="{!! __('links.buy-superdrug') !!}" target="_blank" rel="nofollow"><img src="{{{$image_path}}}/stores/superdrug.svg?{{{$version}}}" alt="Superdrug" class="logo" @click="track"/></a>
		</div>
		<div class="columns span-12 mt5">

		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
	*Offers subject to change
@endsection
