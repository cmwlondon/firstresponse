@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div id="miscWrapper" class="maxInner bottom-space">
		<div class="columns span-12 intro">
			<h1>Policies</h1>
		</div>

		<div class="columns span-12 outline-box bottom-space">
			
			<h3>INTERNET PRIVACY POLICY</h3>
		    <p>Church &amp; Dwight Co., Inc. the makers of FIRST RESPONSE&#8482; Products and other fine products have established an Internet Privacy Policy. We would like you, our Web Site visitors to be aware of our policies.</p>
			<h3>WEBSITE INFORMATION</h3>
			<p>Church &amp; Dwight Co., Inc. will not sell any information we gather at our web sites. We collect identifiable information for four reasons: 1) to contact consumers with reminders, updates, and news, 2) to notify winners of contests, 3) to send out brochures etc. via regular mail, and 4) to conduct consumer testing and feedback.</p>
			<h3>SURVEYS</h3>
			<p>Information gathered from polls and surveys are only for internal use of Church &amp; Dwight Co., Inc.
			</p>
			<h3>REMINDERS, UPDATES, HOT NEWS!</h3>
			<p>In various places within the Church &amp; Dwight Co., Inc Web Sites, there is an option to subscribe to an E-mail database which is used to send reminders, updates and hot news pertaining to the web sites. There's always an option to decline the subscription or unsubscribe after receiving the newsletter. E-mail addresses gathered from minors and adults for subscription purposes are used only for this purpose and no other.</p>
			<h3>COOKIES</h3>
			<p>When using this Website, we may automatically collect information from you using cookies.Cookies are small text files that are placed on your computer by a website in order to make websites work, or work more efficiently, as well as to provide information to the owners of the site. These cookies help us to run the site and provide you with the best possible service. Under NO circumstances is any data tied to any identifiable information such as e-mail address. Most internet browsers automatically accept cookies, however this setting can be changed, or you can manually deactivate cookies using your internet browser&rsquo;s privacy settings. <a href="http://www.allaboutcookies.org/" target="_blank" rel="nofollow">http://www.allaboutcookies.org</a></p>
			<p>We&rsquo;ve outlined below the types of cookie that are used on this Website.</p>
			<h3>ANALYTICS</h3>
			<p>Outlined in the table below is a list of the cookies we use to analyse how this Website is used. You can opt-out of this by <a href="https://tools.google.com/dlpage/gaoptout" target="_blank" rel="nofollow">clicking here</a>.</p>
			<table>
				<tbody>
					<tr>
						<th width="60">Name</th>
						<th>Purpose</th>
					</tr>
					<tr>
						<td>
							_utma<br/>
							_utmb<br/>
							_utmc<br/>
							_utmz<br/>
							_utmv<br/>
							_utmx
						</td>
						<td valign="top">Our analytics cookies monitor how visitors navigate the Website and how they found their way here. We use these so that we can see the total amount of visitors to the Website; this does not offer individual information. It also allows us to see what content our users enjoy most which helps us to provide you with the best service.</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
