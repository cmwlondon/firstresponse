@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>Pregnancy Test Results - Positive</h1>
			<p class="mt2">FIRST RESPONSE&#8482; Pregnancy Tests have an easy to read test stick - two pink lines in the Result Window means you are pregnant, one pink line means you are not pregnant. The appearance of a second line, which may be lighter than the other, is a positive result.</p>
			<p class="mt2">Finding out you are pregnant can be an exciting time for many but also quite a shock for those who hadn't planned to conceive just yet.</p>
		</div>

		
		<div class="columns span-6 sm-12 md-12 mt1 outline-box match" data-group="1">
			<div class="toMeasure">
				<h2>PLANNED</h2>
				<p>The first thing you should do is make an appointment to see your healthcare professional. Your midwife or doctor can provide you with much of the information you will need to know about the months ahead. Learning that you are pregnant should serve as encouragement for you to adopt healthy lifestyle habits. Avoid using drugs, cigarettes, alcohol and caffeine to the best of your ability. If you are currently taking medications (prescription or over-the-counter), be sure to discuss any possible effects they may have on you and your baby when you see your midwife or doctor.</p>
			</div>
		</div>
		<div class="columns span-6 sm-12 md-12 mt1 outline-box match" data-group="1">
			<div class="toMeasure">
				<h2>UNPLANNED</h2>
				<p>If you haven't planned for a baby, discovering you are pregnant can be a scary time. No choice may feel completely right - just what's best given your circumstances. You need to act quickly in order to keep all your options open, and therefore you should consult a health specialist as soon as possible. You can discuss it with your doctor, midwife or local family planning association. And don't forget that you may find it helpful to talk it over with a number of people you feel you can trust such as your partner, parent or carer.</p>
			</div>
		</div>

		<div class="columns span-12 mt1 outline-box">
			<h2>TIPS FOR A HEALTHY PREGNANCY</h2>
			<h3>SMOKING, ALCOHOL AND MEDICATION</h3>
			<p>If you haven't already done so, now is the time to stop smoking. Smoking during pregnancy can lead to problems for your baby once it is born. These problems can include low birth weight, premature birth and asthma developing later on. The earlier you can stop smoking the better but it is never too late to quit. It is advisable to stop drinking alcohol during pregnancy. However, there is no evidence that an occasional drink is harmful - talk to your midwife or doctor about how much alcohol it is safe to drink.You should see your midwife or doctor as soon as possible regarding any medication you may already be taking or would like to take. Do not take any illegal drugs as they can harm your baby.</p>
			<h3>EXERCISE</h3>
			<p>It is possible to continue exercising throughout your pregnancy, the fitter you are the easier it is to deal with pregnancy and birth. Many forms of exercise are suitable during pregnancy, such as swimming or walking, but do not suddenly start exercising vigorously. You should consult your midwife or doctor about any exercise you wish to undertake.</p>
			<h3>EAT HEALTHILY</h3>
			<p>We all know we should eat healthily but it is even more important before and during pregnancy. To find out more about the advantages of a healthy diet and get advice &amp; tips on what to eat, visit our <a href="{!! route('planning-for-a-baby') !!}">Trying for a baby</a>? page. Any dietary change should be made in consultation with your GP or ask your midwife or doctor.</p>
			<h3>ENSURE YOU ARE GETTING ENOUGH OF THE RIGHT VITAMINS</h3>
			<p>Once you are pregnant it is still important that you get enough folic acid and calcium. The Department of Health recommends that women continue to take a folic acid supplement and eat a diet rich in folic acid up until the 12th week of pregnancy. Pregnant women should avoid foods containing too much vitamin A. Speak to your midwife about what foods contain vitamin A and how much is safe to have.</p>
		</div>

	</div>	

	<div class="cream">
		<div class="maxInner">
			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<p class="button-group"><a href="{!! route('your-test-results') !!}" class="button twoline left"><span>Pregnancy Test<br/>Results</span>
				</a> <a href="{!! route('tips-for-a-healthy-pregnancy') !!}" class="button twoline right"><span>Healthy Pregnancy Tips</span></a></p>
			</div>
			
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
