@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
	@include('main.layouts.partials._masthead', ['masthead_image'=>'two-pregnancy-tests', 'alt'=>'Two Pregnancy Tests'])
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>Am I Pregnant?</h1>
			<p class="mt2">Follow the instructions below to understand your FIRST RESPONSE&#8482; Pregnancy Test results:</p>
		</div>

		<div class="columns span-12 outline-box">
			<h2>Pregnant</h2>
			<hr/>
			<p class="block" data-id="1"><span>Two pink lines in the pregnancy test result window means <b>you are pregnant</b>. A positive test result shows that the pregnancy hormone (hCG) was detected.</span></p>
			<hr class="or"/>
			<p class="block" data-id="2"><span>One pink line and one line lighter than the other in the pregnancy test result window means <b>you are pregnant</b>. Any positive test result (even a very faint line) shows that the pregnancy hormone (hCG) was detected.</span></p>
			<hr class="mb0"/>
		</div>

		<div class="columns span-12 outline-box">
			<h2>Not Pregnant</h2>
			<hr/>
			<p class="block" data-id="3"><span>One pink line in the pregnancy test result window means <b>you may not be pregnant</b>, or it may be too early to tell.</span></p>
			<hr class="mb0"/>
		</div>
		
		<div class="columns span-12 solid-box">
			<p>CAUTION: If you choose to test early and the result is negative, you may not be pregnant or it may be too early to tell because your urine does not have enough hCG for the test to give a positive result. If you do not get your period within seven days, you should retest with another FIRST RESPONSE&#8482; Early Result Pregnancy Test. If you receive another Not Pregnant result and your period still hasn’t started, we recommend you consult your healthcare professional.</p>
		</div>

	</div>

	<div class="cream">
		<div class="maxInner">
			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('due-date-calculator') !!}" class="button twoline left"><span>Due Date<br/>Calculator</span>
				</a> <a href="{!! route('faqs') !!}" class="button twoline right"><span>FAQs</span></a></div>

				<h2>Need to retest?</h2>
				<p><a href="{!! route('buy-now') !!}" class="button">Buy Now</a></p>
			</div>
			
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
