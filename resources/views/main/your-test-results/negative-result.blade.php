@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>Pregnancy Test Results - Negative</h1>
			<p class="mt2">If you've taken a pregnancy test and have received a negative result, you may not be pregnant or it may be too early to tell. It is possible that you miscalculated the length of your cycle or some pregnant women may not have detectable amounts of the pregnancy hormone in their urine on the day they use the test. The amount of pregnancy hormone increases as the pregnancy progresses. If you do not get your period within seven days, you should retest with another FIRST RESPONSE&#8482; Early Result Pregnancy Test.</p>
		</div>

	</div>	

	<div class="cream">
		<div class="maxInner">
			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<p class="button-group"><a href="{!! route('your-test-results') !!}" class="button twoline left"><span>Pregnancy Test<br/>Results</span>
				</a> <a href="{!! route('conception-advice') !!}" class="button twoline right"><span>Conception Advice</span></a></p>
			</div>
			
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
