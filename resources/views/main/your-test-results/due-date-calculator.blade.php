@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')

	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>When Are You Due?</h1>
			<p class="mt2">It's happened, you're pregnant. Congratulations! Of course you want to know the approximate arrival date of this much-anticipated delivery.</p>
		</div>

		<div id="calculator" class="row full">

			<div class="columns span-12 calculator-wrap">

				<div class="form">

					<div class="form-row">
						<label for="datepicker_0">What was the first date of your last menstrual period?</label>
						<div class="calendar-wrap">
							<input name="first_day" v-model="first_day.value" @change="onChange" type="text" id="datepicker_0">
							<div class="calendar-inner">
								<img src="{{{$image_path}}}/pregnancy-test-calculator/calendar.jpg?{{{$version}}}" alt="">
							</div>
						</div>
					</div>

					<div class="form-row">
						<label for="average_cycle">How many days are there in your cycle?</label>
						<div class="control-wrap">
							<input type="text" :value="average_cycle.rendered" id="average_cycle" @blur="checkCycleValue" @keyup.enter="checkCycleValue">
							<div class="control-inner">
								<div class="up" @click="incCycle"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 10" style="enable-background:new 0 0 15 10;" xml:space="preserve">
								<path d="M7.5,0C8,0,8.6,0.3,8.9,0.7l5.6,7.2c0.5,0.7,0.4,1.2,0.3,1.4c-0.1,0.2-0.4,0.7-1.3,0.7H1.5c-0.9,0-1.2-0.4-1.3-0.7C0,9.1-0.1,8.6,0.4,7.9l5.6-7.2C6.4,0.3,7,0,7.5,0z"/>
								</svg></div>
								<div class="down" @click="decCycle"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 10" style="enable-background:new 0 0 15 10;" xml:space="preserve">
								<path d="M7.5,10C7,10,6.4,9.7,6.1,9.3L0.4,2.1C-0.1,1.4,0,0.9,0.1,0.7C0.3,0.4,0.6,0,1.5,0h12.1c0.9,0,1.2,0.4,1.3,0.7c0.1,0.2,0.2,0.8-0.3,1.4L8.9,9.3C8.6,9.7,8,10,7.5,10z"/>
								</svg></div>
							</div>
						</div>
						
					</div>

					<div class="button-row">
						<button type="button" class="button" @click="calc">Calculate</button>
					</div>
				</div>
			</div>

			<div class="columns span-12 result-wrap" :show="resultDisplay">
				<h3>Your due date is...</h3>
				<p v-html="resultOutput"></p>
			</div>
			
			<div class="columns span-12 solid-box">
				<h3>How does this tool calculate my conception date?<sup>1</sup></h3>
				<p>By counting backwards 14 days from the day before you expect your next period to start, which is also the day ovulation most likely occurred, and your estimated date of conception.</p>
				<p class="mt2 small"><sup>1</sup> Only your physician can accurately determine your due date or date of conception, based on his/her knowledge of your complete medical condition.</p>
			</div>

			
		</div>

	</div>
	
	<div class="cream">
		<div class="maxInner">
			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<p class="button-group"><a href="{!! route('stages-of-pregnancy') !!}" class="button twoline left"><span>Stages of<br/>Pregnancy</span>
				</a> <a href="{!! route('tips-for-a-healthy-pregnancy') !!}" class="button twoline right"><span>Healthy Pregnancy Tips</span></a></p>
			</div>
			
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
