<div class="bar"></div>
<img src="/images/triangle-up.png?{ time() }" class="triangle" data-pin-nopin="true" alt="Menu Identifier"/>
<div id="topMenu" class="topmenu-container maxInner">
	
	<img src="/images/hamburger.svg" class="burger" alt="Show Menu" @click="showMobileMenu" data-pin-nopin="true"/>
	<a href="/"><img src="/images/first-response-logo.png?{ time() }" class="logo" alt="FIRST RESPONSE&#8482; FERTILITY"/></a>

	<menu class="menu left">
		<li><div class="btn" @click="noClick" @mouseover="showSubMenu" data-id="3"><a href="#">Our Products</a></div></li>
		<li><div class="btn" @click="noClick" @mouseover="showSubMenu" data-id="2"><a href="#">Your Test Results</a></div></li>
		<li><div class="btn" @click="noClick" @mouseover="showSubMenu" data-id="1"><a href="#">How to Use</a></div></li>
	</menu>
	<menu class="menu right">
		<li><div class="btn" @click="noClick" @mouseover="showSubMenu" data-id="4"><a href="#">Planning for a
Baby</a></div></li>
		<li><div class="btn" data-id="5"><a href="{!! route('faqs') !!}">FAQs</a></div></li>
		<li><div class="btn" data-id="6"><a href="{!! route('buy-now') !!}">Buy Now</a></div></li>
	</menu>
</div>

<div class="submenu-mask">
	<div class="submenu-container" @mouseout="hideSubMenu">
		<menu class="submenu" data-id="1">
			<li><div class="btn"><a href="{!! route('how-to-use') !!}">Taking a Test</a></div></li>
			<li><div class="btn"><a href="{!! route('pregnancy-test-calculator') !!}">Pregnancy Test Calculator</a></div></li>
			<li><div class="btn"><a href="{!! route('am-i-pregnant') !!}">Am I Pregnant? Symptoms</a></div></li>
			
		</menu>
		<menu class="submenu" data-id="2">
			<li><div class="btn"><a href="{!! route('your-test-results') !!}">Reading Your Test</a></div></li>
			<li><div class="btn"><a href="{!! route('positive-result') !!}">Positive Result</a></div></li>
			<li><div class="btn"><a href="{!! route('negative-result') !!}">Negative Result</a></div></li>
			<li><div class="btn"><a href="{!! route('due-date-calculator') !!}">Due Date Calculator</a></div></li>
		</menu>
		<menu class="submenu" data-id="3">
			<li><div class="btn"><a href="{!! route('our-pregnancy-tests') !!}">FIRST RESPONSE&#8482; Tests</a></div></li>
			<li><div class="btn"><a href="{!! route('accuracy') !!}">FIRST RESPONSE&#8482; Accuracy</a></div></li>
			<li><div class="btn"><a href="{!! route('pre-seed') !!}">PRE&#8209;SEED&#8482; Fertility&#8209;Friendly Personal Lubricant</a></div></li>
		</menu>
		<div class="submenu" data-id="4">
			<ul class="colmenu">
				<li>
					<div class="heading">Contraception</div>
					<menu class="termenu">
						<li><a href="{!! route('contraception-advice') !!}">Contraception Advice</a></li>
					</menu>
				</li>
				<li>
					<div class="heading">Our Bodies</div>
					<menu class="termenu">
						<li><a href="{!! route('the-female-body') !!}">The Female Body</a></li>
						<li><a href="{!! route('the-male-body') !!}">The Male Body</a></li>
						<li><a href="{!! route('your-fertile-time') !!}">Your Fertile Time</a></li>
						<li><a href="{!! route('ovulation-calculator') !!}">Ovulation Calculator</a></li>
					</menu>
				</li>
				<li>
					<div class="heading">Diet, Lifestyle &amp; Stress</div>
					<menu class="termenu">
						<li><a href="{!! route('the-right-diet') !!}">The Right Diet</a></li>
						<li><a href="{!! route('vitamins-and-minerals') !!}">Vitamins and Minerals</a></li>
						<li><a href="{!! route('a-healthy-lifestyle') !!}">A Healthy Lifestyle</a></li>
						<li><a href="{!! route('stress-management') !!}">Stress Management</a></li>
						<li><a href="{!! route('positive-mind-plan') !!}">Positive Mind Plan</a></li>
					</menu>
				</li>
				<li>
					<div class="heading">Sex &amp; Relationships</div>
					<menu class="termenu">
						<li><a href="{!! route('sex') !!}">Sex</a></li>
						<li><a href="{!! route('your-relationship') !!}">Your Relationship</a></li>
						{{-- <li><a href="{!! route('working-together') !!}">Working Together</a></li> --}}
					</menu>
				</li>
				<li>
					<div class="heading">Struggling to Conceive?</div>
					<menu class="termenu">
						<li><a href="{!! route('conception-advice') !!}">Conception Advice</a></li>
						<li><a href="{!! route('zita-west-videos') !!}">Advice from Zita West</a></li>
					</menu>
				</li>
				<li>
					<div class="heading">Pregnancy</div>
					<menu class="termenu">
						<li><a href="{!! route('stages-of-pregnancy') !!}">Stages of Pregnancy</a></li>
						<li><a href="{!! route('tips-for-a-healthy-pregnancy') !!}">Tips for a Healthy Pregnancy</a></li>
					</menu>
				</li>
			</ul>
		</div>
	</div>
</div>
