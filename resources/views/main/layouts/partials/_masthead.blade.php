<div class="masthead">
	@include('main.layouts.partials._shim-1920-600')
	@include('main.layouts.partials._shim-750-310')
	<img src="{{{$image_path}}}/headers/{{{$masthead_image}}}-large.jpg?{{{$version}}}" srcset="
	{{{$image_path}}}/headers/{{{$masthead_image}}}-small.jpg?{{{$version}}} 240w,
	{{{$image_path}}}/headers/{{{$masthead_image}}}-medium.jpg?{{{$version}}} 540w,
	{{{$image_path}}}/headers/{{{$masthead_image}}}-large.jpg?{{{$version}}} 1119w"
	alt="{{{$alt}}}" class="base">
	<img src="{{{$image_path}}}/headers/mask.png?{{{$version}}}" class="mask" alt="Image Mask"/>
	@if (isset($preSeed))
	<img src="{{{$image_path}}}/headers/swoosh-blue.png?{{{$version}}}" class="swoosh" alt="PRE SEED&#8482; branding curve"/>
	@else
	<img src="{{{$image_path}}}/headers/swoosh.png?{{{$version}}}" class="swoosh" alt="FIRST RESPONSE&#8482; branding curve"/>
	@endif
</div>