<div class="mobile-menu">
	<div class="bg"></div>
	<div class="bar"></div>
	<img src="{{{$image_path}}}/cross.svg" class="cross" alt="Close Menu" @click="closeMobileMenu" nopin="nopin"/>
	<a href="{!! route('home') !!}"><img src="{{{$image_path}}}/first-response-logo.png?{{{$version}}}" class="logo" alt="FIRST RESPONSE&#8482; FERTILITY"/></a>
	<div class="mask">
		<menu class="menu top">
			<li class="l2"><a href="#" data-id="1" class="sub" @click="openSubMenu">How to Use</a></li>
			<li class="l2"><a href="#" data-id="2" class="sub" @click="openSubMenu">Your Test Results</a></li>
			<li class="l2"><a href="#" data-id="3" class="sub" @click="openSubMenu">Our Products</a></li>
			<li class="l2"><a href="#" data-id="4" class="sub" @click="openSubMenu">Planning for a Baby</a></li>
			<li class="l2"><a href="{!! route('faqs') !!}" data-id="5">FAQs</a></li>
			<li class="l2"><a href="{!! route('buy-now') !!}" data-id="6">Buy Now</a></li>
		</menu>

		<menu class="menu sub" data-id="1">
			<li class="l2"><a href="{!! route('how-to-use') !!}" data-id="1">Taking a Test</a></li>
			<li class="l2"><a href="{!! route('pregnancy-test-calculator') !!}" data-id="2">Pregnancy Test Calculator</a></li>
			<li class="l2"><a href="{!! route('am-i-pregnant') !!}" data-id="3">Am I Pregnant? Symptoms</a></li>
			
			<li class="l2"><a href="#" data-id="x" class="return" @click="closeSubMenu">Back to Main Menu</a></li>
		</menu>

		<menu class="menu sub" data-id="2">
			<li class="l2"><a href="{!! route('your-test-results') !!}" data-id="1">Reading Your Test</a></li>
			<li class="l2"><a href="{!! route('positive-result') !!}" data-id="2">Positive Result</a></li>
			<li class="l2"><a href="{!! route('negative-result') !!}" data-id="3">Negative Result</a></li>
			<li class="l2"><a href="{!! route('due-date-calculator') !!}" data-id="4">Due Date Calculator</a></li>
			<li class="l2"><a href="#" data-id="x" class="return" @click="closeSubMenu">Back to Main Menu</a></li>
		</menu>

		<menu class="menu sub" data-id="3">
			<li class="l2"><a href="{!! route('our-pregnancy-tests') !!}" data-id="1">FIRST RESPONSE&#8482; Tests</a></li>
			<li class="l2"><a href="{!! route('accuracy') !!}" data-id="2">FIRST RESPONSE&#8482; Accuracy</a></li>
			<li class="l2"><a href="{!! route('pre-seed') !!}" data-id="2">PRE&#8209;SEED&#8482; Fertility&#8209;Friendly Personal&nbsp;Lubricant</a></li>
			<li class="l2"><a href="#" data-id="x" class="return" @click="closeSubMenu">Back to Main Menu</a></li>
		</menu>

		<menu class="menu sub" data-id="4" :show="terShow">
			<li class="l2 ter"><a href="#" class="arr" data-id="1" @click="toggleTerMenu">Contraception</a>
				<menu class="ter" data-id="1">
					<li class="l3"><a href="{!! route('contraception-advice') !!}">Contraception Advice</a></li>
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="2" @click="toggleTerMenu">Our Bodies</a>
				<menu class="ter" data-id="2">
					<li class="l3"><a href="{!! route('the-female-body') !!}">The Female Body</a></li>
					<li class="l3"><a href="{!! route('the-male-body') !!}">The Male Body</a></li>
					<li class="l3"><a href="{!! route('your-fertile-time') !!}">Your Fertile Time</a></li>
					<li class="l3"><a href="{!! route('ovulation-calculator') !!}">Ovulation Calculator</a></li>
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="3" @click="toggleTerMenu">Diet, Lifestyle &amp; Stress</a>
				<menu class="ter" data-id="3">
					<li class="l3"><a href="{!! route('the-right-diet') !!}">The Right Diet</a></li>
					<li class="l3"><a href="{!! route('vitamins-and-minerals') !!}">Vitamins and Minerals</a></li>
					<li class="l3"><a href="{!! route('a-healthy-lifestyle') !!}">A Healthy Lifestyle</a></li>
					<li class="l3"><a href="{!! route('stress-management') !!}">Stress Management</a></li>
					<li class="l3"><a href="{!! route('positive-mind-plan') !!}">Positive Mind Plan</a></li>
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="4" @click="toggleTerMenu">Sex &amp; Relationships</a>
				<menu class="ter" data-id="4">
					<li class="l3"><a href="{!! route('sex') !!}">Sex</a></li>
					<li class="l3"><a href="{!! route('your-relationship') !!}">Your Relationship</a></li>
					{{-- <li class="l3"><a href="{!! route('working-together') !!}">Working Together</a></li> --}}
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="5" @click="toggleTerMenu">Struggling to Conceive?</a>
				<menu class="ter" data-id="5">
					<li class="l3"><a href="{!! route('conception-advice') !!}">Conception Advice</a></li>
					<li class="l3"><a href="{!! route('zita-west-videos') !!}">Advice from Zita West</a></li>
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="6" @click="toggleTerMenu">Pregnancy</a>
				<menu class="ter" data-id="6">
					<li class="l3"><a href="{!! route('stages-of-pregnancy') !!}">Stages of Pregnancy</a></li>
					<li class="l3"><a href="{!! route('tips-for-a-healthy-pregnancy') !!}">Tips for a Healthy Pregnancy</a></li>
				</menu>
			</li>
			<li class="l2"><a href="#" data-id="x" class="return" @click="closeSubMenu">Back to Main Menu</a></li>
		</menu>

	</div>
</div>