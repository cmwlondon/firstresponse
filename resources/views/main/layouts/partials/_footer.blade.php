<ul>
	<li><a href="{!! route('ovulation-calculator') !!}">Ovulation Calculator</a></li>
	<li>|</li>
	<li><a href="{!! route('contact') !!}">Get in touch</a></li>
	<li>|</li>
	 
	<li><a href="{!! route('cookie-notice') !!}">Cookie Notice</a></li>
	<li>|</li>
	<li><a href="{!! route('privacy-policy') !!}">Privacy Policy</a></li>
	<li>|</li>
	{{--
	<li><a href="{!! route('policies') !!}">Policies</a></li>
	<li>|</li>--}}
	<li><a href="{!! route('terms') !!}">Terms</a></li>
	<li>|</li>
	<li><a href="http://www.churchdwight.co.uk" target="_blank">Church&nbsp;&amp;&nbsp;Dwight</a></li>
</ul>
{{ print_r(config('frf.pageClass'),true) }}
{{--
	@if ($pageClass == 'planning')
	<p class="mt1">Medical information provided by Zita West, Midwife, Fertility and Pregnancy Expert.</p>
@endif
--}}
<!-- OneTrust Cookies Settings button start -->
<button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button>
<!-- OneTrust Cookies Settings button end -->

<p class="mt1">&copy; Copyright Church &amp; Dwight UK Ltd. 2021.<br/>All rights reserved. <br class="hidden vis-sm"/>FIRST RESPONSE&#8482; is a trademark of Church &amp; Dwight Co., Inc</p>