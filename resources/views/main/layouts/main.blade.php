<!doctype html>
<html class="no-js debug" lang="en">
	<head>
		<!-- OneTrust Cookies Consent Notice start for firstresponsefertility.com -->
		<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="9cd0d3d0-3fa6-4edf-88ba-14ca5445313f" ></script>
		<script type="text/javascript">
		function OptanonWrapper() { }
		</script>
		<!-- OneTrust Cookies Consent Notice end for firstresponsefertility.com -->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<title>{!! $meta['title'] !!}</title>
		<meta name="description" content="{{{$meta['desc']}}}" />
		<meta name="keywords" content="{{{$meta['keywords']}}}"/>
		<meta name="keyphrases" content="{{{$meta['keyphrases']}}}"/>

		@if (App::environment() == 'local')
			<link rel="stylesheet" href="/css/main/styles.css?{{{$version}}}" toRefresh="/css/main/styles.css"/>
			<link rel="stylesheet" href="/css/main/print.css?{{{$version}}}" toRefresh="/css/main/print.css"/>
		@else
			<link rel="stylesheet" href="/css/main/styles.css?{{{$version}}}"/>
			<link rel="stylesheet" href="/css/main/print.css?{{{$version}}}"/>
		@endif
		<!-- <link rel="preload" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" as="font"> -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet">
		
		@if (isset($pageViewCSS) && $pageViewCSS != '')
			@if (App::environment() == 'local')
            	<link rel="stylesheet" href="/css/{{{$pageViewCSS}}}.css?{{{$version}}}" toRefresh="/css/{{{$pageViewCSS}}}.css"/>
            @else
            	<link rel="stylesheet" href="/css/{{{$pageViewCSS}}}.css?{{{$version}}}"/>
            @endif
		@endif


		@if (isset($noindex))
			<meta name="robots" content="noindex" />
		@endif

		<link rel="canonical" href="{{{$meta['link']}}}" />
		<meta property="og:locale" content="en_GB" />
		<meta property="og:type" content="{{{$meta['pagetype']}}}" />
		<meta property="og:title" content="{{{$meta['title']}}}" />
		<meta property="og:description" content="{{{$meta['desc']}}}" />
		<meta property="og:url" content="{{{$meta['link']}}}" />
		<meta property="og:site_name" content="First Response Fertility" />
		<meta property="og:image" content="{{{$meta['image']}}}" />

		@include('main.layouts.partials._favicon')

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/animations-ie-fix.css">
			<style type="text/css">
			    #switch-group {display:none;}
			</style>
		<![endif]-->

		<!--[if gte IE 9]>
		  <style type="text/css">
		    .gradient {
		       filter: none!important;
		    }
		  </style>
		<![endif]-->
	</head>

	<body class="{{{ $pageClass }}}">
		<a name="top"></a>

		@if (App::environment() == 'local')
		<div id="css-debug" class="hidden"><span class="hidden vis-xsm">Size:&nbsp;xsm</span><span class="hidden vis-sm">Size:&nbsp;sm</span><span class="hidden vis-md">Size:&nbsp;md</span><span class="hidden vis-lg">Size:&nbsp;lg</span><span class="hidden vis-xlg">Size:&nbsp;xlg</span><span class="hidden vis-xxlg">Size:&nbsp;xxlg</span><span class="hidden vis-xxxlg">Size:&nbsp;xxxlg</span></div>
		@endif
		
		<div id="cookieDisclaimer" class="cookie-disclaimer" :class="cookieClass">To give you the best possible experience this site uses cookies. By continuing to use this website you are giving consent to cookies being used. For more information visit our <a href="/cookie-notice">Cookie&nbsp;Notice</a>.<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64.6 64.6" enable-background="new 0 0 64.6 64.6" xml:space="preserve" class="btn-close" @click="closeCookieDisclaimer">
			    <line x1="3.5" y1="3.5" x2="61.1" y2="61.1"/>
			    <line x1="61.1" y1="3.5" x2="3.5" y2="61.1"/>
			</svg></div>

		<header id="headerWrapper" :show="show">
			@yield('header')
		</header>

		<div id="contentWrapper">
			<section class="row full">
				@yield('content')
			</section>
		</div>

		<div id="footerWrapper">
			@yield('footer')
		</div>

		<div id="mobileMenuWrapper" :class="{visible:isVisible}" :show="show">
			@include('main.layouts.partials._mobile-menu')
		</div>

		<script>
			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif
			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif
		</script>

		{!! Html::script('js/libs/requirejs/require.js', ['data-main' => '/js/main/main.min.js']) !!}

		@include('main.layouts.partials._analytics')
	</body>
</html>
