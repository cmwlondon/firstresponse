@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner buy-now">
		<div class="columns span-12 intro">
			<h1>Buy Now</h1>
			<p class="mt2 mb2">Buy a FIRST RESPONSE&#8482; Pregnancy Test online at:</p>
		</div>

		<div class="columns span-4 md-6 sm-12">
			<img src="data:image/false;base64,R0lGODlhCgAEAIAAAP///wAAACH5BAAAAAAALAAAAAAKAAQAAAIFhI+py1gAOw==" class="shim"/>
			<a href="{!! __('links.buy-boots') !!}" target="_blank" rel="nofollow"><img src="{{{$image_path}}}/stores/boots.svg?{{{$version}}}" alt="Boots" class="logo" @click="track"/></a>
		</div>

		<div class="columns span-4 md-6 sm-12">
			<img src="data:image/false;base64,R0lGODlhCgAEAIAAAP///wAAACH5BAAAAAAALAAAAAAKAAQAAAIFhI+py1gAOw==" class="shim"/>
			<a href="{!! __('links.buy-asda') !!}" target="_blank" rel="nofollow"><img src="{{{$image_path}}}/stores/asda.svg?{{{$version}}}" alt="ASDA" class="logo" @click="track"/></a>
		</div>

		<div class="columns span-4 md-6 sm-12">
			<img src="data:image/false;base64,R0lGODlhCgAEAIAAAP///wAAACH5BAAAAAAALAAAAAAKAAQAAAIFhI+py1gAOw==" class="shim"/>
			<a href="{!! __('links.buy-superdrug') !!}" target="_blank" rel="nofollow"><img src="{{{$image_path}}}/stores/superdrug.svg?{{{$version}}}" alt="Superdrug" class="logo" @click="track"/></a>
		</div>

		<div class="columns span-4 md-6 sm-12">
			<img src="data:image/false;base64,R0lGODlhCgAEAIAAAP///wAAACH5BAAAAAAALAAAAAAKAAQAAAIFhI+py1gAOw==" class="shim"/>
			<a href="{!! __('links.buy-lloyds') !!}" target="_blank" rel="nofollow"><img src="{{{$image_path}}}/stores/llyods-pharmacy.svg?{{{$version}}}" alt="Lloyds Pharmacy" class="logo" @click="track"/></a>
		</div>

		<div class="columns span-4 md-6 sm-12">
			<img src="data:image/false;base64,R0lGODlhCgAEAIAAAP///wAAACH5BAAAAAAALAAAAAAKAAQAAAIFhI+py1gAOw==" class="shim"/>
			<a href="{!! __('links.buy-amazon') !!}" target="_blank" rel="nofollow"><img src="{{{$image_path}}}/stores/amazon.svg?{{{$version}}}" alt="Amazon" class="logo" @click="track"/></a>
		</div>

		<div class="columns span-4 md-6 sm-12">
			<img src="data:image/false;base64,R0lGODlhCgAEAIAAAP///wAAACH5BAAAAAAALAAAAAAKAAQAAAIFhI+py1gAOw==" class="shim"/>
			<a href="{!! __('links.buy-chemist-direct') !!}" target="_blank" rel="nofollow"><img src="{{{$image_path}}}/stores/chemist-direct.svg?{{{$version}}}" alt="Chemist Direct" class="logo" @click="track"/></a>
		</div>


		<div class="columns span-12">
			<p>You can also find FIRST RESPONSE&#8482; Pregnancy Tests at local pharmacies.</p>
		</div>
	
	</div>

	<div class="cream">
		<div class="maxInner bottom-space">
			<div class="columns span-12 mt4 mb2">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('how-to-use') !!}" class="button twoline left"><span>How to Test</span>
			</a> <a href="{!! route('your-fertile-time') !!}" class="button twoline right"><span>Your Fertile Time</span></a></div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
