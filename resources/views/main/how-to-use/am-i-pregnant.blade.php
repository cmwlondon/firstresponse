@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
	@include('main.layouts.partials._masthead', ['masthead_image'=>'two-pregnancy-tests', 'alt'=>'Two Pregnancy Tests'])
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>Early Pregnancy Symptoms</h1>
			<p class="mt2">Knowing you are pregnant sooner affords you an early start on a healthier pregnancy for both you and your baby. Most women know the basic early signs of pregnancy, especially a missed period. But that's not the only sign. If you're wondering whether or not you're pregnant, it's probably time to take a pregnancy test – especially if you're experiencing any of the following early pregnancy symptoms.</p>
			<div class="outline-box mt4">
				<h2>Common pregnancy symptoms</h2>
				<h3>Pregnancy symptom - Morning Sickness</h3>
				<p>Or, afternoon sickness. Or, evening sickness. Nausea – with or without vomiting – can occur any time of day starting as early as three weeks after conception and it’s one of the most common early signs of pregnancy. These bouts can be caused by rapidly rising hormone levels, which cause the stomach to empty more slowly.</p>
				<h3>Pregnancy symptom - Frequent Urination</h3>
				<p>Many women find themselves taking more trips to the bathroom, especially at night. These frequent trips only add to fatigue (also a symptom).</p>
				<h3>Pregnancy symptom - Fatigue</h3>
				<p>Early on, progesterone levels peak, making you feel sleepy. High progesterone &mdash; combined with lower blood sugar levels, lower blood pressure and increased blood production &mdash; may leave you feeling drained. Take it easy and get that sleep while you can.</p>
				<h3>Pregnancy Symptom - Tender, Swollen Breasts</h3>
				<p>Often, one of the first early pregnancy signs women notice is that their breasts may feel tingly or sore as early as two to three weeks after conception. They may also feel fuller or heavier.</p>
				<h3>Pregnancy Symptom - Food Cravings or Aversions</h3>
				<p>Like most early signs of pregnancy, food preferences are typically thought to be the result of hormonal changes, especially during the dramatic shifts of the first trimester.</p>
				<h2 class="mt2">Pregnancy Symptoms - Less Common Signs of Pregnancy</h2>
				<h3>Pregnancy Symptom - Slight Bleeding or Cramping</h3>
				<p>Sometimes women notice a small amount of bleeding, known as implantation bleeding, very early on in pregnancy. This happens when a fertilized egg attaches to the lining of the uterus around 10-14 days after fertilisation. Sometimes mistaken for a period, it&#39;s usually lighter, spottier and happens earlier.</p>
				<h3>Pregnancy Symptom - Mood Swings</h3>
				<p>Another effect of those raging hormones in the first trimester, you may feel unusually emotional or weepy.</p>
				<h3>Pregnancy Symptom - Dizziness</h3>
				<p>Low blood pressure and dilating blood vessels early in pregnancy, along with low blood sugar, can cause you to feel lightheaded. Take care to keep yourself safe.</p>
				<h3>Pregnancy Symptom - Constipation</h3>
				<p>Increased production of progesterone means food makes its way through your intestines slower, leading to constipation. The iron in prenatal vitamins often exaggerates the problem.</p>
				<h3>When to take a pregnancy test?</h3>
				<p>All of these early signs are a good guide to help you determine if you may be pregnant, but they don’t provide definitive answers. If you are experiencing any of them, you may wish to take a pregnancy test. The <a href="{!! route('our-pregnancy-tests') !!}#early-results-pregnancy-test">FIRST RESPONSE&#8482; Early Result Pregnancy Test</a> can capture scant amounts of the pregnancy hormone; no brand gives you a result sooner.</p>
			</div>
		</div>

	</div>

	<div class="cream">
		<div class="maxInner">
			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<p class="button-group"><a href="{!! route('your-test-results') !!}" class="button twoline left"><span>Pregnancy Test<br/>Results</span>
				</a> <a href="{!! route('stages-of-pregnancy') !!}" class="button twoline right"><span>Stages of<br/>Pregnancy</span></a></p>
			</div>
			
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
