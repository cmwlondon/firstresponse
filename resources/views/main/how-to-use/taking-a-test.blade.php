@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
	@include('main.layouts.partials._masthead', ['masthead_image'=>'woman-looking-at-pregnancy-test', 'alt'=>'Woman looking at a pregnancy test'])
@endsection

@section('content')
	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>Could I be pregnant?</h1>
			<p class="mt2">If you're not quite sure, you can find a summary of early pregnancy symptoms <a href="{!! route('am-i-pregnant') !!}">here</a>. If you're experiencing any of these early pregnancy symptoms or have missed a period, we recommend you take a pregnancy test. Click <a href="{!! route('pregnancy-test-calculator') !!}"><span>here</a> to find out the best time to take a pregnancy test.</p>
		</div>

		<div class="columns span-12 outline-box">
			<h2>Taking your FIRST RESPONSE&#8482; Early Result Pregnancy Test</h2>
			<p class="mt2">The FIRST RESPONSE&#8482; Early Result Pregnancy Test is designed to detect the pregnancy hormone (hCG) as early as 6 days before your missed period. Therefore you can use FIRST RESPONSE&#8482; Early Result Pregnancy Tests as early as six days before the day of your missed period. For example, if you expect your period on the 21st of the month, you can test as early as the 16th day of the month. You can use FIRST RESPONSE&#8482; Early Result Pregnancy Tests at any time of the day.</p>
			<img src="{{{$image_path}}}/could-i-be-pregnant/stick.png?{{{$version}}}" class="w100 mt2"/>

			<div class="w100 cf mt4">
				<div class="block" data-id="1">
					<div class="grad"></div>
					<img src="{{{$image_path}}}/could-i-be-pregnant/early-results/instruction-1.png?{{{$version}}}" alt=""/>
					<span>Holding the FIRST RESPONSE&#8482; Early Result Pregnancy Test stick by the thumb grip with the absorbent tip pointing downwards and the result window away from your body, place the absorbent tip in your urine stream for 5 seconds only.</span>
				</div>

				<div class="block" data-id="2">
					<div class="grad"></div>
					<img src="{{{$image_path}}}/could-i-be-pregnant/early-results/instruction-2.png?{{{$version}}}" alt=""/>
					<span>With the aborbent tip still pointing downward replace the overcap* and lay the stick on a flat surface with the result window facing up.</span>
				</div>

				<div class="block" data-id="3">
					<div class="grad"></div>
					<img src="{{{$image_path}}}/could-i-be-pregnant/early-results/instruction-3.png?{{{$version}}}" alt=""/>
					<span>Wait 3 minutes until reading the result.</span>
				</div>

				<div class="block" data-id="4">
					<div class="grad"></div>
					<img src="{{{$image_path}}}/could-i-be-pregnant/early-results/instruction-4.png?{{{$version}}}" alt=""/>
					<span>You may also collect your urine in a clean, dry cup and immerse the entire absorbent tip in the urine for 5 seconds.</span>
				</div>
			</div>
		</div>

		<div class="columns span-12 outline-box">
			<h2>Taking your FIRST RESPONSE&#8482; Rapid Result Pregnancy Test</h2>

			<div class="w100 cf mt4">
				<div class="block" data-id="1">
					<div class="grad"></div>
					<img src="{{{$image_path}}}/could-i-be-pregnant/rapid-results/instruction-1.png?{{{$version}}}" alt=""/>
					<span>Holding the FIRST RESPONSE&#8482; Rapid Result Pregnancy Test stick by the thumb grip with the absorbent tip pointing downward and the result window away from your body, place the absorbent tip in your urine stream for 5 seconds only.</span>
				</div>

				<div class="block" data-id="2">
					<div class="grad"></div>
					<img src="{{{$image_path}}}/could-i-be-pregnant/rapid-results/instruction-2.png?{{{$version}}}" alt=""/>
					<span>With the aborbent tip still pointing downwards replace the overcap* and lay the stick on a flat surface with the result window facing up.</span>
				</div>

				<div class="block" data-id="3">
					<div class="grad"></div>
					<img src="{{{$image_path}}}/could-i-be-pregnant/rapid-results/instruction-3.png?{{{$version}}}" alt=""/>
					<span>Wait 45 seconds until reading the result.</span>
				</div>

				<div class="block" data-id="4">
					<div class="grad"></div>
					<img src="{{{$image_path}}}/could-i-be-pregnant/rapid-results/instruction-4.png?{{{$version}}}" alt=""/>
					<span>You may also collect your urine in a clean, dry cup and immerse the entire absorbent tip in the urine for 20 seconds.</span>
				</div>
			</div>

			<p class="mt2 cf">*Replacement of the overcap is necessary for the proper functioning of the test.</p>
		</div>

	</div>

	<div class="cream">
		<div class="maxInner">
			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<p class="button-group"><a href="{!! route('your-test-results') !!}" class="button twoline left"><span>Pregnancy Test<br/>Results</span>
				</a> <a href="{!! route('faqs') !!}" class="button twoline right"><span>FAQs</span></a></p>
			</div>
			
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
