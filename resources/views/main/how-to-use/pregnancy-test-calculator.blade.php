@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')

	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>Pregnancy Test Calculator</h1>
			<p class="mt2">Use this pregnancy test calculator to help you decide when to take your FIRST RESPONSE&#8482; Early Result Pregnancy Test. Avoid any disappointment by testing before you have sufficient hormone in your urine. Find out when to take a pregnancy test by completing your information below:</p>
		</div>

		<div id="calculator" class="row full">

			<div class="columns span-12 calculator-wrap">

				<h2>When can i take the test?</h2>

				<div class="form">

					<div class="form-row">
						<label for="datepicker_0">First day of last menstrual period</label>
						<div class="calendar-wrap">
							<input name="first_day" v-model="first_day.value" @change="onChange" type="text" id="datepicker_0">
							<div class="calendar-inner">
								<img src="{{{$image_path}}}/pregnancy-test-calculator/calendar.jpg?{{{$version}}}" alt="">
							</div>
						</div>
					</div>

					<div class="form-row">
						<label for="average_cycle">Average cycle length</label>
						<div class="control-wrap">
							<input type="text" :value="average_cycle.rendered" id="average_cycle" @blur="checkAvgValue" @keyup.enter="checkAvgValue">
							<div class="control-inner">
								<div class="up" @click="incAvr"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 10" style="enable-background:new 0 0 15 10;" xml:space="preserve">
								<path d="M7.5,0C8,0,8.6,0.3,8.9,0.7l5.6,7.2c0.5,0.7,0.4,1.2,0.3,1.4c-0.1,0.2-0.4,0.7-1.3,0.7H1.5c-0.9,0-1.2-0.4-1.3-0.7C0,9.1-0.1,8.6,0.4,7.9l5.6-7.2C6.4,0.3,7,0,7.5,0z"/>
								</svg></div>
								<div class="down" @click="decAvr"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 10" style="enable-background:new 0 0 15 10;" xml:space="preserve">
								<path d="M7.5,10C7,10,6.4,9.7,6.1,9.3L0.4,2.1C-0.1,1.4,0,0.9,0.1,0.7C0.3,0.4,0.6,0,1.5,0h12.1c0.9,0,1.2,0.4,1.3,0.7c0.1,0.2,0.2,0.8-0.3,1.4L8.9,9.3C8.6,9.7,8,10,7.5,10z"/>
								</svg></div>
							</div>
						</div>
						
					</div>

					<div class="form-row">
						<h3><strong>When did you last ovulate?</strong></h3>
						<div class="radio-row">
							<input type="radio" v-model="picked" value="last_ovulated_checkbox" checked id="one" class="display-block checkbox-style last-ovulated">
							<label for="one">I ovulated on</label>
						</div>
						<div class="calendar-wrap">
							<input v-model="last_ovulated.value" @change="onChange" type="text" :disabled="last_ovulated.disabled" id="datepicker_1">
							<div class="calendar-inner">
								<img src="{{{$image_path}}}/pregnancy-test-calculator/calendar.jpg?{{{$version}}}" alt="">
							</div>
						</div>
					</div>

					<div class="form-row">
						<div class="radio-row">
							<input type="radio" v-model="picked" value="not_sure_checkbox" id="two" class="display-block checkbox-style not-sure">
							<label for="two">Not sure, calculate luteal <br class="hidden vis-sm"/>phase length</label>
						</div>
						<div class="control-wrap">
							<input type="text" :value="luteal_phase_length.rendered" :disabled="this.luteal_phase_length.disabled">
							<div class="control-inner">
								<div class="up" @click="incLuteal"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 10" style="enable-background:new 0 0 15 10;" xml:space="preserve">
								<path d="M7.5,0C8,0,8.6,0.3,8.9,0.7l5.6,7.2c0.5,0.7,0.4,1.2,0.3,1.4c-0.1,0.2-0.4,0.7-1.3,0.7H1.5c-0.9,0-1.2-0.4-1.3-0.7C0,9.1-0.1,8.6,0.4,7.9l5.6-7.2C6.4,0.3,7,0,7.5,0z"/>
								</svg></div>
								<div class="down" @click="decLuteal"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 10" style="enable-background:new 0 0 15 10;" xml:space="preserve">
								<path d="M7.5,10C7,10,6.4,9.7,6.1,9.3L0.4,2.1C-0.1,1.4,0,0.9,0.1,0.7C0.3,0.4,0.6,0,1.5,0h12.1c0.9,0,1.2,0.4,1.3,0.7c0.1,0.2,0.2,0.8-0.3,1.4L8.9,9.3C8.6,9.7,8,10,7.5,10z"/>
								</svg></div>
							</div>
						</div>
						<p class="mt1">(If you are not sure, use 14. <br class="hidden vis-sm"/>This is the average.)</p>
					</div>

					<div class="button-row">
						<button type="button" class="button" @click="calc">calculate</button>
					</div>
				</div>
			</div>

			<div id="result" class="columns span-12 result-wrap" :show="resultDisplay">
				<h3 v-html="resultMonth"></h3>
			    <table class="results" cellpadding="0" cellspacing="0">
			      <tr>
			        <th>S</th>
			        <th>M</th>
			        <th>T</th>
			        <th>W</th>
			        <th>T</th>
			        <th>F</th>
			        <th>S</th>
			      </tr>
			      <tbody v-html="resultTable">
			        
			      </tbody>
			    </table>
			    <div class="results-footer">
					<div class="key light"><span>FIRST RESPONSE&#8482; detects the pregnancy hormone 5 days before the day of your expected period.</span></div>
					<div class="key dark"><span>Best days to test.</span></div>
					<div class="disclaimer">* Always read the leaflet, in laboratory testing, FIRST RESPONSE&#8482; Early Result Pregnancy Test detected hormone levels consistent with pregnancy in 76% of women 6 days before the day of missed period, in 96% of women 5 days before the day of missed period, and in >99% of women 4 or fewer days before the day of missed period.</div>
				</div>
			</div>

			<div class="columns span-12 solid-box warning" :show="resultDisplay">
				<p>Please note: These dates are approximate and are best used as a guideline for when to begin testing with the FIRST RESPONSE&#8482; Early Result Pregnancy Test. Additionally, these dates are based on a regular menstrual cycle. If your menstrual cycle is irregular, you may have more difficulty pinpointing the day of testing based on the calendar alone.</p>
			</div>

		</div>
	</div>

	<div class="cream">
		<div class="maxInner">

			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('how-to-use') !!}" class="button twoline left"><span>How to Test</span>
				</a> <a href="{!! route('ovulation-calculator') !!}" class="button twoline right"><span>Ovulation<br/>Calculator</span></a></div>
			</div>
			
		</div>
	</div>

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
