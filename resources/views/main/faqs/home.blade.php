@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div id="faqsWrapper" class="maxInner">
		<div id="searchGroup" class="columns span-12 intro">
			<h1>Your Questions</h1>
			<div id="searchBox">
				<form id="searchFAQs" action="#">
					<input type="text" v-model="terms" placeholder="SEARCH FAQS"/>
					<button type="submit" class="search" @click.prevent="doSearch"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" enable-background="new 0 0 80 80" xml:space="preserve">
<path d="M78.6,69.9c-2,3-6,3.7-8.9,1.7L43.3,52.9c-1.9-1.3-2.8-3.5-2.8-5.6
	c-3.4,3.7-7.9,6.2-12.8,7.1c-6.1,1.1-12.3-0.2-17.4-3.8c-5.1-3.6-8.5-9-9.7-15.3c-1.1-6.3,0.2-12.6,3.8-17.8
	c7.3-10.8,21.9-13.5,32.4-6c9.6,6.8,12.7,19.6,7.7,30c1.9-0.8,4.3-0.7,6.1,0.6L77,60.8C79.9,62.8,80.6,66.9,78.6,69.9z M39.6,28.1
	c-0.8-4.4-3.2-8.1-6.7-10.7c-7.4-5.2-17.5-3.3-22.6,4.2c-5.1,7.5-3.3,17.9,4.1,23.1c7.4,5.2,17.5,3.3,22.6-4.2
	C39.4,36.9,40.3,32.5,39.6,28.1z"/>
</svg></button>
				</form>

				
			</div>
			<p id="noResults" class="hidden">Sorry no results found</p>
		</div>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">HOW DO THE FIRST RESPONSE&#8482; PREGNANCY TESTS WORK?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>From the earliest moments of conception, your body is already starting to undergo monumental changes. One of the very first is production of the pregnancy hormone, hCG (human Chorionic Gonadotropin), which can be detected in your blood and in your urine.</p>
					<p>The amount of hCG in your body increases rapidly. It doubles every 36 to 48 hours as your pregnancy progresses, reaching its peak at eight to ten weeks. FIRST RESPONSE&#8482; Pregnancy Tests work by detecting hCG levels in your urine. When a woman's urine comes in contact with the specially treated strip on a FIRST RESPONSE&#8482; Pregnancy Test stick, results appear within minutes, indicating whether or not hCG, the pregnancy hormone, has been detected.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">WHEN CAN/SHOULD I TAKE A PREGNANCY TEST?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>You can use the FIRST RESPONSE&#8482; Early Result Pregnancy Test any time of the day, up to five days before the day your period is due. Generally you don't have to use first morning urine, however, your first morning urine contains the highest level of the pregnancy hormone. Use our <a href="{!! route('pregnancy-test-calculator') !!}">test day calculator</a> to determine the best day for you to take a test.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">HOW DO I TAKE A FIRST RESPONSE&#8482; PREGNANCY TEST?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>FIRST RESPONSE&#8482; Pregnancy Tests are simple to use - just hold the test stick in your urine stream for a few seconds and you will soon see the pink colour moving across the Result Window to indicate that the test is working. After the allocated waiting time, you can read your result. Follow the full instructions enclosed in the pack before carrying out the test. See our <a href="{!! route('how-to-use') !!}">how to test</a> page for more information.</p>
					<div class="columns span-10 before-1 after-1">
						<img src="{{{$image_path}}}/could-i-be-pregnant/stick.png?{{{$version}}}" class="w100 mt2" alt="Pregnancy test stick"/>
					</div>
					<div class="columns span-6 sm-12 md-12 outline-box mt2 mb0 result">
						<h2>Pregnant</h2>
						<hr/>
						<p class="block" data-id="1"><img src="{{{$image_path}}}/reading-your-test/pregnant-one-pink-line-one-faint-pink-line.jpg?{{{$version}}}" class="" alt="One pink line, one faint pink line"/><span>One pink line and one line lighter than the other in the pregnancy test result window means <strong>you are pregnant</strong>. Any positive test result (even a very faint line) shows that the pregnancy hormone (hCG) was detected.</span></p>
						<hr/>
						<p class="block" data-id="2"><img src="{{{$image_path}}}/reading-your-test/pregnant-two-pink-lines.jpg?{{{$version}}}" class="" alt="Two pink lines"/><span>Two pink lines in the pregnancy test result window means <strong>you are pregnant</strong>. A positive test result shows that the pregnancy hormone (hCG) was detected.</span></p>
					</div>

					<div class="columns span-6 sm-12 md-12 outline-box mt2 mb0 result">
						<h2>Not Pregnant</h2>
						<hr/>
						<p class="block" data-id="3"><img src="{{{$image_path}}}/reading-your-test/not-pregnant-one-pink-line.jpg?{{{$version}}}" class="" alt="One pink line"/><span>One pink line in the pregnancy test result window means <strong>you may not be pregnant</strong>, or it may be too early to tell.</span></p>
					</div>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">I HAVE WAITED LONGER THAN THE ALLOCATED WAITING TIME, AND STILL THERE ARE NO LINES IN THE RESULT WINDOW. IS THERE ANYTHING WRONG?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>If using the FIRST RESPONSE&#8482; Early Result Pregnancy Test your result should appear within three minutes.</p>
					<p>If using the FIRST RESPONSE&#8482; Rapid Result Pregnancy Test your results should appear within 45 seconds.</p>
					<p>There should be at least one pink line in the FIRST RESPONSE&#8482; Result Window after this time to indicate that the test is completed and you have done it correctly. If at 5 minutes there are no lines within the FIRST RESPONSE&#8482; Result Window the test is invalid. This could be due to insufficient urine on the Absorbent Tip or not holding the device with the Absorbent Tip pointing downward while replacing the Overcap. You should retest with another device, carefully following the instructions which can be found on this <a href="{!! route('how-to-use') !!}">page</a>.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">HOW SOON CAN A PREGNANCY BE DETECTED?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>Your doctor can perform blood tests that will accurately diagnose pregnancy several days before you missed your period, or your doctor may opt to conduct a urine test a day or so later. Home pregnancy tests, like the FIRST RESPONSE&#8482; Early Result Pregnancy Tests, are also a great option. FIRST RESPONSE&#8482; Early Result Pregnancy Tests can capture scant amounts of the pregnancy hormone. No test gives you an answer sooner than FIRST RESPONSE&#8482;.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">HOW DO I KNOW IF I AM PREGNANT OR NOT?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>The FIRST RESPONSE&#8482; Early Result Pregnancy Test has an easy to read test stick - two pink lines in the Result Window means you are pregnant, one pink line means you are not pregnant. The appearance of a second line, which may be lighter than the other, is a positive result. See our <a href="{!! route('your-test-results') !!}">results page</a> for more information on what your test result means.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">WILL THE RESULT CHANGE IF LEFT STANDING FOR A CERTAIN PERIOD OF TIME?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>The FIRST RESPONSE&#8482; Early Result Pregnancy Tests should be read as soon as possible after the allocated waiting time (3 minutes for FIRST RESPONSE&#8482; Early Result Pregnancy Test, 45 seconds for FIRST RESPONSE&#8482; Rapid Result Pregnancy Test). A positive result will last for at least 48 hours. However, a negative result may not last and should not be read after 10 minutes.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">THE LINE(S) IN THE RESULT WINDOW OF THE RAPID RESULT TEST TOOK LONGER THAN 45 SECONDS TO APPEAR. IS THIS A VALID RESULT?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>Yes, in some cases it may take longer than 45 seconds to obtain a result. The presence of at least one pink line in the FIRST RESPONSE&#8482; Result Window indicates a valid test.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">I CAN SEE A VERY FAINT SECOND PINK LINK IN THE RESULT WINDOW. AM I PREGNANT?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>The FIRST RESPONSE&#8482; Early Result Pregnancy Test has an easy to read test stick - two pink lines in the Result Window means you are pregnant, one pink line means you are not pregnant. The appearance of a second line, which may be lighter than the other, is a positive result.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">HOW ACCURATE ARE FIRST RESPONSE&#8482; TESTS?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>FIRST RESPONSE&#8482; Pregnancy Tests are over 99% accurate at detecting typical hormone levels in laboratory testing. No test can tell you sooner than FIRST RESPONSE&#8482; Early Result Pregnancy Test. In laboratory testing, FIRST RESPONSE&#8482; detected hormone levels consistent with pregnancy in 76% of women 6 days before the day of missed period, in 96% of women 5 days before the day of missed period, and in >99% of women 4 or fewer days before the day of missed period.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">DO MEDICATIONS OR ALCOHOL AFFECT THE TEST?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>Only medications that contain the pregnancy hormone (hCG) affect the result (for example, Pregnyl+++ and Pergonal++). This test should not be affected by hormone therapies containing clomiphene citrate (for example, Clomid+), alcohol, painkillers, antibiotics or the contraceptive pill.</p>
					<p>+++ Pregnyl is a registered trademark of Organon, Inc.<br/>++ Pergonal is a registered trademark used by Serona Laboratories Inc.<br/>+ Clomid is a registered trademark of Hoechst Marion Roussel.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">WHAT ARE THE PHYSICAL SYMPTOMS OF PREGNANCY?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>There are many symptoms that you may suffer from when pregnant including morning sickness, breast tenderness and fatigue. Find detailed information on each symptom <a href="{!! route('am-i-pregnant') !!}">here</a>.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">WHAT DO I DO IF THE RESULT IS NEGATIVE?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>You may not be pregnant, or it may be too early to tell. If you do not get your period within seven days, you should retest with another FIRST RESPONSE&#8482; Early Result Pregnancy Test. It is possible that either you miscalculated the length of your cycle or your urine may not have had enough pregnancy hormone for the test to give a positive result. If you retest and again no hCG is found, and your period still has not started, you should consult your doctor.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">I JUST FOUND OUT I'M PREGNANT. NOW WHAT?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>The first thing you should do is make an appointment to see your medical professional. Your midwife or doctor can provide you with much of the information you will need to know about the months ahead. Learning that you are pregnant should serve as encouragement for you to adopt healthy lifestyle habits. Avoid using drugs, cigarettes, alcohol and caffeine to the best of your ability. If you are currently taking medications (prescription or over-the-counter), be sure to discuss possible effects they may have on you and your baby when you see your midwife or doctor.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">I’VE HAD A POSITIVE RESULT. HOW MANY WEEKS PREGNANT AM I?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>The best way to accurately understand how many weeks pregnant you may be is to consult your doctor or midwife. The FIRST RESPONSE&#8482; <a href="{!! route('due-date-calculator') !!}">Due Date calculator</a> is also a handy tool to give you an estimated due date based on the date of your last period.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">WHAT CAN I DO ABOUT MORNING SICKNESS?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>Morning sickness can be one of the more unpleasant aspects of your pregnancy. However, you can do several things to ease the discomfort it causes, including:</p>
					<ul>
						<li>Get plenty of rest.</li>
						<li>Move slowly in the morning, as nausea may worsen with quick movements.</li>
						<li>Eat foods that you know agree with you to avoid the nausea often caused by an empty stomach. Avoid irritants such as caffeine or spicy foods.</li>
						<li>Keep a diary each day to help you determine what improves your condition, as well as what seems to make it worse.</li>
						<li>Ginger has been shown to help relieve sickness. It's safe to use in pregnancy and you can take it in several forms. Try ginger tea, ginger ale or ginger biscuits.</li>
						<li>Small, frequent snacks can help alleviate your symptoms. Avoid large meals, especially those high in fat, as they'll put a greater strain on your digestive system.</li>
						<li>Don't let your stomach remain empty for more than a couple of hours. Have a dry crispbread, cracker or piece of plain toast to keep your system “ticking over”.</li>
					</ul>
					<p>If you find yourself losing weight or suffering from dehydration due to your morning sickness, consult your doctor to make sure you are still absorbing all necessary nutrition for you and your baby. In some cases, your doctor may prescribe medication to ease your discomfort.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">CAN I STILL EXERCISE DURING PREGNANCY?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>You must discuss this with your doctor. Generally, however, exercise will prove beneficial both to you and your unborn baby, as it improves muscle strength, muscle tone, and flexibility. It promotes a sense of well-being and enhances self-esteem. Weight-bearing exercise helps develop and preserve bone, and aerobic exercise helps stave off cardiovascular disease.</p>
					<p>Normal exercise will not bring about complications or miscarriage, though common sense suggests that you should not run more than a few miles a week or participate in rigorous activity after about 20-22 weeks into your pregnancy. Use your own judgment as to how much exercise may be too much for you, or create a well-balanced exercise program with your doctor.</p>
				</div>
			</div>
		</transition>

		<transition class="faq" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered" v-if="!filtered">
				<h3 @click="toggle">WHO SHOULD I CONTACT IF I HAVE MORE QUESTIONS?</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>For further information, or advice on using FIRST RESPONSE&#8482; tests, contact the FIRST RESPONSE&#8482; Advice Line at Church &amp; Dwight UK Ltd between 8.30am and 5.00pm Monday - Friday on 0800 121 6080. Alternatively, please complete our enquiry form <a href="{!! route('contact') !!}">here</a> and we will get back to you via email.</p>
				</div>
			</div>
		</transition>		
	</div>

	<div class="cream">
		<div class="maxInner">

			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('how-to-use') !!}" class="button twoline left"><span>How to Test</span>
				</a> <a href="{!! route('tips-for-a-healthy-pregnancy') !!}" class="button twoline right"><span>Tips for a<br/>healthy pregnancy</span></a></div>
			</div>
			
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
