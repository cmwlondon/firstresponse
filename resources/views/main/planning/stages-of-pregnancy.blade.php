@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Stages of Pregnancy</h1>

			<div class="row nopad mt2">
				<div class="columns span-12">
					<p class="lod">You followed the tips and tricks. You made the late night baby making time. Now, in a mere nine months, you&rsquo;ll be enjoying the best time of your life&hellip;. with the life you made! We tend to think of pregnancy in trimesters, each composed of about 13 weeks.</p>
					<p class="lod">Here&rsquo;s what you can expect during each stage.</p>
				</div>
			</div>

			<div class="row nopad mt2 two-col">
				<div class="columns span-6 md-12 sm-12" data-side="left">
					<div class="headed-box eq stage" data-group="1" data-stage="1">
						<div class="img"></div>
						<div class="eq-inner">
							<h3>First Trimester</h3>
							<p>The first trimester is critical to foetal development. This is when the baby&rsquo;s basic organ system starts to develop. During this time, some women experience nausea, which usually subsides at the end of the first 13 weeks. Expect to visit the bathroom more often (due to the pregnant uterus pressing on your bladder) and to start feeling tired. Some folks talk about eating for two, but few mention sleeping for two!</p>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12" data-side="right">
					<div class="headed-box eq stage" data-group="1" data-stage="2">
						<div class="img"></div>
						<div class="eq-inner">
							<h3>Second Trimester</h3>
							<p>On a physiology level, your circulating blood volume will rise 50%, so you may notice an increased heart rate, or become short of breath climbing up stairs. By the end of the second trimester, your baby&rsquo;s major organ systems will have developed their basic architecture, but the brain is constantly developing. (This is the time you can determine your baby&rsquo;s gender).</p>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12" data-side="left">
					<div class="headed-box eq stage" data-group="2" data-stage="3">
						<div class="img"></div>
						<div class="eq-inner">
							<h3>Third Trimester</h3>
							<p>Most women feel some tightening of their <a href="{!! route('the-female-body') !!}">uterus</a> during the last trimester called Braxton-Hicks contractions. These help the uterus practice for labour. If they start coming regularly &ndash; or become painful &ndash;while you&rsquo;re several weeks away from delivery, call your obstetrician or midwife. At this time, your healthcare provider will ask you to come in for more frequent check-ups to help avoid any end of pregnancy complications.</p>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12" data-side="right">
					<div class="headed-box eq stage" data-group="2" data-stage="4">
						<div class="img"></div>
						<div class="eq-inner">
							<h3>Post Pregnancy</h3>
							<p>Some women refer to the post-partum timeframe as the &ldquo;fourth trimester&rdquo;. During this time, it&rsquo;s important to continue taking care of yourself &ndash; you&rsquo;ll most likely be thinking about that baby. Do not hesitate to take kind offers from friends and family to help with the baby, so that you can take a nap. And don&rsquo;t worry what your house looks like &ndash; your friends want to see the baby, they&rsquo;ve seen your house before! You may need to wait about six weeks or so for the baby to smile at you, but once they do it will all be amazingly worthwhile!</p>
						</div>
					</div>
				</div>

			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-pregnancy',['current' => 'stages-of-pregnancy'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('a-healthy-lifestyle') !!}" class="button twoline left"><span>A Healthy Lifestyle</span>
				</a> <a href="{!! route('stress-management') !!}" class="button twoline right"><span>Stress Management</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection