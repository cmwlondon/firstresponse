@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Vitamins and Minerals</h1>
			<div class="row nopad mt2">
				<div class="columns span-12">
					<p>In addition to a healthy balanced diet, there are some vitamins and minerals that are essential for <a href="{!! route('a-healthy-lifestyle') !!}">fertility and pregnancy.</a> Prenatal vitamins are a definite DO in your reproductive periods. And when <a href="{!! route('stages-of-pregnancy') !!}">you&rsquo;re pregnant</a>. Most doctors will tell you that it&rsquo;s a good idea to start your prenatal vitamin regimen before you&rsquo;re pregnant. This ensures that you&rsquo;re getting the vitamins and minerals you need to support your health &ndash; and your baby&rsquo;s &ndash; from the <a href="{!! route('tips-for-a-healthy-pregnancy') !!}">outset</a>.</p>
					<p>Why not incorporate the below into your diet? </p>
				</div>
				
			</div>

			<div class="row nopad mt2">
				<div class="columns span-6 md-12 sm-12">
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/vitamin-a.png" class="w100" alt="Vitamin A"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/betacarotene.png" class="w100" alt="Betacarotone"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/vitamin-e.png" class="w100" alt="Vitamin E"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/vitamin-b6.png" class="w100" alt="Vitamin B6"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/folic-acid.png" class="w100" alt="Folic Acid"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/vitamin-b12.png" class="w100" alt="Vitamin B12"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/vitamin-c.png" class="w100" alt="Vitamin C"/>
					</div>
				</div>
				<div class="columns span-6 md-12 sm-12">
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/vitamin-d.png" class="w100" alt="Vitamin D"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/iron.png" class="w100" alt="Iron"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/iodine.png" class="w100" alt="Iodine"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/magnesium.png" class="w100" alt="Magnesium"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/selenium.png" class="w100" alt="Selenium"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/zinc.png" class="w100" alt="Zinc"/>
					</div>
				</div>				
			</div>

			<div class="row nopad mt2">
				<div class="columns span-12">
					<p class="tac">As always, please consult your doctor before taking any dietary supplement or if you have questions about dietary supplements and your diet.</p>
				</div>
				
			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-diet-lifestyle-stress',['current' => 'vitamins-and-minerals'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<p class="mt2 tac"><a href="{!! route('your-fertile-time') !!}" class="button"><span>Your Fertile Time</span>
				</a></p>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection