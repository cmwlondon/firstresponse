@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Stress Management</h1>
			<div class="row nopad mt2">
				<div class="columns span-12">
					<p class="lod"><picture>
						<source media="(min-width: 0px) and (max-width: 1366px)" srcset="{{{$image_path}}}/planning-for-a-baby/stress-management-wide.jpg">
						<img src="{{{$image_path}}}/planning-for-a-baby/stress-management.jpg" class="rf-image hidden-sm hidden-md lineFix" alt="Stress Management"/>
					</picture>Trying for a baby and dealing with day to day life generally can be an emotionally draining time. Increasing pressures can result in excessive amounts of stress hormones, including adrenaline and cortisol, which can negatively impact upon the chances of conception. It can also bring stress and anxiety into the bedroom.<br/><br class="hidden vis-sm vis-md"/>
					<img src="{{{$image_path}}}/planning-for-a-baby/stress-management-wide.jpg" class="w100 hidden vis-sm vis-md mt1" alt="Stress Management"/>
					<br/>You might catch yourself having a more mechanical sexual experience than a passionate one. You will probably feel that your partner&rsquo;s sexual interest is purely motivated by procreation, which leads to emotional anguish that can have a negative effect on conception.</p>
					<p class="lod">Conversely, feelings of stress in men can reduce the level of male hormones &ndash; affecting libido and sperm production. The pressure to have sex at the &#39;right time&#39; can also cause &#39;performance anxiety&#39;. It is very common for men to suffer temporary problems with maintaining an erection or having difficulties ejaculating.</p>
					<p class="lod">Your partner may experience &lsquo;performance anxiety,&rsquo; where the pressures of pregnancy can lead to difficulty ejaculating, or difficulty getting or sustaining an erection. It&rsquo;s not something to spend countless hours worrying over. Many couples go through the same process. It&rsquo;s part and parcel of the bargain.</p>
					<p class="lod">For tips and tricks on how to manage the stress that comes with trying for a baby, see our Stress Management guide.</p>
				</div>
			</div>

			
			<div class="row nopad mt2">
				<div class="columns span-12">
					<h2 class="mt2">Reducing Stress</h2>
					<p>Take the pressure off yourselves. Avoid thinking about having sex at &lsquo;the right time&rsquo; and just let it happen. Think back to situations that used to get you in the mood for <a href="{!! route('sex') !!}">sex</a> and try to recreate them. This could be something as simple as a surprise candlelit dinner at home. Or you could bring in an element of change, such as an overnight break or weekend away. This will help you relax and enjoy yourselves just before the trying for a baby process.</p>
					<p>The great thing about the human body is that it is always trying to get the balance back so there is a lot you can do through stress reduction techniques to help manage your stress levels, such as:</p>	
				</div>
			</div>
			<div class="row nopad mt1">
				<div class="columns span-12 mt1 numbered-list">
					<h3 data-grp="1">Try to&hellip;</h3>
					<p data-id="1">Have sufficient sleep</p>
					<p data-id="2">Make sure you have a <a href="{!! route('the-right-diet') !!}">healthy diet</a></p>
					<p data-id="3">Get plenty of regular exercise</p>
					<p data-id="4">Follow relaxation techniques such as <a href="{!! route('positive-mind-plan') !!}">visualisation and meditation</a></p>
					<p data-id="5">Get a good work / life balance</p>
					<p data-id="6">Build in treats for yourself</p>

					<h3 data-grp="3">Avoid&hellip;</h3>
					<p data-id="7">Stress triggers such as alcohol, cigarettes and caffeine.</p>
					<p data-id="8">Conflict with your partner - <a href="{!! route('your-relationship') !!}#working-together">effective communication</a> is essential.</p>
					<p data-id="9">Being hard on yourself or beating yourself up - try to get rid of any negative dialogue in your head and decide on what <a href="{!! route('positive-mind-plan') !!}">positive changes</a> you can make instead.</p>

					<h3 data-grp="5">Try these simple ways to relax&hellip;</h3>
					<p data-id="10">Listen to calming music to unwind</p>
					<p data-id="11">Sit in a park or garden and listen to the world going by</p>
					<p data-id="12">Ask <a href="{!! route('your-relationship') !!}">your partner</a> to massage you with aromatherapy oils</p>
					<p data-id="13">Have a hot, relaxing bath and light scented candles</p>
					<p data-id="14">Do some exercise &ndash; this can be a <a href="{!! route('a-healthy-lifestyle') !!}">great mood booster</a></p>
				</div>

				<div class="columns span-12 mt1">
					<p>Other stress management techniques include visualisation, meditation, therapeutic writing, cognitive behavioural therapy, hypnotherapy and art therapy.</p>
				</div>
			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-diet-lifestyle-stress',['current' => 'stress-management'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('positive-mind-plan') !!}" class="button twoline left"><span>Positive Mind Plan</span>
				</a> <a href="{!! route('your-relationship') !!}" class="button twoline right"><span>Your Relationship</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection