@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>The Male Body</h1>
			<div class="row nopad">
				<div class="columns span-10 before-1 after-1 md-12 md-before-0 md-after-0 sm-12 sm-before-0 sm-after-0">
					<div class="full-image bordered mt2 mb2">
						<img src="{{{$image_path}}}/planning-for-a-baby/the-male-body.jpg" class="w100" alt="The Male Body"/>
					</div>
				</div>
				<div class="columns span-12">
					<p>You know that women are from Venus, but it&rsquo;s always good to be familiar with the geography of Mars.</p>
					<p>Did you know that 100 million sperm are made in each testicle every day? Or that it takes approximately 64 days for an immature sperm cell to develop into a mature sperm? AND another 10-14 days to pass through the epididymis ready for ejaculation? Persistent little guys&hellip; aren&rsquo;t they?</p>
					<p>When sperm enters <a href="{!! route('sex') !!}">your vagina after ejaculation</a>, millions of the little swimmers begin their journey through <a href="{!! route('the-female-body') !!}">your reproductive system</a>. Depending on the stage of your menstrual cycle &ndash; if the timing is right &ndash; the sperm will be able to pass into the womb. Only about 0.1% of sperm will make it to this stage. Most will become lost in the complex glands that line the cervical canal or the <a href="{!! route('the-female-body') !!}">wall of the womb.</a></p>
					<p>Those who complete the journey (often in just a few minutes) can healthily live in the fallopian tubes for up to 5 days.</p>
					<p>Here, the sperm can meet and bind with an egg after it has been released from one of the ovaries. When a sperm attaches to the surface of the egg, the acrosome cap releases substances that dissolve a hole in the egg coat allowing the sperm head to penetrate the egg and deposit the male DNA (chromosomes) into the nucleus of the egg. This is the fertilisation stage.</p>
					<p>Poor sperm quality is one of the most important <a href="{!! route('conception-advice') !!}">factors in fertility</a> with approximately 30% of cases of infertility believed to be caused by male factors, which tends to surprise a lot of people. Evidence has shown that <a href="{!! route('the-right-diet') !!}">diet</a> and <a href="{!! route('stress-management') !!}">stress</a> can have an effect on the quality of sperm.</p>
				</div>
				<div class="columns span-12">
					<div class="fixed-box">
						<h3>Zita West&rsquo;s tips to boost sperm quality:</h3>
						<ul>
							<li>Eat a good <a href="{!! route('the-right-diet') !!}">breakfast</a> every day as it provides energy. Without a good breakfast, the body goes into survival mode and the production of sperm decreases.</li>
							<li>A man must ejaculate regularly, even when you&rsquo;re not having <a href="{!! route('sex') !!}">sex</a>. Sperm don&rsquo;t live forever; if they&rsquo;re not ejaculated regularly they will overheat and may die off in the epididymis. Long durations without ejaculation means the amount of dead sperm builds up and as they decay they release toxins that harm the fresh sperm.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-our-bodies',['current' => 'the-male-body'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('a-healthy-lifestyle') !!}" class="button twoline left"><span>A Healthy Lifestyle</span>
				</a> <a href="{!! route('your-relationship') !!}" class="button twoline right"><span>Your Relationship</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection