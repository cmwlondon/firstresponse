@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>The Right Diet</h1>
			<div class="row nopad mt2">
				<div class="columns span-12">
					<p class="lod">
						<picture>
							<source media="(min-width: 0px) and (max-width: 1366px)" srcset="{{{$image_path}}}/planning-for-a-baby/the-right-diet-wide.jpg">
							<img src="{{{$image_path}}}/planning-for-a-baby/the-right-diet.jpg" class="rf-image hidden-sm hidden-md lineFix" alt="The Right Diet"/>
						</picture>
						What you eat (and decide not to eat!) at this time is perhaps the single greatest advantage you can give yourself during pregnancy. Some of your favourite foods may have to stay locked away in the cupboard. Most doctors encourage their patients to avoid consuming certain types of fish, unpasteurised milk, juice, cheese, caffeine and of course, alcohol. Also, steer clear of any kind of diet to gain or lose weight as this may affect your reproductive cycle.<br/><br class="hidden vis-sm vis-md"/>
					<img src="{{{$image_path}}}/planning-for-a-baby/the-right-diet-wide.jpg" class="w100 hidden vis-sm vis-md mt1" alt="The Right Diet"/>
					</p>
				</div>
			</div>
			<div class="row nopad">
				<div class="columns span-12">
					<h5 class="tal ttn mt1">Here&rsquo;s some golden rules to follow to ensure you’re taking in the best foods to boost your fertility:</h5>
				</div>
				<div class="columns span-12 mt1 numbered-list">
					<h3 data-grp="1">Foods to incorporate in to your diet and lifestyle:</h3>
					<p data-id="1">Balance your blood sugar by eating breakfast, not leaving long gaps between meals and eating protein and carbs together&ndash; when blood sugar is unbalanced it can seriously impact your hormones, weight and energy levels.</p>
					<p data-id="2">Eat at least five portions of fresh fruit and vegetables each day &ndash; only 15% of women and 13% of men manage to achieve this.</p>
					<p data-id="3">Try to eat red meat no more than twice a week.</p>
					<p data-id="4">Stay away from fatty meat such as lamb and switch to lean cuts of beef.</p>
					<p data-id="5">Choose game and poultry meat which is low in fat instead of fatty meats.</p>
					<p data-id="6">Choose complex and unrefined carbohydrates such as rye or wholegrain bread.</p>
					<h3 data-grp="2">Foods to limit:</h3>
					<p data-id="7">Limit the amount of wheat and dairy that you eat.</p>
					<p data-id="8">Try to eat fresh fruit instead of cakes and biscuits. A handful of nuts are also a good alternative.&nbsp; That way you&rsquo;ll get more <a href="{!! route('vitamins-and-minerals') !!}">nutrients</a> and long lasting energy whilst keeping your blood sugar levels stable.</p>
					<h3 data-grp="3">Foods to avoid:</h3>
					<p data-id="9">Switch to butter instead of margarine or spread. Margarine often contains hydrogenated fats where as butter contains <a href="{!! route('vitamins-and-minerals') !!}">vitamin D.</a></p>
					<p data-id="10">Take in sufficient fibre.</p>
					<p data-id="11">Drink organic milk as this is richer in omega-3 than normal milk.</p>
					<h3 data-grp="4">Healthy food switches:</h3>
					<p data-id="12">Switch tea and coffee for caffeine free herbal teas. Alternatively try decaffeinated coffee using the Swiss Water method or decaffeinated tea using the CO2 method.</p>
					<p data-id="13">Cut out processed and convenience foods &ndash; these can contain free radicals which can cause damage to sperm and eggs.</p>
					<p data-id="14">Shop little and often to maximise freshness of food and choose products that are in season.</p>
					<p data-id="15">Avoid additives such as salt, artificial sweeteners, acrylamide, MSG and certain preservatives, artificial colourings and flavourings.</p>

				</div>
			</div>

			<div class="row nopad mt2">
				<div class="columns span-6 md-12 sm-12">
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/healthy-foetus.png" class="w100" alt="Foods for healthy foetus development"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/sperm-boosting.png" class="w100" alt="Sperm boosting foods"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/protect-sperm-egg.png" class="w100" alt="Protect sperm and egg cells"/>
					</div>

				</div>
				<div class="columns span-6 md-12 sm-12">
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/sex-libido.png" class="w100" alt="Sex and libido enhancing foods"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/brain-foods.png" class="w100" alt="Brain foods"/>
					</div>
					<div class="full-image bordered mt1">
						<img src="{{{$image_path}}}/planning-for-a-baby/infographics/healthy-secretions.png" class="w100" alt="Foods for healthy secretions"/>
					</div>
				</div>				
			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-diet-lifestyle-stress',['current' => 'the-right-diet'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<p class="mt2 tac"><a href="{!! route('vitamins-and-minerals') !!}" class="button"><span>Vitamins and minerals</span>
				</a></p>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection