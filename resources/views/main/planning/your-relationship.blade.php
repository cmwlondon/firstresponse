@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Your relationship</h1>
			<div class="row nopad mt2">
				<div class="columns span-12 mb2">
					<p>Trying for a baby requires big changes to a couple&rsquo;s lifestyle and can put a strain on the healthiest of relationships.</p>
				</div>
				<div class="columns span-6 md-12 sm-12">
					<div class="panel">
						<div class="panel-head bgTC" style="background-image:url({{{$image_path}}}/planning-for-a-baby/your-relationship-1.jpg);"></div>
						<div class="panel-main eq" data-group="1">
							<div class="eq-inner">
								<h3>Your lifestyle</h3>
								<p>It&rsquo;s always best to imagine how having a baby would impact upon you, and your partner&rsquo;s, life. Not only in terms of time but also the fundamental changes that would have to be made to your daily routine and lifestyle. Once this has been realised, it is easier to maintain the romantic side of the pregnancy dream whilst understanding the reality of <a href="{!! route('stages-of-pregnancy') !!}">having a baby</a> and trying to conceive.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12">
					<div class="panel">
						<div class="panel-head" style="background-image:url({{{$image_path}}}/planning-for-a-baby/your-relationship-2.jpg)"></div>
						<div class="panel-main eq" data-group="1">
							<div class="eq-inner">
								<h3>Your romantic relationship</h3>
								<p>It&rsquo;s important to keep the romance alive. As a couple you can include all the pastimes that you enjoyed together before the pressures of trying for a baby, such as listening to music, watching movies or reading a book. A weekend break or holiday can also be beneficial to allow you to focus upon the aspects that first attracted you to one another. It is true that in our work-orientated society, <a href="{!! route('sex') !!}">sex</a> can often be the last thing you want to think about.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12">
					<div class="panel" data-group="2">
						
						<div class="panel-head" style="background-image:url({{{$image_path}}}/planning-for-a-baby/your-relationship-3.jpg)"></div>
						<div class="panel-main">
							<div class="eq-inner">
								<h3>Communication</h3>
								<p>Communication is the key, as in some cases one partner is more willing to try for a baby than the other, so you have to be open and listen to each other and respect each other&rsquo;s needs and time. If couples find this difficult, it can be helpful to seek the assistance of a third-party, such as a fertility counsellor, to reopen the lines of communication.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12">
					<div class="fixed-box" data-group="2">
						<div class="eq-inner">
							<h3><a name="working-together"></a>Zita&rsquo;s tips on working together</h3>
							<ul>
								<li>Physical contact doesn&rsquo;t always have to lead to sex. Make more physical contact with your partner such as kissing, cuddling or stroking and explain to your partner why you&rsquo;re doing and why it doesn&rsquo;t always have to lead to sex.</li>
								<li>Avoid putting pressure on each other &ndash; sexual problems arise from both the mind and body.</li>
								<li>Open up about your sexual wants and be prepared to change your style if it will boost sexual desire.</li>
								<li>Keep talking to each other, during sex and everyday life. Emotions play an important role and if feelings are suppressed then this can negatively affect your sex life.</li>
								<li>Look at the way you live; what you eat, how much alcohol you drink, whether you smoke, how you sleep as these can all play a part in how your body responds to sex.</li> 
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-sex-relationship',['current' => 'your-relationship'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<p class="mt2 tac"><a href="{!! route('zita-west-videos') !!}" class="button"><span>Advice from<br/>Zita West</span>
				</a></p>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection