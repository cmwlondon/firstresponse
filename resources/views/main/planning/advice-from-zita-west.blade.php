@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Advice from Zita West</h1>
			<div class="row nopad mt2 vids">
				<div class="columns span-8 md-12 sm-12 eq-vid">
					<div class="eq-inner">
						<a name="player"></a>
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAYAAAA7KqwyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABpJREFUeNpi/P//PwMlgImBQjBqwLAwACDAAOVfAw9/ZDvcAAAAAElFTkSuQmCC" class="shim"/>
						<video id="video-player" class="video-js">
							
						</video>
					</div>
				</div>
				<div class="columns span-4 md-12 sm-12 eq-playlist">
					<menu class="playlist" :data-playing="playingID">
						<li @click="changeVideo" data-id="1" data-ref="advice-for-men"><div class="preview"><img src="/images/planning-for-a-baby/videos/advice-for-men.jpg"/></div>Boosting Fertility – Advice for Men<br/><span class="time">2m 32s</span></li>
						<li @click="changeVideo" data-id="2" data-ref="relaxation"><div class="preview"><img src="/images/planning-for-a-baby/videos/relaxation.jpg"/></div>Boosting Fertility – Mind &amp; Relaxation<br/><span class="time">2m 33s</span></li>
						<li @click="changeVideo" data-id="3" data-ref="diet-and-exercise"><div class="preview"><img src="/images/planning-for-a-baby/videos/diet-and-exercise.jpg"/></div>Boosting Fertility – Diet, Nutrition &amp; Exercise<br/><span class="time">2m 40s</span></li>
						<li @click="changeVideo" data-id="4" data-ref="your-body"><div class="preview"><img src="/images/planning-for-a-baby/videos/your-body.jpg"/></div>Boosting Fertility – Your Body<br/><span class="time">3m 05s</span></li>
						<li @click="changeVideo" data-id="5" data-ref="top-tips"><div class="preview"><img src="/images/planning-for-a-baby/videos/top-tips.jpg"/></div>Boosting Fertility – Top Tips<br/><span class="time">1m 37s</span></li>
					</menu>
				</div>
			</div>
		</div>
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-struggling-to-conceive',['current' => 'zita-west-videos'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('contraception-advice') !!}" class="button twoline left"><span>Contraception<br/>Advice</span>
				</a> <a href="{!! route('your-fertile-time') !!}" class="button twoline right"><span>Your Fertile Time</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection