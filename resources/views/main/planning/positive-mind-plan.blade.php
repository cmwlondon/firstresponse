@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Zita West&rsquo;s Positive Mind Plan</h1>
			<div class="row nopad mt2">
				<div class="columns span-12">
					<p>The Positive Mind Plan by Zita West and Jane Knight, is designed to help you understand your current situation and allow you to move forward with a more positive outlook on life.</p>
					<p>First you must determine what it is that&rsquo;s causing any negative thoughts and once you&rsquo;ve figured this out then you can begin to build a mind plan to combat it using the techniques outlined below. Follow the steps below to build your own - there is no minimum or maximum number of steps to include; it&rsquo;s entirely up to you to decide what&rsquo;s right for you and what will help you achieve your goals.</p>
				</div>
				
				<div class="columns span-12 mt1">
					<div class="headed-box">
						<h3>1. Action plan</h3>
						<p>This is the most important element and should be where you determine your key goals and plan how you will achieve them. This could be anything from eating more healthily to having wilder sex! Be sure to write them down and include lots of things that give you pleasure. Make sure you write down the next action you need to take and when, to help you achieve that goal (for example: &#39;Buy salad veg on the way home&#39;)</p>
					</div>
				</div>

				<div class="columns span-12 mt1">
					<div class="headed-box">
						<h3>2. Positive affirmations</h3>
						<p>Get your positive thoughts out there! Write down some personal positive statements and read them before you got to bed. You may want to try taking your negative thoughts about conception and turning them in to a positive by writing in the present tense. For example:</p>
						<p>Negative thought: &lsquo;The doctor says there&rsquo;s only a 5% chance I&rsquo;ll conceive.&rsquo;</p>
						<p>Positive affirmation: &lsquo;I am one of the 5%.&rsquo;</p>
						<p>Read them out loud convincingly until you&rsquo;re comfortable. Take 10 long, slow, deep breaths and allow your body to relax. Allow each of your positive statements to repeat themselves in your subconscious mind whilst you picture yourself living them. For example for &lsquo;I am one of the 5%&rsquo; - imagine yourself finding out you&rsquo;re pregnant.</p>
					</div>
				</div>

				<div class="columns span-12 mt1">
					<div class="headed-box">
						<h3>3. Visualisation techniques</h3>
						<p>Try to practice 20 minutes of self-help visualisation or stress reduction techniques per day.</p>
					</div>
				</div>

			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-diet-lifestyle-stress',['current' => 'positive-mind-plan'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('a-healthy-lifestyle') !!}" class="button twoline left"><span>A Healthy<br/>Lifestyle</span>
				</a> <a href="{!! route('stress-management') !!}" class="button twoline right"><span>Stress<br/>Management</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection