@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Struggling to conceive</h1>
			<div class="row nopad mt2">
				<div class="columns span-12 mb2">
					<p>How long should it take to <a href="{!! route('your-fertile-time') !!}">get pregnant</a>? It&rsquo;s a question many of us ask ourselves. The problem? There&rsquo;s tons of conflicting information out there. The good news is 95% of couples conceive within four years. The bad news? Four years can feel like an eternity.</p>
				</div>
				<div class="columns span-6 md-12 sm-12">
					<div class="panel mb1">
						<div class="panel-head bgTC" style="background-image:url({{{$image_path}}}/planning-for-a-baby/struggling-to-conceive-1.jpg);"></div>
						<div class="panel-main eq" data-group="1">
							<div class="eq-inner">
								<h3>Poor sperm quality</h3>
								<p><strong>Poor sperm quality</strong> is one of the most important factors in fertility and almost 30% of cases of infertility are down to a <a href="{!! route('the-male-body') !!}">male factor</a>, which tends to surprise a lot of people. Sperm need to be produced in sufficient numbers and <a href="{!! route('a-healthy-lifestyle') !!}">quality</a> in order to reach and penetrate the egg. Evidence has shown that <a href="{!! route('the-right-diet') !!}">diet</a> and <a href="{!! route('stress-management') !!}">stress</a> can have an effect on the quality of sperm. Also, a man must ejaculate regularly. Even when you&rsquo;re not having sex! Sperm don&rsquo;t live forever and long durations without ejaculation means the amount of dead sperm builds up. As they decay, they releasing toxins that harm the fresh sperm.</p>
							</div>
						</div>
					</div>

					<div class="panel mb1">
						<div class="panel-head" style="background-image:url({{{$image_path}}}/planning-for-a-baby/struggling-to-conceive-2.jpg)"></div>
						<div class="panel-main eq" data-group="1">
							<div class="eq-inner">
								<h3>Male infertility</h3>
								<p>Although <a href="{!! route('the-male-body') !!}">male infertility</a> is the single main factor for those struggling to conceive, there are plenty of other factors that can affect a couple&rsquo;s chances of conception, such as <a href="{!! route('a-healthy-lifestyle') !!}">smoking and alcohol</a> (or caffeine) overconsumption, being considerably under or <a href="{!! route('the-right-diet') !!}">overweight</a> and age.</p>

							</div>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12">
					<div class="panel">
						<div class="panel-head" style="background-image:url({{{$image_path}}}/planning-for-a-baby/struggling-to-conceive-3.jpg)"></div>
						<div class="panel-main eq" data-group="1">
							<div class="eq-inner">
								<h3>Fertility and age</h3>
								<p>Experts say that the average woman&rsquo;s fertility peaks at the age of 24, because eggs are more viable and less likely to have abnormalities. Also, health issues like fibroids and endometriosis are lower for women in their 20s, as is the risk of miscarriage. But even though a decade is a long time, women in their late 20&rsquo;s are still truly at their optimal child-bearing age.</p>
								<p>A woman&rsquo;s 30th year is the point where fertility falls off precipitously, and they warn couples that the quest will become more difficult as 30s inch toward 40s. Risks of miscarriage, ectopic pregnancies and other complications also increase, and doctors typically recommend more comprehensive screening for women who get pregnant in this age bracket.</p>
								<p>Later pregnancies are more common than they used to be, but even with that trend, would-be parents have to acknowledge the numbers. Fewer than 1% of women between the ages of 40 and 44 get pregnant, and the chances of a woman becoming pregnant during any one month after the age of 40 drops to five percent.</p>
								<p>Most people know at least one woman in her 40s who has defied the odds.</p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-struggling-to-conceive',['current' => 'conception-advice'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('a-healthy-lifestyle') !!}" class="button twoline left"><span>A Healthy Lifestyle</span>
				</a> <a href="{!! route('positive-mind-plan') !!}" class="button twoline right"><span>Positive Mind Plan</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection