@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Sex</h1>
			

			<div class="row nopad mt2">
				<div class="columns span-12">
					<p class="lod"><img src="{{{$image_path}}}/planning-for-a-baby/sex.jpg" class="rf-image hidden-sm hidden-md lineFix" alt="Sex"/>When it comes to baby making, repetition is key. On average, couples who have sex 2-3 times a week have a 15% chance of conception whereas couples who have sex every day have closer to a 50% chance. But there&rsquo;s more to increasing your chances of conception than &lsquo;doing it&rsquo; more. How you &lsquo;do it&rsquo; can help too. Yes, we&rsquo;re talking sex positions!<br><br class="hidden vis-sm vis-md"/>
					<img src="{{{$image_path}}}/planning-for-a-baby/sex.jpg" class="w100 hidden vis-sm vis-md mt1" alt="Sex"/>
					<br/>There is no need to perform extreme gymnastic style sex positions to help sperm on its journey. <a href="{!! route('the-female-body') !!}">Your body</a>, and especially the vagina, has the perfect natural angle which keeps the seminal fluid in contact with your cervix. <a href="{!! route('the-male-body') !!}">Semen</a> is ejaculated at about 28 mph as a sticky fluid which is designed to attach itself to the surface of the cervix. Within minutes it liquefies to release the sperm that swim upwards.</p>
					<p class="lod">Saying this, any position you feel comfortable and pleasurable is good (and always trying to ensure that penetration is deep). If you get more pleasure when you are on top then that is absolutely fine, you can try and roll over together after your partner has ejaculated so that you are on your back to keep the pool of seminal fluid against the cervix for a few minutes. It&rsquo;s necessary that sex is pleasurable for both of you, as an orgasm can help sperm on its way too. So, the more passion the better. You have might have heard that you have to keep your hips raised, but that is just a myth.</p>
					<p class="lod">The best tip is to lie flat on your back for 10-15 minutes after sex. This will keep the pool of seminal fluid against the cervix for a few minutes, <a href="{!! route('your-fertile-time') !!}">increasing chances of conception.</a></p>
				</div>
			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-sex-relationship',['current' => 'sex'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('your-fertile-time') !!}" class="button twoline left"><span>Your Fertile Time</span>
				</a> <a href="{!! route('stages-of-pregnancy') !!}" class="button twoline right"><span>Stages of<br/>Pregnancy </span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection