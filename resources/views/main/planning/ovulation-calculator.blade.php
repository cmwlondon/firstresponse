@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')

	<div class="maxInner">
		<div class="columns span-12 intro">
			<h1>Ovulation Calculator</h1>
			<p class="mt2">Trying to get pregnant? Our Ovulation Calculator helps you determine the days you&#39;re most likely to conceive. If you&#39;re actively trying to get pregnant, knowing when you will ovulate will greatly enhance your chance of success! Our Ovulation Calculator will help you predict the dates you&#39;re most likely to ovulate based on your cycle length. Enter the day of your last period and the number of days in your cycle (anywhere from 20 to 45 days). Our Ovulation Calculator will tell you the 2 days you&#39;re most likely to conceive.</p>
		</div>

		<div id="calculator" class="row full">

			<div class="columns span-12 calculator-wrap">

				<h2>CALCULATE MY BEST DAYS TO CONCEIVE</h2>

				<div class="form">



					<div class="form-row">
						<label for="datepicker_0">When did your last period begin?</label>
						<div class="calendar-wrap">
							<input name="first_day" v-model="first_day.value" @change="onChange" type="text" id="datepicker_0">
							<div class="calendar-inner">
								<img src="{{{$image_path}}}/pregnancy-test-calculator/calendar.jpg?{{{$version}}}" alt="">
							</div>
						</div>
					</div>

					<div class="form-row">
						<label for="average_cycle">Average cycle length</label>
						<div class="control-wrap">
							<input type="text" :value="average_cycle.rendered" id="average_cycle" @blur="checkAvgValue" @keyup.enter="checkAvgValue">
							<div class="control-inner">
								<div class="up" @click="incAvr"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 10" style="enable-background:new 0 0 15 10;" xml:space="preserve">
								<path d="M7.5,0C8,0,8.6,0.3,8.9,0.7l5.6,7.2c0.5,0.7,0.4,1.2,0.3,1.4c-0.1,0.2-0.4,0.7-1.3,0.7H1.5c-0.9,0-1.2-0.4-1.3-0.7C0,9.1-0.1,8.6,0.4,7.9l5.6-7.2C6.4,0.3,7,0,7.5,0z"/>
								</svg></div>
								<div class="down" @click="decAvr"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15 10" style="enable-background:new 0 0 15 10;" xml:space="preserve">
								<path d="M7.5,10C7,10,6.4,9.7,6.1,9.3L0.4,2.1C-0.1,1.4,0,0.9,0.1,0.7C0.3,0.4,0.6,0,1.5,0h12.1c0.9,0,1.2,0.4,1.3,0.7c0.1,0.2,0.2,0.8-0.3,1.4L8.9,9.3C8.6,9.7,8,10,7.5,10z"/>
								</svg></div>
							</div>
						</div>
						
					</div>

					<div class="button-row">
						<button type="button" class="button noselect" @click="calc">calculate</button>
					</div>
				</div>
			</div>

			<div id="result" class="columns span-12 result-wrap" :show="resultDisplay">
				<h2>Your best days to conceive are</h2>
				<h3 v-html="resultRange"></h3>
				<p class="tac">Your likely due date would be <span v-html="resultDueDate"></span></p>
			    
			    <table class="results" cellpadding="0" cellspacing="0">
			      <tr>
			        <th>S</th>
			        <th>M</th>
			        <th>T</th>
			        <th>W</th>
			        <th>T</th>
			        <th>F</th>
			        <th>S</th>
			      </tr>
			      <tbody v-html="resultTable">
			        
			      </tbody>
			    </table>
			    <p class="cf mt2 w100">Other future fertile days:</p>
			    <div class="row mt1">
			    	<div class="columns span-6 tac"><p v-html="resultRange2"></p></div>
			    	<div class="columns span-6 tac"><p v-html="resultRange3"></p></div>
			    </div>
			    <div class="results-footer">
					<div class="disclaimer">How does this tool calculate my most fertile days? Your most fertile days are based on an estimate of your ovulation date, which occurs 14 days before the first day of your next period. If you know your &quot;cycle length&quot; (the number of days starting from the first day of one period to the day before the first day of the next), you can predict when your next period will begin. To arrive at your ovulation date, the First Response Ovulation Calculator counts backwards 14 days from the day before you expect your next period to start. Your most fertile days are a range of days around your estimated ovulation date. An unfertilised egg has a survival window of approximately 24 hours. Sperm can live longer and may already be present before an egg is released. Or sperm may arrive in the 24 hours after the egg has been released. This span of time accounts for the range in fertile days.Please note: These dates are approximate and are best used as a guideline. Additionally, these dates are based on a regular menstrual cycle. If your menstrual cycle is irregular, you may have more difficulty pinpointing your day of ovulation based on the calendar alone.</div>
				</div>
			</div>
		</div>
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-our-bodies',['current' => 'ovulation-calculator'])
			<div class="columns span-12 mt1 noselect">
				<h5>You might also be interested in:</h5>
				<p class="mt2 tac"><a href="{!! route('pregnancy-test-calculator') !!}" class="button"><span>Pregnancy Test<br/>Calculator</span>
				</a></p>
			</div>
			
		</div>
	</div>

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
