@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>The Female Body</h1>
			<div class="row nopad">
				<div class="columns span-10 before-1 after-1 md-12 md-before-0 md-after-0 sm-12 sm-before-0 sm-after-0">
					<div class="full-image bordered mt2 mb2">
						<img src="{{{$image_path}}}/planning-for-a-baby/the-female-body.jpg" class="w100" alt="The Female Body"/>
					</div>
				</div>
				<div class="columns span-12">
					<p>You&rsquo;ve been living with it all your life but there&rsquo;s never been a better time to re-familiarise yourself with your body&rsquo;s biology.</p>
					<p>Every woman is born with her lifetime supply of eggs stored in her ovaries &ndash; each ovary is no bigger than a small plum. Each month, <a href="{!! route('ovulation-calculator') !!}">an egg is released</a>, ready for fertilisation. At this time, the lining of the uterus prepares for a <a href="{!! route('am-i-pregnant') !!}">possible pregnancy</a>. If fertilisation does not occur, then the period (shedding of the lining) should occur within 10-16 days of ovulation.</p>
					<p>This process is known as the &lsquo;menstrual cycle&rsquo;. An average menstrual cycle is 28 days in length and comprises two phases: the follicular phase (from the start of the period until ovulation) and the luteal phase (from ovulation to the next period).</p>
				</div>
			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-our-bodies',['current' => 'the-female-body'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('a-healthy-lifestyle') !!}" class="button twoline left"><span>A Healthy Lifestyle</span>
				</a> <a href="{!! route('faqs') !!}" class="button twoline right"><span>FAQs</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection