@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1 class="long-word">Contraception Advice</h1>
			<div class="row nopad mt2">
				<div class="columns span-12">
					<p class="lod"><img src="{{{$image_path}}}/planning-for-a-baby/contraception-advice.jpg" class="rf-image w50 hidden-sm hidden-md lineFix" alt="Contraception Advice"/>First things first. After all those years of using contraceptives to avoid getting pregnant, here you are&hellip; planning for your new baby! Whether you&rsquo;ve been using long-acting hormonal contraceptives or barrier contraceptives, there is no need to worry.<br/><br/>All contraceptives are reversible. Hormonal methods contain either progestogen, or a combination of progestogen and oestrogen. Oestrogen-containing methods may take a while for ovulation to re-establish; whereas the progestogen causes changes in a woman&rsquo;s cervical secretions, blocking the passage of sperm.<br/><br class="hidden vis-sm vis-md"/>
					<img src="{{{$image_path}}}/planning-for-a-baby/contraception-advice.jpg" class="w100 hidden vis-sm vis-md mt1" alt="Contraception Advice"/>
					<br/>When you stop any <a href="{!! route('your-fertile-time') !!}">hormonal method</a>, it may take a while for your secretions to return to normal to provide an excellent sperm-friendly environment.</p>
					<h5 class="tal ttn mt2">I'm trying for a baby. When should I stop my contraception?</h5>
				</div>
			</div>
		</div>

		<transition class="expander" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered">
				<h3 @click="toggle">Contraceptive injection</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>You may have to wait up to 12 months (or more) to conceive after the final injection due to its residual effects. If you are thinking about trying for a baby in the next year, it might be a good idea to switch to an alternative method of contraception to give your body plenty of time to adjust before conception.</p>
				</div>
			</div>
		</transition>

		<transition class="expander" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered">
				<h3 @click="toggle">Implant</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>The implant lasts for three years and &ndash; like other types of hormonal contraception &ndash; it affects the cervical mucus. This means it can take some time for your hormonal balance to return. On average, fertility returns approximately 3-12 months after the implant is removed.</p>
				</div>
			</div>
		</transition>

		<transition class="expander" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered">
				<h3 @click="toggle">Intrauterine Device (IUD)</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>Also known as the &lsquo;Coil,&rsquo; the IUD is not a hormonal contraception. As there are no hormones involved, your ovulation is not affected and fertility normally returns to normal as soon as the device is removed.</p>
				</div>
			</div>
		</transition>

		<transition class="expander" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered">
				<h3 @click="toggle">Intrauterine System (IUS)</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>The IUS is a small T-shaped device which contains progestogen. It thickens cervical mucus and may also prevent ovulation in some women. Many women can expect fertility to return in several months.</p>
				</div>
			</div>
		</transition>

		<transition class="expander" name="filtered">
			<div class="columns span-12 outline-box" :show="show" :filtered="!filtered">
				<h3 @click="toggle">Barrier methods</h3>
				<div class="ans" :style="{'max-height':styleHeight}">
					<p>Barrier methods, such as condoms and female condoms do not have any effect on your future fertility as you can start the baby making immediately. Having said this, they protect your sexual health from sexually transmitted infections such as Chlamydia, which could impact upon your fertility.</p>
				</div>
			</div>
		</transition>

		<div class="columns span-12 sources mt4">
			<p>Sources:<br/>
			<a href="http://www.fpa.org.uk/" target="_blank">www.fpa.org.uk</a> - <em>&lsquo;Plan to get Pregnant&rsquo; by Zita West, pg 15, 2008</em><br />
			<a href="http://www.netdoctor.co.uk/ate/womenshealth/206943.html" target="_blank">www.netdoctor.co.uk/ate/womenshealth/206943.html</a><br />
			<a href="http://www.nhs.uk/Conditions/contraception-guide/Pages/contraceptive-implant.aspx" target="_blank">www.nhs.uk/Conditions/contraception-guide/Pages/contraceptive-implant.aspx</a><br />
			<a href="http://www.nhs.uk/Conditions/contraception-guide/Pages/the-pill-progestogen-only.aspx" target="_blank">www.nhs.uk/Conditions/contraception-guide/Pages/the-pill-progestogen-only.aspx</a></p>
		</div>
	</div>

	<div class="cream">
		<div class="maxInner">

			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('am-i-pregnant') !!}" class="button twoline left"><span>Am I pregnant?<br/>Symptoms</span>
				</a> <a href="{!! route('conception-advice') !!}" class="button twoline right"><span>Struggling<br/>to conceive?</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection