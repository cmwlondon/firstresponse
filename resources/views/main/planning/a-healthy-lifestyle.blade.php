@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>A Healthy Lifestyle</h1>
			<div class="row nopad mt2">
				<div class="columns span-12">
					
					<p class="lod"><picture>
						<source media="(min-width: 0px) and (max-width: 1366px)" srcset="{{{$image_path}}}/planning-for-a-baby/a-healthy-lifestyle-wide.jpg">
						<img src="{{{$image_path}}}/planning-for-a-baby/a-healthy-lifestyle.jpg" class="rf-image hidden-sm hidden-md lineFix" alt="A Healthy Lifestyle"/>
					</picture>Fertility involves the whole body, so it&rsquo;s important to try and stay healthy <a href="{!! route('the-right-diet') !!}">inside</a> and out. For men and women, being <a href="{!! route('the-right-diet') !!}">overweight</a> and having a BMI (Body Mass Index) over 25 can have an impact on the <a href="{!! route('conception-advice') !!}">ability to conceive</a>. For women it can interrupt normal menstrual cycles and stop ovulation. When a woman is overweight fat cells store excess oestrogen which causes hormonal imbalances that interfere with <a href="{!! route('your-fertile-time') !!}">ovulation</a>. It can also increase the chances of a miscarriage and the need for a caesarean section. Plus, it also has an effect on pregnancy as it can cause health issues such as high blood pressure and diabetes.
					<br/><br class="hidden vis-sm vis-md"/>
					<img src="{{{$image_path}}}/planning-for-a-baby/a-healthy-lifestyle-wide.jpg" class="w100 hidden vis-sm vis-md mt1" alt="A Healthy Lifestyle"/>
					<br/>Excess pounds can also affect <a href="{!! route('the-male-body') !!}">male reproduction</a> because of a process carried out in fat cells called aromatisation, which converts testosterone (the male hormone) to oestrogen (the female hormone) thus reducing sperm count.</p>
					<p class="lod">Being underweight can also negatively impact fertility as the body senses famine and shuts the reproductive system down. Women who are underweight may not have enough oestrogen in the body for ovulation to occur. Adopting a regular exercise routine now will help you develop the strength and endurance needed down the road. Nearly all forms of exercise are recommended. Running, walking, stretching, yoga? You name it, the doctors say, &lsquo;do it!&rsquo; We always advise steering clear of any contact sports, so, no kick-boxing!</p>
					<p class="lod">One thing you should kick is bad habits. It&rsquo;s best to get a head start to help you develop strength and endurance down the road. Use this time as motivation to eliminate unhealthy habits from your lifestyle such as alcohol, illegal drugs and smoking.</p>
					<p class="lod">Smoking is a big factor when it comes to infertility as it can cause harm to the ovaries and accelerate the loss of eggs. It can also bring menopause forward by several years and interfere with oestrogen levels. For men, smoking can seriously impact sperm count and quality; it impairs sperm shape and function.</p>
					<p class="lod">Finally, it&rsquo;s important to get plenty of sleep. Once your baby is here it will be hard to come by, so take advantage now!</p>
				</div>
			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-diet-lifestyle-stress',['current' => 'a-healthy-lifestyle'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('stress-management') !!}" class="button twoline left"><span>Stress Management</span>
				</a> <a href="{!! route('the-right-diet') !!}" class="button twoline right"><span>The Right Diet</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection