@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Zita&rsquo;s tips on working together</h1>
			<div class="row nopad mt2">
				<div class="columns span-12">
					<div class="fixed-box">
						<ul>
							<li>Physical contact doesn&rsquo;t always have to lead to sex. Make more physical contact with your partner such as kissing, cuddling or stroking and explain to your partner why you&rsquo;re doing and why it doesn&rsquo;t always have to lead to sex.</li>
							<li>Avoid putting pressure on each other &ndash; sexual problems arise from both the mind and body.</li>
							<li>Open up about your sexual wants and be prepared to change your style if it will boost sexual desire.</li>
							<li>Keep talking to each other, during sex and everyday life. Emotions play an important role and if feelings are suppressed then this can negatively affect your sex life.</li>
							<li>Look at the way you live; what you eat, how much alcohol you drink, whether you smoke, how you sleep as these can all play a part in how your body responds to sex.</li>
						</ul>
					</div>
				</div>
				<div class="columns span-12">
					<p>Medical information provided by Zita West, Midwife, Fertility and Pregnancy Expert.</p>
				</div>
			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">

			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<p class="mt2"><a href="{!! route('your-relationship') !!}" class="button"><span>Your Relationship</span>
				</a></p>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection