@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div id="miscWrapper" class="maxInner bottom-space">
		<div class="columns span-12 intro">
			<h1>Trying for a baby?</h1>
			<p class="mt2">More information will be arriving here soon. In the meantime, find answers to popular question on our <a href="{!! route('faqs') !!}">FAQs</a> page or contact our FIRST RESPONSE&#8482; advice team on 0800 121 6080 8.30am and 5.00pm Monday - Friday.</p>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
