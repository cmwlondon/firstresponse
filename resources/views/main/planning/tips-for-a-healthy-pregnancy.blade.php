@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Tips for a Healthy Pregnancy</h1>
			<div class="row nopad mt2">
				<div class="columns span-6 md-12 sm-12">
					<div class="panel">
						<div class="panel-head bgTC" style="background-image:url({{{$image_path}}}/planning-for-a-baby/healthy-pregnancy-1.jpg);"></div>
						<div class="panel-main eq" data-group="1">
							<div class="eq-inner">
								<h3>Achieve your ideal body weight</h3>
								<p>Recommendations on weight gain during pregnancy vary, depending upon your pre-pregnancy weight. For healthy women of good body weight, a weight gain of 25-35 pounds is a good goal. For overweight women, minimal weight gain is recommended.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12">
					<div class="panel">
						<div class="panel-head" style="background-image:url({{{$image_path}}}/planning-for-a-baby/healthy-pregnancy-2.jpg)"></div>
						<div class="panel-main eq" data-group="1">
							<div class="eq-inner">
								<h3>Ensure you&rsquo;re getting proper nutrients</h3>
								<p>Pregnant women should ensure they&rsquo;re getting enough Vitamin D, Iron and Calcium throughout pregnancy. It&rsquo;s also important to note that all women who are thinking about trying to conceive should take a <a href="{!! route('vitamins-and-minerals') !!}">daily vitamin with folic acid</a>. It&rsquo;s also important to pay attention to what <a href="{!! route('the-right-diet') !!}">you eat.</a> Most doctors encourage their patients to avoid consuming certain types of fish, unpasteurised milk, juice, cheese, and caffeine.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12">
					<div class="panel">
						<div class="panel-head" style="background-image:url({{{$image_path}}}/planning-for-a-baby/healthy-pregnancy-3.jpg)"></div>
						<div class="panel-main eq" data-group="2">
							<div class="eq-inner">
								<h3>Abstain from drinking, smoking and drugs</h3>
								<p>It is known that women who drink two or more alcoholic beverages a day put their children at risk of fetal alcohol syndrome. Equally important is smoking cessation, as smoking can lead to fetal growth restriction by inhibiting blood flow to the placenta. All recreational drugs should be avoided <a href="{!! route('a-healthy-lifestyle') !!}">during pregnancy</a>.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12">
					<div class="panel">
						<div class="panel-head" style="background-image:url({{{$image_path}}}/planning-for-a-baby/healthy-pregnancy-4.jpg)"></div>
						<div class="panel-main eq" data-group="2">
							<div class="eq-inner">
								<h3>Prescription drugs should be monitored closely</h3>
								<p>For example, certain blood pressure medications are best avoided during pregnancy. If a woman is diabetic, she should have her blood sugars in control before getting pregnant. Women who are thinking of becoming pregnant should schedule a pre-pregnancy consultation to discuss their current health and medications before pregnancy.</p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-pregnancy',['current' => 'tips-for-a-healthy-pregnancy'])
			<div class="columns span-12 mt1">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('positive-result') !!}" class="button twoline left"><span>Positive test results</span>
				</a> <a href="{!! route('stress-management') !!}" class="button twoline right"><span>Stress Management</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection