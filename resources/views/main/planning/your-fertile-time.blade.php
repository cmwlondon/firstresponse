@extends('main.layouts.main')

@section('header')
	@include('main.layouts.partials._main-menu')
@endsection

@section('content')
	<div class="maxInner">
		<div id="contentGroup" class="columns span-12 intro">
			<h1>Your fertile time</h1>
			<div class="row nopad mt2 two-col">
				<div class="columns span-12" data-side="left">
					<p class="lod mb1"><img src="{{{$image_path}}}/planning-for-a-baby/your-fertile-time.jpg" class="rf-image hidden-sm hidden-md lineFix" alt="Contraception Advice"/>Determining your fertile time can be hugely beneficial when you are trying for a baby and can help you to get pregnant. During each menstrual cycle, you have about 6 days of your &lsquo;fertile window&rsquo;. This includes the day of <a href="{!! route('ovulation-calculator') !!}">ovulation itself</a> (where the egg is released and will survive for 24 hours) and the 5 days before (where sperm can survive in your body waiting for the egg).<br/><br class="hidden vis-sm vis-md"/>
					<img src="{{{$image_path}}}/planning-for-a-baby/your-fertile-time.jpg" class="w100 hidden vis-sm vis-md mt1" alt="Contraception Advice"/>
					<br/>It is beneficial for <a href="{!! route('your-relationship') !!}">you and your partner</a> to be aware of this &ldquo;window&rdquo; as it maximises your chances of <a href="{!! route('stages-of-pregnancy') !!}">having a baby</a>. It&rsquo;s the time when the egg is going to be released from one of the ovaries and the time when sperm are most likely to reach the uterus. During the fertile time a woman&rsquo;s cervix opens and her secretions become moist, white and sticky and then wetter, clear and stretchy. These secretions nourish the sperm and encourage its progression through the cervix and into the uterus.<br/><br/>The fertile time will not always be the same length and the day that the egg is released cannot be exactly determined. However, there is a simple way to get a rough idea of <a href="{!! route('ovulation-calculator') !!}">your fertile time</a> if you know the length of your cycles from the past 6 to 12 months&hellip;</p>
					
				</div>

				<div class="columns span-6 md-12 sm-12" data-side="left">
					<div class="fixed-box">
						<h3>How to calculate your fertile time</h3>
						<ol>
							<li>Simply write down all of the lengths of your previous cycles.</li>
							<li>Then subtract 20 from the lowest number, and subtract 10 from the highest.</li>
							<li>This gives you an estimated start and end date for your fertile time.</li>
						</ol>
					</div>
				</div>
				<div class="columns span-6 md-12 sm-12" data-side="right">
					<p class="lod">For example; if your last 6 cycles have been 28, 29, 27, 29, 26, 30 days long, then 26 is your shortest cycle (lowest number), so subtract 20 which gives you 6, meaning your fertile time starts around the 6th day of your cycle. Then subtract 10 from your longest cycle (highest number) which in this case is 30. This gives you 20, meaning your fertile time ends around the 20th day of your cycle.</p>
				</div>
				<div class="columns span-12 mt1">
					<h2 class="question lod">What happens in the menstrual cycle?*</h2>
					<p class="disclaimer lod">*Based on the average menstrual cycle of 28 days</p>
				</div>

				<div class="columns span-6 md-12 sm-12 mt1" data-side="left">
					<div class="headed-box">
						<h3>Day 1-6: (Period)</h3>
						<ul>
							<li>The womb lining from the previous cycle is shed and exits the body as a period.</li>
							<li>Follicles begin to develop in the ovaries.</li>
							<li>Follicle stimulating hormone (FSH) is released into the bloodstream to stimulate follicle growth.</li>
						</ul>
					</div>

					<div class="headed-box mt2">
						<h3>Day 7-9 (Relatively infertile time)</h3>
						<ul>
							<li>The vagina will feel dry as the cervix is tightly closed with thick secretions preventing sperm from getting through.</li>
							<li>One of the follicles will grow faster than the others in a natural cycle to become the dominant one.</li>
							<li>Oestrogen levels begin to rise.</li>
						</ul>
					</div>
				</div>

				<div class="columns span-6 md-12 sm-12 mt1" data-side="right">
					<div class="headed-box">
						<h3>Day 10-18 (Fertile time)</h3>
						<ul>
							<li>This is the best time to have lots of sex as the sperm will be greeted by wetter, clearer and often stretchier secretions which nourish the sperm and encourage it to swim upwards. Sperm can survive for an average of 2-3 days and up to 5 days in this environment.</li>
							<li>Oestrogen levels continue to rise.</li>
							<li>FSH production is shut down and a surge of LH is released, triggering ovulation.</li>
							<li>The dominant follicle becomes the corpus luteum and progesterone is released.</li>
							<li>Any strong sperm should be waiting in the fallopian tubes ready to fertilise the egg</li>
							<li>The non-dominant follicles will decrease in size and degenerate in a natural cycle.</li>
							<li>Ovulation occurs</li>
						</ul>
					</div>
				</div>

				<div class="columns span-12 mt1">
					<div class="headed-box">
						<h3>Day 19-28 (Infertile time)</h3>
						<ul>
							<li>The waiting sperm will meet the egg in the fallopian tube and, hopefully, penetrate and fertilise it.</li>
							<li>The corpus luteum will produce progesterone which encourages the womb lining to thicken, closes the cervix and thickens the secretions to prevent any further sperm getting through.</li>
							<li>If the egg is not fertilised, progesterone levels will drop and the womb lining begins to shed, starting the cycle again.</li>
							<li>If the egg has been fertilised then it travels down one of the fallopian tubes into the uterus and implants itself.</li>
						</ul>
					</div>
				</div>

			</div>
		</div>

		
	</div>

	<div class="cream">
		<div class="maxInner">
			@include('main.planning.partials._also-our-bodies',['current' => 'your-fertile-time'])
			<div class="columns span-12 mt4">
				<h5>You might also be interested in:</h5>
				<div class="button-group"><a href="{!! route('a-healthy-lifestyle') !!}" class="button twoline left"><span>A Healthy Lifestyle</span>
				</a> <a href="{!! route('ovulation-calculator') !!}" class="button twoline right"><span>Ovulation<br/>Calculator</span></a></div>
			</div>
			
		</div>
	</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection