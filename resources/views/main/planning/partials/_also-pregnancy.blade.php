<?php
	$sections = [
		'Stages of Pregnancy' => 'stages-of-pregnancy',
		'Tips for a Healthy Pregnancy' => 'tips-for-a-healthy-pregnancy',
	];

	$output = "";
	foreach($sections as $name => $url) {
		if ($url != $current) {
			if ($output != "") {
				$output .= '<span class="divide"> | </span>';
			}
			$output .= '<br/><a href="'.route($url).'">'.$name.'</a>';
		}
	}

?>
<div class="columns span-12 mt4">
	<p class="also"><b>Also in &lsquo;Pregnancy&rsquo;:</b> {!! $output !!}</p>
</div>