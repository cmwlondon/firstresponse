<?php
	$sections = [
		'The Right Diet' => 'the-right-diet',
		'Vitamins and Minerals' => 'vitamins-and-minerals',
		'A Healthy Lifestyle' => 'a-healthy-lifestyle',
		'Stress Management' => 'stress-management',
		'Positive Mind Plan' => 'positive-mind-plan'
	];

	$output = "";
	foreach($sections as $name => $url) {
		if ($url != $current) {
			if ($output != "") {
				$output .= '<span class="divide"> | </span>';
			}
			$output .= '<br/><a href="'.route($url).'">'.$name.'</a>';
		}
	}

?>
<div class="columns span-12 mt4">
	<p class="also"><b>Also in &lsquo;Diet, Lifestyle &amp; Stress&rsquo;:</b> {!! $output !!}</p>
</div>