<?php
	$sections = [
		'Conception Advice' => 'conception-advice',
		'Advice from Zita West' => 'zita-west-videos',
	];

	$output = "";
	foreach($sections as $name => $url) {
		if ($url != $current) {
			if ($output != "") {
				$output .= '<span class="divide"> | </span>';
			}
			$output .= '<br/><a href="'.route($url).'">'.$name.'</a>';
		}
	}

?>
<div class="columns span-12 mt4">
	<p class="also"><b>Also in &lsquo;Struggling to Conceive&rsquo;:</b> {!! $output !!}</p>
</div>