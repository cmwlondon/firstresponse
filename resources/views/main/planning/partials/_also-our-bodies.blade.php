<?php
	$sections = [
		'The Female Body' => 'the-female-body',
		'The Male Body' => 'the-male-body',
		'Your Fertile Time' => 'your-fertile-time',
		'Ovulation Calculator' => 'ovulation-calculator'
	];

	$output = "";
	foreach($sections as $name => $url) {
		if ($url != $current) {
			if ($output != "") {
				$output .= '<span class="divide"> | </span>';
			}
			$output .= '<br/><a href="'.route($url).'">'.$name.'</a>';
		}
	}

?>
<div class="columns span-12 mt4">
	<p class="also"><b>Also in &lsquo;Our Bodies&rsquo;:</b> {!! $output !!}</p>
</div>