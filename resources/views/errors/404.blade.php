<!doctype html>
<html class="no-js debug" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<title>FIRST RESPONSE</title>
		<meta name="description" content="FIRST RESPONSE&amp;#8482; Pregnancy Tests tell sooner than any other pregnancy test brand." />
		<meta name="keywords" content="pregnant,pregnancy,test,firstresponse,pregnancytest,first,response,missed,period"/>
		<meta name="keyphrases" content="first response, pregnancy test, am i pregnant, missed period"/>

		<link rel="stylesheet" href="/css/main/styles.css?{{ time() }}"/>
		<link rel="stylesheet" href="/css/main/print.css?{{ time() }}"/>
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet">
		
		<link rel="stylesheet" href="/css/main/pages/misc.css?12"/>
		<meta name="robots" content="noindex" />

		<link rel="canonical" href="http://local.firstresponse" />
		<meta property="og:locale" content="en_GB" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="FIRST RESPONSE" />
		<meta property="og:description" content="FIRST RESPONSE&amp;#8482; Pregnancy Tests tell sooner than any other pregnancy test brand." />
		<meta property="og:url" content="http://local.firstresponse" />
		<meta property="og:site_name" content="First Response Fertility" />
		<meta property="og:image" content="http://local.firstresponse/images/sharing/first-response.jpg" />

		<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png?{{ time() }}">
		<link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png?{{ time() }}">
		<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png?{{ time() }}">
		<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png?{{ time() }}">
		<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png?{{ time() }}">
		<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png?{{ time() }}">
		<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png?{{ time() }}">
		<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png?{{ time() }}">
		<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png?{{ time() }}">
		<link rel="icon" type="image/png" sizes="192x192"  href="/images/favicon/android-icon-192x192.png?{{ time() }}">
		<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png?{{ time() }}">
		<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png?{{ time() }}">
		<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png?{{ time() }}">
		<link rel="manifest" href="/images/favicon/manifest.json?{{ time() }}">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/images/favicon/ms-icon-144x144.png?{{ time() }}">
		<meta name="theme-color" content="#ffffff">

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/animations-ie-fix.css">
			<style type="text/css">
			    #switch-group {display:none;}
			</style>
		<![endif]-->

		<!--[if gte IE 9]>
		  <style type="text/css">
		    .gradient {
		       filter: none!important;
		    }
		  </style>
		<![endif]-->
	</head>

	<body class="error">
		<a name="top"></a>

		@if (App::environment() == 'local')
		<div id="css-debug" class="hidden"><span class="hidden vis-xsm">Size:&nbsp;xsm</span><span class="hidden vis-sm">Size:&nbsp;sm</span><span class="hidden vis-md">Size:&nbsp;md</span><span class="hidden vis-lg">Size:&nbsp;lg</span><span class="hidden vis-xlg">Size:&nbsp;xlg</span><span class="hidden vis-xxlg">Size:&nbsp;xxlg</span><span class="hidden vis-xxxlg">Size:&nbsp;xxxlg</span></div>
		@endif
		
		<div id="cookieDisclaimer" class="cookie-disclaimer" :class="cookieClass">To give you the best possible experience this site uses cookies. By continuing to use this website you are giving consent to cookies being used. For more information visit our <a href="/cookie-notice">Cookie&nbsp;Notice</a>.<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64.6 64.6" enable-background="new 0 0 64.6 64.6" xml:space="preserve" class="btn-close" @click="closeCookieDisclaimer">
			    <line x1="3.5" y1="3.5" x2="61.1" y2="61.1"/>
			    <line x1="61.1" y1="3.5" x2="3.5" y2="61.1"/>
			</svg></div>

		<header id="headerWrapper" :show="show">
				<div class="bar"></div>
<img src="/images/triangle-up.png?12" class="triangle" data-pin-nopin="true" alt="Menu Identifier"/>
<div id="topMenu" class="topmenu-container maxInner">
	
	<img src="/images/hamburger.svg" class="burger" alt="Show Menu" @click="showMobileMenu" data-pin-nopin="true"/>
	<a href="/"><img src="/images/first-response-logo.png?{{ time() }}" class="logo" alt="FIRST RESPONSE&#8482; FERTILITY"/></a>

	<menu class="menu left">
		<li><div class="btn" @click="noClick" @mouseover="showSubMenu" data-id="3"><a href="#">Our Products</a></div></li>
		<li><div class="btn" @click="noClick" @mouseover="showSubMenu" data-id="2"><a href="#">Your Test Results</a></div></li>
		<li><div class="btn" @click="noClick" @mouseover="showSubMenu" data-id="1"><a href="#">How to Use</a></div></li>
	</menu>

	<menu class="menu right">
		<li><div class="btn" @click="noClick" @mouseover="showSubMenu" data-id="4"><a href="#">Planning for a
Baby</a></div></li>
		<li><div class="btn" data-id="5"><a href="/faqs">FAQs</a></div></li>
		<li><div class="btn" data-id="6"><a href="/buy-now">Buy Now</a></div></li>
	</menu>
</div>

<div class="submenu-mask">
	<div class="submenu-container" @mouseout="hideSubMenu">
		<menu class="submenu" data-id="1">
			<li><div class="btn"><a href="/how-to-use">Taking a Test</a></div></li>
			<li><div class="btn"><a href="/how-to-use/pregnancy-test-calculator">Pregnancy Test Calculator</a></div></li>
			<li><div class="btn"><a href="/how-to-use/am-i-pregnant">Am I Pregnant? Symptoms</a></div></li>
			
		</menu>
		<menu class="submenu" data-id="2">
			<li><div class="btn"><a href="/your-test-results">Reading Your Test</a></div></li>
			<li><div class="btn"><a href="/your-test-results/positive-result">Positive Result</a></div></li>
			<li><div class="btn"><a href="/your-test-results/negative-result">Negative Result</a></div></li>
			<li><div class="btn"><a href="/your-test-results/due-date-calculator">Due Date Calculator</a></div></li>
		</menu>
		<menu class="submenu" data-id="3">
			<li><div class="btn"><a href="/our-products">FIRST RESPONSE&#8482; Tests</a></div></li>
			<li><div class="btn"><a href="/our-products/first-response-accuracy">FIRST RESPONSE&#8482; Accuracy</a></div></li>
			<li><div class="btn"><a href="/our-products/pre-seed">PRE&#8209;SEED&#8482; Fertility&#8209;Friendly Personal Lubricant</a></div></li>
		</menu>
		<div class="submenu" data-id="4">
			<ul class="colmenu">
				<li>
					<div class="heading">Contraception</div>
					<menu class="termenu">
						<li><a href="/planning-for-a-baby/contraception/contraception-advice">Contraception Advice</a></li>
					</menu>
				</li>
				<li>
					<div class="heading">Our Bodies</div>
					<menu class="termenu">
						<li><a href="/planning-for-a-baby/our-bodies/the-female-body">The Female Body</a></li>
						<li><a href="/planning-for-a-baby/our-bodies/the-male-body">The Male Body</a></li>
						<li><a href="/planning-for-a-baby/our-bodies/your-fertile-time">Your Fertile Time</a></li>
						<li><a href="/planning-for-a-baby/our-bodies/ovulation-calculator">Ovulation Calculator</a></li>
					</menu>
				</li>
				<li>
					<div class="heading">Diet, Lifestyle &amp; Stress</div>
					<menu class="termenu">
						<li><a href="/planning-for-a-baby/diet-lifestyle-and-stress/the-right-diet">The Right Diet</a></li>
						<li><a href="/planning-for-a-baby/diet-lifestyle-and-stress/vitamins-and-minerals">Vitamins and Minerals</a></li>
						<li><a href="/planning-for-a-baby/diet-lifestyle-and-stress/a-healthy-lifestyle">A Healthy Lifestyle</a></li>
						<li><a href="/planning-for-a-baby/diet-lifestyle-and-stress/stress-management">Stress Management</a></li>
						<li><a href="/planning-for-a-baby/diet-lifestyle-and-stress/positive-mind-plan">Positive Mind Plan</a></li>
					</menu>
				</li>
				<li>
					<div class="heading">Sex &amp; Relationships</div>
					<menu class="termenu">
						<li><a href="/planning-for-a-baby/sex-and-relationships/sex">Sex</a></li>
						<li><a href="/planning-for-a-baby/sex-and-relationships/your-relationship">Your Relationship</a></li>
						
					</menu>
				</li>
				<li>
					<div class="heading">Struggling to Conceive?</div>
					<menu class="termenu">
						<li><a href="/planning-for-a-baby/struggling-to-conceive/conception-advice">Conception Advice</a></li>
						<li><a href="/planning-for-a-baby/struggling-to-conceive/advice-from-zita-west">Advice from Zita West</a></li>
					</menu>
				</li>
				<li>
					<div class="heading">Pregnancy</div>
					<menu class="termenu">
						<li><a href="/planning-for-a-baby/pregnancy/stages-of-pregnancy">Stages of Pregnancy</a></li>
						<li><a href="/planning-for-a-baby/pregnancy/tips-for-a-healthy-pregnancy">Tips for a Healthy Pregnancy</a></li>
					</menu>
				</li>
			</ul>
		</div>
	</div>
</div>
		</header>

		<div id="contentWrapper">
			<section class="row full">
				<div class="maxInner centered">
					<div class="columns span-12 err-msg">
						<img src="/images/404/404-stick.png" class="stick"/>
						<h2>Not the result you were looking for?</h2>
						<p>Oooppps, this page must no longer be running.<br/>How about trying:</p>
						<ul>
							<li><a href="{!! route('how-to-use') !!}">Taking a test</a></li>
							<li><a href="{!! route('your-test-results') !!}">Reading your test</a></li>
							<li><a href="{!! route('our-pregnancy-tests') !!}">Our products</a></li>
							<li><a href="{!! route('faqs') !!}">FAQs</a></li>
							<li><a href="{!! route('contact') !!}">Get in touch</a></li>
							<li><a href="{!! route('buy-now') !!}">Where to buy</a></li>
						</ul>
					</div>
				</div>
			</section>
		</div>

		<div id="footerWrapper">
			<ul>
				<li><a href="{!! route('ovulation-calculator') !!}">Ovulation Calculator</a></li>
				<li>|</li>
				<li><a href="{!! route('contact') !!}">Get in touch</a></li>
				<li>|</li>
				 
				<li><a href="{!! route('cookie-notice') !!}">Cookie Notice</a></li>
				<li>|</li>
				<li><a href="{!! route('privacy-policy') !!}">Privacy Policy</a></li>
				<li>|</li>
				{{--
				<li><a href="{!! route('policies') !!}">Policies</a></li>
				<li>|</li>--}}
				<li><a href="{!! route('terms') !!}">Terms</a></li>
				<li>|</li>
				<li><a href="http://www.churchdwight.co.uk" target="_blank">Church&nbsp;&amp;&nbsp;Dwight</a></li>
			</ul>

			<p class="mt1">&copy; Copyright Church &amp; Dwight UK Ltd. 2017.<br/>All rights reserved. <br class="hidden vis-sm"/>FIRST RESPONSE&#8482; is a trademark of Church &amp; Dwight Co., Inc</p>
		</div>

		<div id="mobileMenuWrapper" :class="{visible:isVisible}" :show="show">
<div class="mobile-menu">
	<div class="bg"></div>
	<div class="bar"></div>
	<img src="/images/cross.svg" class="cross" alt="Close Menu" @click="closeMobileMenu" nopin="nopin"/>
	<a href="{!! route('home') !!}"><img src="/images/first-response-logo.png?{{ time() }}" class="logo" alt="FIRST RESPONSE&#8482; FERTILITY"/></a>
	<div class="mask">
		<menu class="menu top">
			<li class="l2"><a href="#" data-id="1" class="sub" @click="openSubMenu">How to Use</a></li>
			<li class="l2"><a href="#" data-id="2" class="sub" @click="openSubMenu">Your Test Results</a></li>
			<li class="l2"><a href="#" data-id="3" class="sub" @click="openSubMenu">Our Products</a></li>
			<li class="l2"><a href="#" data-id="4" class="sub" @click="openSubMenu">Planning for a Baby</a></li>
			<li class="l2"><a href="{!! route('faqs') !!}" data-id="5">FAQs</a></li>
			<li class="l2"><a href="{!! route('buy-now') !!}" data-id="6">Buy Now</a></li>
		</menu>

		<menu class="menu sub" data-id="1">
			<li class="l2"><a href="{!! route('how-to-use') !!}" data-id="1">Taking a Test</a></li>
			<li class="l2"><a href="{!! route('pregnancy-test-calculator') !!}" data-id="2">Pregnancy Test Calculator</a></li>
			<li class="l2"><a href="{!! route('am-i-pregnant') !!}" data-id="3">Am I Pregnant? Symptoms</a></li>
			
			<li class="l2"><a href="#" data-id="x" class="return" @click="closeSubMenu">Back to Main Menu</a></li>
		</menu>

		<menu class="menu sub" data-id="2">
			<li class="l2"><a href="{!! route('your-test-results') !!}" data-id="1">Reading Your Test</a></li>
			<li class="l2"><a href="{!! route('positive-result') !!}" data-id="2">Positive Result</a></li>
			<li class="l2"><a href="{!! route('negative-result') !!}" data-id="3">Negative Result</a></li>
			<li class="l2"><a href="{!! route('due-date-calculator') !!}" data-id="4">Due Date Calculator</a></li>
			<li class="l2"><a href="#" data-id="x" class="return" @click="closeSubMenu">Back to Main Menu</a></li>
		</menu>

		<menu class="menu sub" data-id="3">
			<li class="l2"><a href="{!! route('our-pregnancy-tests') !!}" data-id="1">FIRST RESPONSE&#8482; Tests</a></li>
			<li class="l2"><a href="{!! route('accuracy') !!}" data-id="2">FIRST RESPONSE&#8482; Accuracy</a></li>
			<li class="l2"><a href="{!! route('pre-seed') !!}" data-id="2">PRE&#8209;SEED&#8482; Fertility&#8209;Friendly Personal&nbsp;Lubricant</a></li>
			<li class="l2"><a href="#" data-id="x" class="return" @click="closeSubMenu">Back to Main Menu</a></li>
		</menu>

		<menu class="menu sub" data-id="4" :show="terShow">
			<li class="l2 ter"><a href="#" class="arr" data-id="1" @click="toggleTerMenu">Contraception</a>
				<menu class="ter" data-id="1">
					<li class="l3"><a href="{!! route('contraception-advice') !!}">Contraception Advice</a></li>
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="2" @click="toggleTerMenu">Our Bodies</a>
				<menu class="ter" data-id="2">
					<li class="l3"><a href="{!! route('the-female-body') !!}">The Female Body</a></li>
					<li class="l3"><a href="{!! route('the-male-body') !!}">The Male Body</a></li>
					<li class="l3"><a href="{!! route('your-fertile-time') !!}">Your Fertile Time</a></li>
					<li class="l3"><a href="{!! route('ovulation-calculator') !!}">Ovulation Calculator</a></li>
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="3" @click="toggleTerMenu">Diet, Lifestyle &amp; Stress</a>
				<menu class="ter" data-id="3">
					<li class="l3"><a href="{!! route('the-right-diet') !!}">The Right Diet</a></li>
					<li class="l3"><a href="{!! route('vitamins-and-minerals') !!}">Vitamins and Minerals</a></li>
					<li class="l3"><a href="{!! route('a-healthy-lifestyle') !!}">A Healthy Lifestyle</a></li>
					<li class="l3"><a href="{!! route('stress-management') !!}">Stress Management</a></li>
					<li class="l3"><a href="{!! route('positive-mind-plan') !!}">Positive Mind Plan</a></li>
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="4" @click="toggleTerMenu">Sex &amp; Relationships</a>
				<menu class="ter" data-id="4">
					<li class="l3"><a href="{!! route('sex') !!}">Sex</a></li>
					<li class="l3"><a href="{!! route('your-relationship') !!}">Your Relationship</a></li>
					{{-- <li class="l3"><a href="{!! route('working-together') !!}">Working Together</a></li> --}}
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="5" @click="toggleTerMenu">Struggling to Conceive?</a>
				<menu class="ter" data-id="5">
					<li class="l3"><a href="{!! route('conception-advice') !!}">Conception Advice</a></li>
					<li class="l3"><a href="{!! route('zita-west-videos') !!}">Advice from Zita West</a></li>
				</menu>
			</li>
			<li class="l2 ter"><a href="#" class="arr" data-id="6" @click="toggleTerMenu">Pregnancy</a>
				<menu class="ter" data-id="6">
					<li class="l3"><a href="{!! route('stages-of-pregnancy') !!}">Stages of Pregnancy</a></li>
					<li class="l3"><a href="{!! route('tips-for-a-healthy-pregnancy') !!}">Tips for a Healthy Pregnancy</a></li>
				</menu>
			</li>
			<li class="l2"><a href="#" data-id="x" class="return" @click="closeSubMenu">Back to Main Menu</a></li>
		</menu>

	</div>
</div>			
		</div>

		<script>
			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif
			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif
		</script>

		{!! Html::script('js/libs/requirejs/require.js', ['data-main' => '/js/main/main.min.js']) !!}

		@include('main.layouts.partials._analytics')
	</body>
</html>
