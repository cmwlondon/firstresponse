<?php

return [
    'buy-boots' => 'http://www.boots.com/search/first+response+pregnancy',
    'buy-asda' => 'https://groceries.asda.com/product/pregnancy-tests/first-response-one-step-pregnancy-test-2-pack/910000055742',
    'buy-superdrug' => 'http://www.superdrug.com/First-Response/First-Response-Pregnancy-Tests-2s/p/746164?singleResultSearchPage=true',
    'buy-lloyds' => 'http://www.lloydspharmacy.com/en/first-response-early-result-pregnancy-test-2-tests',
    'buy-amazon' => 'https://www.amazon.co.uk/First-Response-Early-Result-Pregnancy/dp/B001DULLKE/ref=sr_1_1_a_it?ie=UTF8&qid=1490190900&sr=8-1&keywords=first+Response',
    'buy-chemist-direct' => 'https://www.chemistdirect.co.uk/first-response-early-result-pregnancy-test-double/prd-4ir?gclid=CjwKEAjwwcjGBRDj-P7TwcinyBkSJADymblTncaeeOdo_lszfLczL98AM2wjtcgTIWxDRl8KFz25zBoCrunw_wcB',
    'buy-pre-seed-boots' => 'http://www.boots.com/pre-seed-fertility-lubricant-40g-10244954'
];
