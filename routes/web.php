<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Main\HomeController@index')->name('home');

Route::group(['prefix' => 'how-to-use'], function () {
	Route::get('/', 'Main\HowToUseController@index')->name('how-to-use');
	Route::get('/pregnancy-test-calculator', 'Main\HowToUseController@PregnancyTestCalculator')->name('pregnancy-test-calculator');
	Route::get('/am-i-pregnant', 'Main\HowToUseController@AmIPregnant')->name('am-i-pregnant');
	
});

Route::group(['prefix' => 'your-test-results'], function () {
	Route::get('/', 'Main\ResultsController@index')->name('your-test-results');
	Route::get('/positive-result', 'Main\ResultsController@PositiveResult')->name('positive-result');
	Route::get('/negative-result', 'Main\ResultsController@NegativeResult')->name('negative-result');
	Route::get('/due-date-calculator', 'Main\ResultsController@DueDateCalculator')->name('due-date-calculator');
});

Route::group(['prefix' => 'our-products'], function () {
	Route::get('/', 'Main\ProductsController@index')->name('our-pregnancy-tests');
	Route::get('/first-response-accuracy', 'Main\ProductsController@accuracy')->name('accuracy');
	Route::get('/pre-seed', 'Main\ProductsController@preseed')->name('pre-seed');
	//Route::get('/pre-seed/how-to-use', 'Main\ProductsController@preseed_useage')->name('pre-seed-how-to-use');
});



Route::group(['prefix' => 'planning-for-a-baby'], function () {
	Route::get('/', 'Main\PlanningController@index')->name('planning-for-a-baby');

	Route::group(['prefix' => 'contraception'], function () {
		Route::get('/contraception-advice', 'Main\PlanningController@section')->name('contraception-advice');
	});

	Route::group(['prefix' => 'our-bodies'], function () {
		Route::get('/the-female-body', 'Main\PlanningController@section')->name('the-female-body');
		Route::get('/your-fertile-time', 'Main\PlanningController@section')->name('your-fertile-time');
		Route::get('/ovulation-calculator', 'Main\PlanningController@OvulationCalculator')->name('ovulation-calculator');
		Route::get('/the-male-body', 'Main\PlanningController@section')->name('the-male-body');
	});

	Route::group(['prefix' => 'diet-lifestyle-and-stress'], function () {
		Route::get('/the-right-diet', 'Main\PlanningController@section')->name('the-right-diet');
		Route::get('/vitamins-and-minerals', 'Main\PlanningController@section')->name('vitamins-and-minerals');
		Route::get('/a-healthy-lifestyle', 'Main\PlanningController@section')->name('a-healthy-lifestyle');
		Route::get('/stress-management', 'Main\PlanningController@section')->name('stress-management');
		Route::get('/positive-mind-plan', 'Main\PlanningController@section')->name('positive-mind-plan');
	});

	Route::group(['prefix' => 'sex-and-relationships'], function () {
		Route::get('/sex', 'Main\PlanningController@section')->name('sex');
		Route::get('/your-relationship', 'Main\PlanningController@section')->name('your-relationship');
		// Route::get('/working-together', 'Main\PlanningController@section')->name('working-together');
	});

	Route::group(['prefix' => 'struggling-to-conceive'], function () {
		Route::get('/conception-advice', 'Main\PlanningController@section')->name('conception-advice');
		Route::get('/advice-from-zita-west', 'Main\PlanningController@section')->name('zita-west-videos');
	});

	Route::group(['prefix' => 'pregnancy'], function () {
		Route::get('/stages-of-pregnancy', 'Main\PlanningController@section')->name('stages-of-pregnancy');
		Route::get('/tips-for-a-healthy-pregnancy', 'Main\PlanningController@section')->name('tips-for-a-healthy-pregnancy');
	});
});

Route::get('/faqs', 'Main\FAQsController@index')->name('faqs');
Route::get('/contact', 'Main\SupportController@index')->name('contact');
Route::post('/contact', 'Main\SupportController@store');
Route::get('/buy-now', 'Main\BuyNowController@index')->name('buy-now');

// Footer
//Route::get('/policies', 'Main\MiscController@policies')->name('policies');
Route::get('/cookie-notice', 'Main\MiscController@cookies')->name('cookie-notice');
Route::get('/privacy-policy', 'Main\MiscController@privacy')->name('privacy-policy');
Route::get('/third-party-information-collection', 'Main\MiscController@thirdparty')->name('third-party');
Route::get('/terms', 'Main\MiscController@terms')->name('terms');
Route::get('/sitemap', 'Main\MiscController@sitemap')->name('sitemap');

Route::get('/mailtest', 'Main\MiscController@mailtest')->name('mailtest');

Route::get('/coupon', 'Main\MiscController@coupon')->name('coupon');

Route::get('/flush', 'Main\MiscController@flush');

Route::get('/gdpr', function () {
    $exitCode = Artisan::call('gdpr:check', []);
    echo $exitCode;
});

