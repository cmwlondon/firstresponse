<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Meta Data All in One Place
    |--------------------------------------------------------------------------
    */

    '/' => [
        'title' => 'FIRST RESPONSE&#8482; Fertility - Pregnancy Tests',
        'desc'  => 'FIRST RESPONSE&#8482; Pregnancy Tests tell sooner than any other pregnancy test brand.',
        'image' => 'first-response.jpg',
        'keywords' => 'pregnant,pregnancy,test,firstresponse,pregnancytest,first,response,missed,period',
        'keyphrases' => 'first response, pregnancy test, am i pregnant, missed period'
    ],
    'how-to-use' => [
        'title' => 'How to use your FIRST RESPONSE&#8482; Pregnancy Test',
        'desc'  => 'Step-by-step instructions for how to use your FIRST RESPONSE&#8482; Pregnancy Test.',
        'image' => 'first-response-product.jpg',
        'keywords' => 'pregnancy,test,how,use,instructions,guide',
        'keyphrases' => 'pregnancy test, how to use, pregnancy test instructions'
    ],
    'how-to-use/pregnancy-test-calculator' => [
        'title' => 'When is the best time to take your FIRST RESPONSE&#8482; Pregnancy Test?',
        'desc'  => 'Decide when to take your Pregnancy Test with the FIRST RESPONSE&#8482; Pregnancy Test calculator ',
        'image' => 'first-response-product.jpg',
        'keywords' => 'pregnancy,test,calculator,when,take,guide,pregnant,first,response,firstresponse',
        'keyphrases' => 'first response, pregnancy test calculator, when to take, pregnancy test'
    ],
    'how-to-use/am-i-pregnant' => [
        'title' => 'Am I pregnant? Early pregnancy symptoms to look out for',
        'desc'  => 'Do I need to take a pregnancy test? Early pregnancy symptoms and signs to look out plus when to take a pregnancy test.',
        'image' => 'first-response-could-i-be-pregnant.jpg',
        'keywords' => 'pregnant,pregnancy,test,pregnancytest,symptoms,signs,early',
        'keyphrases' => 'am i pregnant?, pregnancy symptoms?, early signs of pregnancy'
    ],

    'your-test-results' => [
        'title' => 'Am I pregnant? How to read FIRST RESPONSE&#8482; Pregnancy Test results',
        'desc'  => 'Step-by-step instructions on how to read your FIRST RESPONSE&#8482; Pregnancy Test results to tell if you are pregnant or not.',
        'image' => 'first-response-could-i-be-pregnant.jpg',
        'keywords' => 'pregnancy,test,result,results,reading,help,say,pregnant,faint,line',
        'keyphrases' => 'pregnancy test result, reading pregnancy test, faint pink line, one pink line, two pink lines, pregnancy test'
    ],
    'your-test-results/positive-result' => [
        'title' => 'Positive pregnancy test result - what to do now?',
        'desc'  => 'Advice and tips on positive pregnancy test results for planned and unplanned pregnancy - what happens now?',
        'image' => 'first-response-positive-test.jpg',
        'keywords' => 'positive,pregnancy,test,result,pregnant,two,lines,window,pregnancytest,planned,unplanned',
        'keyphrases' => 'positive pregnancy test, pregnancy test result, pregnancy test faint line, two pink lines, two lines'
    ],
    'your-test-results/negative-result' => [
        'title' => 'Negative pregnancy test result - what next?',
        'desc'  => 'Advice on negative pregnancy test results for planned and unplanned pregnancy - what happens now?',
        'image' => 'first-response-negative-test.jpg',
        'keywords' => 'negative,pregnancy,test,result,pregnant,line,window,pregnancytest',
        'keyphrases' => 'negative pregnancy test, pregnancy test result, not pregnant, one pink line'
    ],
    'your-test-results/due-date-calculator' => [
        'title' => 'Calculate your due date with FIRST RESPONSE&#8482; Due Date Calculator.',
        'desc'  => 'You&rsquo;re pregnant! Calculate when your baby is due with FIRST RESPONSE&#8482; Online Due Date Calculator',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'pregnant,pregnancy,test,due,date,calculator,arrival,baby,born,birth',
        'keyphrases' => 'due date calculator, baby due date, give birth'
    ],

    'our-products' => [
        'title' => 'Our FIRST RESPONSE&#8482; Pregnancy Tests - no brand tells you sooner.',
        'desc'  => 'Am I pregnant? Ultra-sensitive FIRST RESPONSE&#8482; Pregnancy Tests offer early results - no brand gives you an answer sooner.',
        'image' => 'first-response-product.jpg',
        'keywords' => 'firstresponse,test,pregnancy,rapid,quickly,sooner,instant,urgent,pregnant,accurate',
        'keyphrases' => 'first response, pregnancy test, early detection, early result'
    ],
    'our-products/first-response-accuracy' => [
        'title' => 'FIRST RESPONSE&#8482; Pregnancy Test accuray',
        'desc'  => 'Find out more about the science behind why FIRST RESPONSE&#8482; Pregnancy Tests are so accurate',
        'image' => 'first-response-product.jpg',
        'keywords' => 'firstresponse,test,pregnancy,rapid,accuracy,sooner,accurate,pregnant,reliable',
        'keyphrases' => 'accurate pregnancy test, reliable pregnancy test, first response accuracy, most accurate pregnancy test'
    ],
    'our-products/pre-seed' => [
        'title' => 'PRE-SEED&#8482; Fertility-Friendly Personal Lubricant',
        'desc'  => 'Trying for a baby? Use Pre-Seed&#8482; Fertility-Friendly Personal Lubricant, from the makers of First Response&#8482; Pregnancy Tests',
        'image' => 'pre-seed.jpg',
        'keywords' => 'conceive, baby, notpregnant, lube, lubricant, pregnancy, pregnant, fertile, notfertile',
        'keyphrases' => 'planning for a baby, trying to get pregnant, help getting pregnant, trying to conceive, how to get pregnant'
    ],
    'our-products/pre-seed/how-to-use' => [
        'title' => 'How to Use Pre-Seed&#8482; Fertility-Friendly Personal Lubricant',
        'desc'  => 'Trying for a baby? See our guide on how to use Pre-Seed&#8482; Fertility-Friendly Personal Lubricant, from the makers of First Response&#8482; Pregnancy Tests',
        'image' => 'pre-seed.jpg',
        'keywords' => 'conceive, baby, notpregnant, lube, lubricant, pregnancy, pregnant, fertile, notfertile',
        'keyphrases' => 'planning for a baby, trying to get pregnant, help getting pregnant, trying to conceive, how to get pregnant'
    ],

    'faqs' => [
        'title' => 'Your pregnancy questions answered - FIRST RESPONSE&#8482; FAQs.',
        'desc'  => 'Your pregnancy FAQs - how do I take a pregnancy test? How do pregnancy tests work? I&rsquo;m pregnant - what now?',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'pregnancy,pregnant,pregnancytest,help,advice,faqs,questions,test',
        'keyphrases' => 'pregnancy faq, questions about being pregnant, why aren\'t i pregnant, how do pregnancy tests work'
    ],
    'contact' => [
        'title' => 'Contact FIRST RESPONSE&#8482; Advice Line with your pregnancy questions.',
        'desc'  => 'Speak to FIRST RESPONSE&#8482; specialist team with your pregnancy questions for advice, help and quick answers.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'contact,first,response,pregnant,pregnancy,test,company,advice,helpline,help',
        'keyphrases' => 'helpline, first response, advice on pregnancy, just found out i\'m pregnant'
    ],
    'buy-now' => [
        'title' => 'Where to buy FIRST RESPONSE&#8482; Pregnancy Tests online and in-store.',
        'desc'  => 'Where to buy your FIRST RESPONSE&#8482; Pregnancy Tests both online and in-store quickly.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'first,response,buynow,pregnancy,test,online,quick,order',
        'keyphrases' => 'order first response online, where to buy pregnancy test, order pregnancy test online'
    ],

    'cookie-notice' => [
        'title' => 'FIRST RESPONSE&#8482; Cookie Notice.',
        'desc'  => 'Information regarding cookies used on the FIRST RESPONSE&#8482; website.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'first,response,company,policies,pregnancy,test,legal,help',
        'keyphrases' => 'first response policies, pregnancy test policy, first response legal policies'
    ],
    'privacy-policy' => [
        'title' => 'FIRST RESPONSE&#8482; Privacy Policy.',
        'desc'  => 'FIRST RESPONSE&#8482; privacy policy.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'first,response,company,policies,pregnancy,test,legal,help',
        'keyphrases' => 'first response policies, pregnancy test policy, first response legal policies'
    ],
    'policies' => [
        'title' => 'FIRST RESPONSE&#8482; Pregnancy Test policies online.',
        'desc'  => '',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'first,response,company,policies,pregnancy,test,legal,help',
        'keyphrases' => 'first response policies, pregnancy test policy, first response legal policies'
    ],
    'terms' => [
        'title' => 'FIRST RESPONSE&#8482; Pregnancy Test terms online.',
        'desc'  => '',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'first,response,pregnancy,test,terms,conditions,help,legal,t&cs',
        'keyphrases' => 'first response terms, pregnancy test terms'
    ],
    
    'planning-for-a-baby' => [
        'title' => 'Advice from FIRST RESPONSE&#8482; on planning for a baby.',
        'desc'  => 'Advice from FIRST RESPONSE&#8482; on planning for a baby.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'planning,baby,first,response,pregnancy,pregnant,trying,conceive,tips,help,advice',
        'keyphrases' => 'trying for a baby, planning for a baby, how to get pregnant, family planning'
    ],
    
    'coupon' => [
        'title' => 'Looking for FIRST RESPONSE&#8482; offers?',
        'desc'  => 'We don’t have any coupons running right now, but we will be running offers throughout the year at the retailers below',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'first,response,pregnancy,pregnant,coupons,offers,boots,superdrug',
        'keyphrases' => 'first response coupons, special offers, pregnancy test offers, coupons and offers'
    ],

    'planning-for-a-baby/contraception/contraception-advice' => [
        'title' => 'Advice on stopping contraception when trying for a baby.',
        'desc'  => 'Get advice from First Response on the best time to stop your contraceptive method while trying to get pregnant? ',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'pregnant,pregnancy,firstresponse',
        'keyphrases' => 'trying to get pregnant?'
    ],

    'planning-for-a-baby/our-bodies/the-female-body' => [
        'title' => 'Female reproductive system: Anatomy diagram and information.',
        'desc'  => 'Get to know the female body and the key female reproductive organs with our anatomy diagram and information.',
        'image' => 'first-response-logo.jpg',
        'keywords' => '',
        'keyphrases' => ''
    ],

    'planning-for-a-baby/our-bodies/your-fertile-time' => [
        'title' => 'Calculate your most fertile time.',
        'desc'  => 'When are you most fertile? Advice from First Response on understanding your most fertile time based on your menstrual cycle.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'firstresponse,fertile',
        'keyphrases' => 'when are you most fertile?'
    ],

    'planning-for-a-baby/our-bodies/ovulation-calculator' => [
        'title' => 'Calculate the dates you’re most likely to ovulate.',
        'desc'  => 'Predict your most fertile time with the First Response online Ovulation Calculator.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'fertile,ovulation',
        'keyphrases' => 'ovulation calculator'
    ],

    'planning-for-a-baby/our-bodies/the-male-body' => [
        'title' => 'Male reproductive system: Anatomy diagram and boosting sperm quality tips.',
        'desc'  => 'Get to know the male body and the key male reproductive organs and advice plus tips on boosting sperm quality.',
        'image' => 'first-response-logo.jpg',
        'keywords' => '',
        'keyphrases' => ''
    ],

    'planning-for-a-baby/diet-lifestyle-and-stress/the-right-diet' => [
        'title' => 'The right diet - Advice for boosting fertility and a healthy pregnancy.',
        'desc'  => 'Tips on the best foods to incorporate in to your diet and those to avoid, for boosting fertility and supporting your baby’s growth.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'fertility',
        'keyphrases' => 'boosting fertility'
    ],

    'planning-for-a-baby/diet-lifestyle-and-stress/vitamins-and-minerals' => [
        'title' => 'Vitamins and minerals: Essentials for fertility and a healthy pregnancy.',
        'desc'  => 'A healthy balanced diet? Read our advice on taking vitamins and minerals that are essential for fertility and pregnancy.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'fertility,pregnancy',
        'keyphrases' => ''
    ],

    'planning-for-a-baby/diet-lifestyle-and-stress/a-healthy-lifestyle' => [
        'title' => 'Trying to conceive? Our First Response guide to making healthy lifestyle choices.',
        'desc'  => 'Trying to get pregnant? Fertility involves the whole body so it’s important to try and stay healthy inside and out.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'pregnant,fertility',
        'keyphrases' => 'trying to get pregnant,a healthy pregnancy'
    ],

    'planning-for-a-baby/diet-lifestyle-and-stress/stress-management' => [
        'title' => 'Tips on managing stress while trying to get pregnant.',
        'desc'  => 'Get advice and tips from First Response on managing stress while you’re trying to get pregnant.',
        'image' => 'first-response-logo.jpg',
        'keywords' => '',
        'keyphrases' => ''
    ],

    'planning-for-a-baby/diet-lifestyle-and-stress/positive-mind-plan' => [
        'title' => 'Trying to conceive? Advice from Zita West on maintaining a positive mindframe.',
        'desc'  => 'Read advice from Zita West on maintaing a positive mindframe while trying to get pregnant.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'conceive,pregnant',
        'keyphrases' => 'trying to get pregnant,trying to conceive'
    ],

    'planning-for-a-baby/sex-and-relationships/sex' => [
        'title' => 'Trying to get pregnant: Advice on when and often to have sex.',
        'desc'  => 'How frequently should couples should have sex while trying for a baby? Advice on how to increase chances of conception.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'pregnant',
        'keyphrases' => 'trying to get pregnant'
    ],

    'planning-for-a-baby/sex-and-relationships/your-relationship' => [
        'title' => 'You’re trying for baby! How might this affect your relationship?',
        'desc'  => 'Advice on ways of working together to maintain a healthy relationship while trying for a baby.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'tryingforababy',
        'keyphrases' => 'trying for a baby'
    ],

    'planning-for-a-baby/struggling-to-conceive/conception-advice' => [
        'title' => 'How long should it take to get pregnant? Informaiton on comon causes of infertility.',
        'desc'  => 'First Response guidance on the length of time it takes to conceive and an overview of common factors affecting fertility.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'firstresponse,conceive',
        'keyphrases' => ''
    ],

    'planning-for-a-baby/struggling-to-conceive/advice-from-zita-west' => [
        'title' => 'Videos containing information and advice from fertility expert Zita west.',
        'desc'  => 'Advice and tips from fertility expert Zita West on boosting you and your partner’s fertility.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'fertility,zitawest',
        'keyphrases' => 'boosting fertility'
    ],

    'planning-for-a-baby/pregnancy/stages-of-pregnancy' => [
        'title' => 'You’re pregnant! Understanding the different stages of pregnancy with First Response.',
        'desc'  => 'Find out about the different stages of pregnancy from trimester to trimester.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'pregnant,trimester,pregnancy,firstresponse',
        'keyphrases' => 'you’re pregnant!, stages of pregnancy'
    ],

    'planning-for-a-baby/pregnancy/tips-for-a-healthy-pregnancy' => [
        'title' => 'You’re pregnant! First Response advice and tips for a healthy pregnancy.',
        'desc'  => 'You’re pregnant! Get top tips for staying healthy throughout your pregnancy.',
        'image' => 'first-response-logo.jpg',
        'keywords' => 'pregnant,pregnancy,firstresponse',
        'keyphrases' => 'you’re pregnant!, a healthy pregnancy'
    ],
];
