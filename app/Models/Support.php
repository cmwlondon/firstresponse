<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Support extends Model {

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'supports';


    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','firstname','lastname','address','city','postcode','country','phone','email','product','comments'];

}
