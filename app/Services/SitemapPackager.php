<?php namespace App\Services;

use Response;
use Route;
use Carbon\Carbon;
use Storage;
use LSS\Array2XML;

class SitemapPackager {

	public function __construct()
	{
		Carbon::setLocale('en');

		$this->sitemap = [
			'@attributes' => [
				'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
				'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd',
				'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9'
			],

			'url' => []
		];


	}

	public function create() {
		/* */
		$now = new Carbon();
		$now = $now->toW3cString();

		// generate sitemap URL list based on declared Routes, define routes to omit from sitemap
		$exclusions = ["api/user", "mailtest", "sitemap","flush","coupon"];

		$routeCollection = Route::getRoutes();
		foreach ($routeCollection as $value) {
		    if(in_array("GET",$value->methods())) {
		    	if(!in_array($value->uri(),$exclusions)) {
		    		if ($value->uri() == "/") {
		    			$rank = 1;
		    		} else {
		    			$rank = 1 - (count(explode("/",$value->uri()))*0.1);
		    		}

		    		$this->addUrl(url($value->uri()),$now,'monthly',$rank);
		    	}
		    }
		}

		// Convert and save
		$xml = Array2XML::createXML('urlset', $this->sitemap);

		$response = $xml->saveXML();
		//dd(Storage::disk('public'));
		Storage::disk('public')->put('sitemap.xml', $response);
		/* */

		$response = Storage::disk('public')->get('sitemap.xml'); // /storage/app/public/sitemap.xml
		
		return Response::make($response, '200')->header('Content-Type', 'text/xml');
	}

	private function addUrl($url,$lastmod,$freq,$priority) {
		$this->sitemap['url'][] = ['loc' => $url,
							'lastmod' => $lastmod,
							'changefreq' => $freq,
							'priority' => $priority
							];
	}
}
