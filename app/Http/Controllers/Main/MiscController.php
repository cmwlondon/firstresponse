<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Services\SitemapPackager;
use Mail;
use App\Mail\SysMail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class MiscController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['jsVars']['activeSection'] = 0;
	}

	/**
	 * Show the application cookie notice screen to the user.
	 *
	 * @return Response
	 */
	public function policies()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'misc';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$cache = view('main.misc.policies', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the application cookie notice screen to the user.
	 *
	 * @return Response
	 */
	public function cookies()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'misc';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$cache = view('main.misc.cookies', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the application privacy policy screen to the user.
	 *
	 * @return Response
	 */
	public function privacy()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'misc';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$cache = view('main.misc.privacy', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the application third party information collection screen to the user.
	 *
	 * @return Response
	 */
	public function thirdparty()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'misc';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$cache = view('main.misc.third-party', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the application terms screen to the user.
	 *
	 * @return Response
	 */
	public function terms()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'misc';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$cache = view('main.misc.terms', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the coupon screen to the user.
	 *
	 * @return Response
	 */
	public function coupon()
	{	
		/* Remember this is also excluded in the sitemap */
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['noindex'] 		= true;
			$this->context['pageClass'] 	= 'misc min';
			$this->context['pageViewJS']	= 'main/pages/coupon.min';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$cache = view('main.misc.coupon', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	public function mailtest()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['noindex'] 		= true;
			$this->context['pageClass'] 	= 'misc min';
			$this->context['pageViewJS']	= 'main/pages/mailtest.min';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$this->context['time'] = date("Y-m-d H:i:s");

			$details = array(
				'title'			=> 'Mr',
				'firstname'		=> 'Albert',
				'lastname'		=> 'Einstein',
				'phone'			=> '7542087301',
				'email'			=> 'albert.einstain@gmail.com',
				'product'		=> 'firstresponse mailgun test product',
				'comments'		=> 'firstresponse mailgun test comment: '.date("Y-m-d H:i:s"),
				'mfgcode'		=> 'firstresponse',
				'timestamp' 	=> date("Y-m-d H:i:s")
			);

			Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
			{
				$message->from(env('MAIL_NOREPLY'), 'firstresponse');
				$message->to(env('MAIL_SYS_TO'));
			    $message->subject('UK ChurchDwight Enquiry');
			});

			$cache = view('main.misc.mailtest', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}
	/**
	 * Show the application sitemap screen to the user.
	 *
	 * @return Response
	 */
	public function sitemap(SitemapPackager $packager)
	{	
		$pkg_response = $packager->create(); // Update the Sitemap		
		return $pkg_response;
	}

	/**
	 * Flush the cache
	 *
	 * @return Response
	 */
	public function flush()
	{	
		$this->clearCache();
	}
}
