<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class ResultsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['jsVars']['activeSection'] = 2;
	}

	/**
	 * Show the application reading-your-test screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'reading-your-test';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/reading-your-test';

			$cache = view('main.your-test-results.reading-your-test', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the application due-date-calculator screen to the user.
	 *
	 * @return Response
	 */
	public function DueDateCalculator()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'due-date-calculator';
			$this->context['pageViewJS']	= 'main/pages/due-date-calculator.min';
			$this->context['pageViewCSS']	= 'main/pages/due-date-calculator';

			$cache = view('main.your-test-results.due-date-calculator', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the application positive-result screen to the user.
	 *
	 * @return Response
	 */
	public function PositiveResult()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'result';
			$this->context['pageViewJS']	= 'main/pages/positive-result.min';
			$this->context['pageViewCSS']	= 'main/pages/positive-negative-result';

			$cache = view('main.your-test-results.positive-result', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the application negative-result screen to the user.
	 *
	 * @return Response
	 */
	public function NegativeResult()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'result';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/positive-negative-result';

			$cache = view('main.your-test-results.negative-result', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}
}
