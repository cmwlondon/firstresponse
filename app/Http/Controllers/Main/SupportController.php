<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Http\Requests\Main\SupportRequest;
use App\Models\Support;
use Illuminate\Support\Facades\Mail;
use App\Mail\SysMail;
use App\Mail\Enquiry;
use App;

class SupportController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['jsVars']['activeSection'] = 5;
	}

	/**
	 * Show the application policies screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$this->context['pageClass'] 	= 'misc';
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/pages/support';

		$this->context['titlesArr'] = ['' => 'Select', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Ms' => 'Ms'];

		return view('main.support.home', $this->context);
	}

	public function store(SupportRequest $request, Support $support)
	{
		$support->fill($request->all());
		$support->save();

		// Prepare the email to send.
		$details = array(
			'title'			=> $request->title,
			'firstname'		=> $request->firstname,
			'lastname'		=> $request->lastname,
			'company'		=> '',//$request->company,
			'address'		=> '',//$request->address,
			'address_2'		=> '',//$request->address_2,
			'city'			=> '',//$request->city,
			'state'			=> '',//$request->county,
			'zip'			=> '',//$request->postcode,
			'country'		=> '',//$request->country,
			'phone'			=> $request->phone,
			'email'			=> $request->email,
			'product'		=> $request->product,
			'comments'		=> $request->comments,
			'mfgcode'		=> $request->mfgcode
		);

		// if (App::environment() != 'local') {

			Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
			{
				$message->from(env('MAIL_NOREPLY'), 'firstresponse');
				$message->to(env('MAIL_SYS_TO'));

			    if (!is_null(env('MAIL_ADMIN_CC')) && env('MAIL_ADMIN_CC') != "") {
			    	$message->cc(env('MAIL_ADMIN_CC'));
			    }
			    if (!is_null(env('MAIL_ADMIN_BCC')) && env('MAIL_ADMIN_BCC') != "") {
			    	$message->bcc(env('MAIL_ADMIN_BCC'));
			    }

			    $message->subject('UK ChurchDwight Enquiry');
			});

		// }

		$this->response = array('response_status' => 'success', 'message' => 'Thank you for contacting First Response&#8482;');

		return redirect()->route('contact')->with('response', $this->response);
	}
}
