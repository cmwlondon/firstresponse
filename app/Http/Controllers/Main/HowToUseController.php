<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class HowToUseController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['jsVars']['activeSection'] = 1;
	}

	/**
	 * Show the application taking-a-test screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'taking-a-test';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/taking-a-test';

			$cache = view('main.how-to-use.taking-a-test', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the application am-i-pregnant screen to the user.
	 *
	 * @return Response
	 */
	public function AmIPregnant()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'am-i-pregnant';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/am-i-pregnant';

			$cache = view('main.how-to-use.am-i-pregnant', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function PregnancyTestCalculator()
	{
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'pregnancy-test-caculator';
			$this->context['pageViewJS']	= 'main/pages/pregnancy-test-calculator.min';
			$this->context['pageViewCSS']	= 'main/pages/pregnancy-test-calculator';

			$cache = view('main.how-to-use.pregnancy-test-calculator', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}
}
