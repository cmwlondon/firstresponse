<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Route;

class BuyNowController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['jsVars']['activeSection'] = 6;
	}

	/**
	 * Show the application policies screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'misc';
			$this->context['pageViewJS']	= 'main/pages/buy-now.min';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$cache = view('main.buy.home', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}
}
