<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class ProductsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['jsVars']['activeSection'] = 3;
	}

	/**
	 * Show the application reading-your-test screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'products';
			$this->context['pageViewJS']	= 'main/pages/products.min';
			$this->context['pageViewCSS']	= 'main/pages/products';

			$cache = view('main.products.home', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}


	/**
	 * Show the application first response accuracy screen to the user.
	 *
	 * @return Response
	 */
	public function accuracy()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'misc-alt';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$cache = view('main.products.accuracy', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the pre-seed screen to the user.
	 *
	 * @return Response
	 */
	public function preseed()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'pre-seed';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/pre-seed';
			$this->context['preSeed'] = true;

			$cache = view('main.products.pre-seed', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	/**
	 * Show the pre-seed how to use screen to the user.
	 *
	 * @return Response
	 */
	public function preseed_useage()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'pre-seed how-to-use';
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/pre-seed';
			$this->context['preSeed'] = true;

			$cache = view('main.products.pre-seed-how-to-use', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}
}
