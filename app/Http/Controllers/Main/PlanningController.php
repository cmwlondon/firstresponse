<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Request;


class PlanningController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['jsVars']['activeSection'] = 4;
	}

	/**
	 * Show the application planning for a baby screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$cache = $this->checkCache();

		if (!$cache) {
			$this->context['pageClass'] 	= 'misc min'; // <-- Remove min when we have proper content
			$this->context['pageViewJS']	= '';
			$this->context['pageViewCSS']	= 'main/pages/misc';

			$cache = view('main.planning.home', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	public function section()
	{	
		$section = Request::segment(3);
		$this->pageLog($section);
		

		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'planning';
			
			switch($section) {
				case 'contraception-advice':
					$viewJS = 'main/pages/contraception-advice.min';
					break;
				case 'your-relationship':
					$viewJS = 'main/pages/your-relationship.min';
					break;
				case 'stages-of-pregnancy':
					$viewJS = 'main/pages/stages-of-pregnancy.min';
					break;
				case 'tips-for-a-healthy-pregnancy':
					$viewJS = 'main/pages/tips-for-a-healthy-pregnancy.min';
					break;
				case 'advice-from-zita-west':
					$viewJS = 'main/pages/zita-west-videos.min';
					break;
				default:
					$viewJS = '';
			}

			$this->context['pageViewJS']	= $viewJS;
			$this->context['pageViewCSS']	= 'main/pages/planning';
			$cache = view('main.planning.'.$section, $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}

	public function OvulationCalculator()
	{
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'ovulation-caculator';
			$this->context['pageViewJS']	= 'main/pages/ovulation-calculator.min';
			$this->context['pageViewCSS']	= 'main/pages/ovulation-calculator';
			$cache = view('main.planning.ovulation-calculator', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}
}
