<?php namespace App\Http\Controllers\Main;

use App;
use App\Http\Controllers\Controller;
use Session;
use Request;
use Illuminate\Support\Facades\Cache;
use Log;

use App\Models\Carousel;

class MainController extends Controller {

	public function __construct() {
		$this->uri = Request::path();
		// Context now pulled from config so we can use it in error pages.
		$this->context = config('frf');
		$this->context['site_url'] = url('',[]);
		$this->context['meta']['link'] = url($this->uri);
		$this->context['meta']['image'] = url('/images/first-response-logo.png');
		
		$this->context['image_path'] = '/images';
		$this->context['pageClass'] = config('frf.pageClass');

		if (Session('response') !== NULL) {
			$this->context['response'] = Session('response');
		}

		// Check for meta in the config
		$meta = config('meta');
		if (array_key_exists($this->uri,$meta)) {
			foreach($this->context['meta'] as $key => $value) {
				if (array_key_exists($key,$meta[$this->uri])) {
					if ($key == "title" && $meta[$this->uri][$key] == "") {
						// Ignore blank titles;
					} elseif ($key == "image" && $meta[$this->uri][$key] != "") {
						$this->context['meta']['image'] = url('images/sharing/'.$meta[$this->uri][$key]);
					} else {
						$this->context['meta'][$key] = $meta[$this->uri][$key];
					}
				}
			}
		}
	}

	public function checkCache() {
		if (App::environment('production','staging')) {
			if (Cache::has($this->uri)) {
				return cache($this->uri);
			}
		}
		return false;
	}

	public function saveCache($html) {
		if (App::environment('production','staging')) {
			cache([$this->uri => $html], 10);
		}
	}

	public function clearCache() {
		Cache::flush();
	}

	public function pageLog($msg) {
		if (App::environment('local','development')) {
			Log::info('=============================================');
			Log::info($msg);
		}
	}
}
