<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Http\Requests;
use App\Services\SitemapPackager;

class SitemapController extends MainController {

	/**
	 * Create a new controller instance with dependency injection
	 *
	 * @return void
	 */
	public function __construct(SitemapPackager $packager) {

		parent::__construct();

		$this->packager = $packager;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function refresh()
	{
		$pkg_response = $this->packager->create();
		return $pkg_response;

		/*
		$this->context['title'] = 'Sitemap refresh';
		return view('admin.sitemap.refresh', $this->context);
		*/
	}

}
