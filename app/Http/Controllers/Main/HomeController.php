<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Request;

class HomeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['meta']['title'] = 'FIRST RESPONSE';
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'home';
			$this->context['pageViewJS']	= 'main/pages/home.min';
			$this->context['pageViewCSS']	= ''; //'main/pages/home';

			$cache = view('main.home.home', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}
}
