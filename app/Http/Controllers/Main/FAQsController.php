<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class FAQsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['jsVars']['activeSection'] = 5;
	}

	/**
	 * Show the application faqs screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$cache = $this->checkCache();
		if (!$cache) {
			$this->context['pageClass'] 	= 'faqs';
			$this->context['pageViewJS']	= 'main/pages/faqs.min';
			$this->context['pageViewCSS']	= 'main/pages/faqs';

			$cache = view('main.faqs.home', $this->context)->render();
			$this->saveCache($cache);
		}
		return $cache;
	}
}
