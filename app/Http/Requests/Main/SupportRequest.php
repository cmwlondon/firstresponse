<?php namespace App\Http\Requests\Main;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class SupportRequest extends FormRequest {

	public function __construct() {

	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		
		$rules = [
			'title'				=> 'required|max:255',
			'firstname'			=> 'required|max:255',
			'lastname'      	=> 'required|max:255',
			//'address'      		=> 'max:255',
			//'city'      		=> 'max:255',
			//'postcode'			=> 'max:20',
			//'country'			=> 'max:255',
			'phone'				=> 'min:5|max:20',
			'email'				=> 'required|email',
			'product'			=> 'max:255',
			'comments'			=> ''
		];

		return $rules;
	}

	public function messages()
	{
	    return [
	    ];
	}

}
